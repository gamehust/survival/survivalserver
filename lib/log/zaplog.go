package log

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var ZapLog *zap.SugaredLogger
var zapLogLevel = zapcore.WarnLevel

func InitZapLog(logLevel string) {
	switch logLevel {
	case "debug", "Debug", "DebugLevel":
		zapLogLevel = zapcore.DebugLevel
	case "info", "Info", "InfoLevel":
		zapLogLevel = zapcore.InfoLevel
	case "warn", "Warn", "WarnLevel":
		zapLogLevel = zapcore.WarnLevel
	case "error", "Error", "ErrorLevel":
		zapLogLevel = zapcore.ErrorLevel
	case "panic", "Panic", "PanicLevel":
		zapLogLevel = zapcore.PanicLevel
	case "fatal", "Fatal", "FatalLevel":
		zapLogLevel = zapcore.FatalLevel
	}
	zapLogger, err := zap.Config{
		Level:       zap.NewAtomicLevelAt(zapLogLevel),
		Development: false,
		Encoding:    "console",
		EncoderConfig: zapcore.EncoderConfig{
			MessageKey:          "M",
			LevelKey:            "L",
			TimeKey:             "T",
			NameKey:             "N",
			CallerKey:           "C",
			FunctionKey:         zapcore.OmitKey,
			StacktraceKey:       zapcore.OmitKey,
			LineEnding:          zapcore.DefaultLineEnding,
			EncodeLevel:         zapcore.CapitalLevelEncoder,
			EncodeTime:          zapcore.RFC3339TimeEncoder,
			EncodeDuration:      zapcore.SecondsDurationEncoder,
			EncodeCaller:        zapcore.ShortCallerEncoder,
			EncodeName:          nil,
			NewReflectedEncoder: nil,
			ConsoleSeparator:    " - ",
		},
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
	}.Build()
	if err != nil {
		panic(err)
	}
	ZapLog = zapLogger.Sugar()
}

func GetLogLevel() zapcore.Level {
	return zapLogLevel
}
