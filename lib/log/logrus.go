package log

import (
	"github.com/sirupsen/logrus"
	"os"
)

var Logrus = logrus.New()
var logrusLevel = logrus.WarnLevel

func InitLogrus(logLevel string) {
	Logrus.SetFormatter(&logrus.TextFormatter{})
	Logrus.SetOutput(os.Stdout)
	switch logLevel {
	case "trace", "Trace", "TraceLevel":
		logrusLevel = logrus.TraceLevel
	case "debug", "Debug", "DebugLevel":
		logrusLevel = logrus.DebugLevel
	case "info", "Info", "InfoLevel":
		logrusLevel = logrus.InfoLevel
	case "warn", "Warn", "WarnLevel":
		logrusLevel = logrus.WarnLevel
	case "error", "Error", "ErrorLevel":
		logrusLevel = logrus.ErrorLevel
	case "panic", "Panic", "PanicLevel":
		logrusLevel = logrus.PanicLevel
	case "fatal", "Fatal", "FatalLevel":
		logrusLevel = logrus.FatalLevel
	}
	Logrus.SetLevel(logrusLevel)
}

func GetLogrusLevel() logrus.Level {
	return logrusLevel
}
