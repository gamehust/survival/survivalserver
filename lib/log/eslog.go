package log

import (
	"Survival/src/models"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

func getUserLogsDailyIndex() string {
	y, m, d := time.Now().Date()
	return fmt.Sprintf("survival-user-logs-%d-%d-%d", y, m, d)
}

func ESLogSave(userId string, action int64, detail interface{}) {
	go func() {
		esObj := ESLogObj{userId, time.Now().UnixMilli(), action, fmt.Sprintf("%#v", detail)}
		esUrl := fmt.Sprintf("%s/%s/_doc", models.ENVConfig.ElasticUri, getUserLogsDailyIndex())
		if data, err := json.Marshal(&esObj); err != nil {
			ZapLog.Warn(err)
		} else if _, err = http.Post(esUrl, "application/json", bytes.NewReader(data)); err != nil {
			ZapLog.Warn(err)
		}
	}()
}

const (
	ActionCheatUserLevel int64 = iota + 1
	ActionCheatGem
	ActionCheatGold
	ActionCheatEnergy
	ActionCheatRevivalCoin
	ActionCheatStone
	ActionCheatPetPiece
	ActionCheatDesign
	ActionCheatTechUsed
	ActionCheatTalent
	ActionCheatCampaign
	ActionCheatBossMode
	ActionCheatDungeon
	ActionCheatNewDay
	ActionCheatDailyQuest
	ActionCheatWeeklyQuest
	ActionCheatAchievement
	ActionCheatRookieLogin
	ActionQuest7Days
)

const (
	ActionLoginDevice int64 = iota + 1001
	ActionLoginFacebook
	ActionLoginGoogle
	ActionLoginApple
	ActionLoginPlayGames
	ActionLoginGameCenter
	ActionDeleteAccount
)

const (
	ActionGetMainGlobalRanking int64 = iota + 2001
	ActionGetMainCountryRanking
	ActionClaimMailBox
	ActionClaimGiftCode
	ActionWebSocket
)

const (
	ActionChangeName int64 = iota + 3001
	ActionChangeAvatar
	ActionSelectHero
	ActionEquipPet
	ActionUnequipPet
	ActionEquipEquipment
	ActionUnequipEquipment
	ActionEquipTechPart
	ActionUnequipTechPart
)

const (
	ActionSaveAsset1 int64 = iota + 4001
	ActionSaveAsset2
	ActionUnlockHero
	ActionUpgradeHeroLevel
	ActionUpgradeHeroRarity
	ActionUnlockPet
	ActionUpgradePetLevel
	ActionUpgradePetRarity
	ActionUpgradeEquipment
	ActionUpgradeEquipmentToLevel
	ActionDowngradeEquipment
	ActionMergeEquipment
	ActionSplitEquipment
	ActionUpgradeTechPart
	ActionUpgradeTechPartToLevel
	ActionDowngradeTechPart
	ActionMergeTechPart
	ActionSplitTechPart
	ActionUpgradeTalentNormal
	ActionUpgradeTalentSpecial
)

const (
	ActionPlayCampaign int64 = iota + 5001
	ActionClaimCampaignReward
	ActionPlayBossMode
	ActionClaimBossModeReward
	ActionPlayDungeon
	ActionClaimDungeonReward
	ActionClaimExploration
	ActionQuickExploration
	ActionQuickExplorationAds
	ActionClaimDailyReward
	ActionClaimDailyRewardBonus
	ActionClaimDailyQuestReward
	ActionClaimDailyQuestBonus
	ActionClaimWeeklyQuestReward
	ActionClaimWeeklyQuestBonus
	ActionClaimAchievementReward
	ActionClaimRookieLoginReward
	ActionClaimQuest7DaysReward
	ActionClaimQuest7DaysBonus
)

const (
	ActionClaimMonthlyInAppReward int64 = iota + 6001
	ActionBuyChapterPack
	ActionBuyDailySale
	ActionOpenRareChestAds
	ActionOpenRareChestKey
	ActionOpenRareChestGem
	ActionOpenMythChestAds
	ActionOpenMythChestKey
	ActionOpenMythChestGem
	ActionOpen10MythChest
	ActionBuyGemPack
	ActionBuyGoldPack
	ActionBuyEnergyPack
	ActionBuyDailyDiscount
	ActionClaimDailyDiscountBonus
	ActionBuyWeeklyDiscount
	ActionClaimWeeklyDiscountBonus
	ActionBuyMonthlyDiscount
	ActionClaimMonthlyDiscountBonus
)

const (
	ActionBuyMonthlyCardNormal int64 = iota + 7001
	ActionBuyMonthlyCardSuper
	ActionClaimMonthlyCardNormalReward
	ActionClaimMonthlyCardSuperReward
	ActionBuyBattlePassNormal
	ActionBuyBattlePassSuper
	ActionClaimBattlePassFreeReward
	ActionClaimBattlePassNormalReward
	ActionClaimBattlePassSuperReward
	ActionBuyLevelPass1Normal
	ActionBuyLevelPass1Super
	ActionClaimLevelPass1FreeReward
	ActionClaimLevelPass1NormalReward
	ActionClaimLevelPass1SuperReward
	ActionBuyLevelPass2Normal
	ActionBuyLevelPass2Super
	ActionClaimLevelPass2FreeReward
	ActionClaimLevelPass2NormalReward
	ActionClaimLevelPass2SuperReward
	ActionBuyCampaignPassNormal
	ActionBuyCampaignPassSuper
	ActionClaimCampaignPassFreeReward
	ActionClaimCampaignPassNormalReward
	ActionClaimCampaignPassSuperReward
	ActionBuyDungeonPassHard
	ActionBuyDungeonPassCrazy
	ActionClaimDungeonPassNormalReward
	ActionClaimDungeonPassHardReward
	ActionClaimDungeonPassCrazyReward
	ActionBuyGoldPiggy
)

type ESResponse struct {
	Took     int64            `json:"took,omitempty"`
	TimedOut bool             `json:"timed_out,omitempty"`
	Shards   ESResponseShards `json:"_shards,omitempty"`
	Hits     ESResponseHits   `json:"hits,omitempty"`
}

type ESResponseShards struct {
	Total      int64 `json:"total,omitempty"`
	Successful int64 `json:"successful,omitempty"`
	Failed     int64 `json:"failed,omitempty"`
}

type ESResponseHits struct {
	Total ESResponseTotal `json:"total,omitempty"`
	Hits  []ESLogObjs     `json:"hits,omitempty"`
}

type ESResponseTotal struct {
	Value    int64  `json:"value,omitempty"`
	Relation string `json:"relation,omitempty"`
}

type ESLogObjs struct {
	Index  string   `json:"_index,omitempty"`
	Type   string   `json:"_type,omitempty"`
	Id     string   `json:"_id,omitempty"`
	Source ESLogObj `json:"_source,omitempty"`
}

type ESLogObj struct {
	UserId string `json:"u"`
	Time   int64  `json:"t"`
	Action int64  `json:"a"`
	Detail string `json:"d"`
}

type ESLogApi struct {
	Time   string `json:"0"`
	Action string `json:"1"`
	Detail string `json:"2"`
}

func (m *ESLogObj) GetAction() string {
	switch m.Action {
	case ActionCheatUserLevel:
		return "CheatUserLevel"
	case ActionCheatGem:
		return "CheatGem"
	case ActionCheatGold:
		return "CheatGold"
	case ActionCheatEnergy:
		return "CheatEnergy"
	case ActionCheatRevivalCoin:
		return "CheatRevivalCoin"
	case ActionCheatStone:
		return "CheatStone"
	case ActionCheatPetPiece:
		return "CheatPetPiece"
	case ActionCheatDesign:
		return "CheatDesign"
	case ActionCheatTechUsed:
		return "CheatTechUsed"
	case ActionCheatTalent:
		return "CheatTalent"
	case ActionCheatCampaign:
		return "CheatCampaign"
	case ActionCheatBossMode:
		return "CheatBossMode"
	case ActionCheatDungeon:
		return "CheatDungeon"
	case ActionCheatNewDay:
		return "CheatNewDay"
	case ActionCheatDailyQuest:
		return "CheatDailyQuest"
	case ActionCheatWeeklyQuest:
		return "CheatWeeklyQuest"
	case ActionCheatAchievement:
		return "CheatAchievement"
	case ActionCheatRookieLogin:
		return "CheatRookieLogin"
	case ActionQuest7Days:
		return "Quest7Days"
	case ActionLoginDevice:
		return "LoginDevice"
	case ActionLoginFacebook:
		return "LoginFacebook"
	case ActionLoginGoogle:
		return "LoginGoogle"
	case ActionLoginApple:
		return "LoginApple"
	case ActionLoginPlayGames:
		return "LoginPlayGames"
	case ActionLoginGameCenter:
		return "LoginGameCenter"
	case ActionDeleteAccount:
		return "DeleteAccount"
	case ActionGetMainGlobalRanking:
		return "GetMainGlobalRanking"
	case ActionGetMainCountryRanking:
		return "GetMainCountryRanking"
	case ActionClaimMailBox:
		return "ClaimMailBox"
	case ActionClaimGiftCode:
		return "ClaimGiftCode"
	case ActionWebSocket:
		return "WebSocket"
	case ActionChangeName:
		return "ChangeName"
	case ActionChangeAvatar:
		return "ChangeAvatar"
	case ActionSelectHero:
		return "SelectHero"
	case ActionEquipPet:
		return "EquipPet"
	case ActionUnequipPet:
		return "UnequipPet"
	case ActionEquipEquipment:
		return "EquipEquipment"
	case ActionUnequipEquipment:
		return "UnequipEquipment"
	case ActionEquipTechPart:
		return "EquipTechPart"
	case ActionUnequipTechPart:
		return "UnequipTechPart"
	case ActionUnlockHero:
		return "UnlockHero"
	case ActionUpgradeHeroLevel:
		return "UpgradeHeroLevel"
	case ActionUpgradeHeroRarity:
		return "UpgradeHeroRarity"
	case ActionUnlockPet:
		return "UnlockPet"
	case ActionUpgradePetLevel:
		return "UpgradePetLevel"
	case ActionUpgradePetRarity:
		return "UpgradePetRarity"
	case ActionUpgradeEquipment:
		return "UpgradeEquipment"
	case ActionUpgradeEquipmentToLevel:
		return "UpgradeEquipmentToLevel"
	case ActionDowngradeEquipment:
		return "DowngradeEquipment"
	case ActionMergeEquipment:
		return "MergeEquipment"
	case ActionSplitEquipment:
		return "SplitEquipment"
	case ActionUpgradeTechPart:
		return "UpgradeTechPart"
	case ActionUpgradeTechPartToLevel:
		return "UpgradeTechPartToLevel"
	case ActionDowngradeTechPart:
		return "DowngradeTechPart"
	case ActionMergeTechPart:
		return "MergeTechPart"
	case ActionSplitTechPart:
		return "SplitTechPart"
	case ActionUpgradeTalentNormal:
		return "UpgradeTalentNormal"
	case ActionUpgradeTalentSpecial:
		return "UpgradeTalentSpecial"
	case ActionPlayCampaign:
		return "PlayCampaign"
	case ActionClaimCampaignReward:
		return "ClaimCampaignReward"
	case ActionPlayBossMode:
		return "PlayBossMode"
	case ActionClaimBossModeReward:
		return "ClaimBossModeReward"
	case ActionPlayDungeon:
		return "PlayDungeon"
	case ActionClaimDungeonReward:
		return "ClaimDungeonReward"
	case ActionClaimExploration:
		return "ClaimExploration"
	case ActionQuickExploration:
		return "QuickExploration"
	case ActionQuickExplorationAds:
		return "QuickExplorationAds"
	case ActionClaimDailyReward:
		return "ClaimDailyReward"
	case ActionClaimDailyRewardBonus:
		return "ClaimDailyRewardBonus"
	case ActionClaimDailyQuestReward:
		return "ClaimDailyQuestReward"
	case ActionClaimDailyQuestBonus:
		return "ClaimDailyQuestBonus"
	case ActionClaimWeeklyQuestReward:
		return "ClaimWeeklyQuestReward"
	case ActionClaimWeeklyQuestBonus:
		return "ClaimWeeklyQuestBonus"
	case ActionClaimAchievementReward:
		return "ClaimAchievementReward"
	case ActionClaimRookieLoginReward:
		return "ClaimRookieLoginReward"
	case ActionClaimQuest7DaysReward:
		return "ClaimQuest7DaysReward"
	case ActionClaimQuest7DaysBonus:
		return "ClaimQuest7DaysBonus"
	case ActionClaimMonthlyInAppReward:
		return "ClaimMonthlyInAppReward"
	case ActionBuyChapterPack:
		return "BuyChapterPack"
	case ActionBuyDailySale:
		return "BuyDailySale"
	case ActionOpenRareChestAds:
		return "OpenRareChestAds"
	case ActionOpenRareChestKey:
		return "OpenRareChestKey"
	case ActionOpenRareChestGem:
		return "OpenRareChestGem"
	case ActionOpenMythChestAds:
		return "OpenMythChestAds"
	case ActionOpenMythChestKey:
		return "OpenMythChestKey"
	case ActionOpenMythChestGem:
		return "OpenMythChestGem"
	case ActionOpen10MythChest:
		return "Open10MythChest"
	case ActionBuyGemPack:
		return "BuyGemPack"
	case ActionBuyGoldPack:
		return "BuyGoldPack"
	case ActionBuyEnergyPack:
		return "BuyEnergyPack"
	case ActionBuyDailyDiscount:
		return "BuyDailyDiscount"
	case ActionClaimDailyDiscountBonus:
		return "ClaimDailyDiscountBonus"
	case ActionBuyWeeklyDiscount:
		return "BuyWeeklyDiscount"
	case ActionClaimWeeklyDiscountBonus:
		return "ClaimWeeklyDiscountBonus"
	case ActionBuyMonthlyDiscount:
		return "BuyMonthlyDiscount"
	case ActionClaimMonthlyDiscountBonus:
		return "ClaimMonthlyDiscountBonus"
	case ActionBuyMonthlyCardNormal:
		return "BuyMonthlyCardNormal"
	case ActionBuyMonthlyCardSuper:
		return "BuyMonthlyCardSuper"
	case ActionClaimMonthlyCardNormalReward:
		return "ClaimMonthlyCardNormalReward"
	case ActionClaimMonthlyCardSuperReward:
		return "ClaimMonthlyCardSuperReward"
	case ActionBuyBattlePassNormal:
		return "BuyBattlePassNormal"
	case ActionBuyBattlePassSuper:
		return "BuyBattlePassSuper"
	case ActionClaimBattlePassFreeReward:
		return "ClaimBattlePassFreeReward"
	case ActionClaimBattlePassNormalReward:
		return "ClaimBattlePassNormalReward"
	case ActionClaimBattlePassSuperReward:
		return "ClaimBattlePassSuperReward"
	case ActionBuyLevelPass1Normal:
		return "BuyLevelPass1Normal"
	case ActionBuyLevelPass1Super:
		return "BuyLevelPass1Super"
	case ActionClaimLevelPass1FreeReward:
		return "ClaimLevelPass1FreeReward"
	case ActionClaimLevelPass1NormalReward:
		return "ClaimLevelPass1NormalReward"
	case ActionClaimLevelPass1SuperReward:
		return "ClaimLevelPass1SuperReward"
	case ActionBuyLevelPass2Normal:
		return "BuyLevelPass2Normal"
	case ActionBuyLevelPass2Super:
		return "BuyLevelPass2Super"
	case ActionClaimLevelPass2FreeReward:
		return "ClaimLevelPass2FreeReward"
	case ActionClaimLevelPass2NormalReward:
		return "ClaimLevelPass2NormalReward"
	case ActionClaimLevelPass2SuperReward:
		return "ClaimLevelPass2SuperReward"
	case ActionBuyCampaignPassNormal:
		return "BuyCampaignPassNormal"
	case ActionBuyCampaignPassSuper:
		return "BuyCampaignPassSuper"
	case ActionClaimCampaignPassFreeReward:
		return "ClaimCampaignPassFreeReward"
	case ActionClaimCampaignPassNormalReward:
		return "ClaimCampaignPassNormalReward"
	case ActionClaimCampaignPassSuperReward:
		return "ClaimCampaignPassSuperReward"
	case ActionBuyDungeonPassHard:
		return "BuyDungeonPassHard"
	case ActionBuyDungeonPassCrazy:
		return "BuyDungeonPassCrazy"
	case ActionClaimDungeonPassNormalReward:
		return "ClaimDungeonPassNormalReward"
	case ActionClaimDungeonPassHardReward:
		return "ClaimDungeonPassHardReward"
	case ActionClaimDungeonPassCrazyReward:
		return "ClaimDungeonPassCrazyReward"
	case ActionBuyGoldPiggy:
		return "BuyGoldPiggy"
	}
	return "Undefine"
}
