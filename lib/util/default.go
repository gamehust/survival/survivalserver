package util

import (
	"fmt"
	"reflect"
)

func Default(data interface{}) error {
	tp := reflect.TypeOf(data)
	if tp.Kind() != reflect.Ptr || tp.Elem().Kind() != reflect.Struct {
		return fmt.Errorf("invalid argument: Expected kind *struct, got %+v", tp)
	}
	t := tp.Elem()
	v := reflect.Indirect(reflect.ValueOf(data))
	for i := 0; i < v.NumField(); i++ {
		f := v.Field(i)
		if !f.CanSet() {
			continue
		}
		if f.Kind() == reflect.Struct {
			if err := Default(f.Addr().Interface()); err != nil {
				continue
			}
		}
		tag, found := t.Field(i).Tag.Lookup("default")
		if !found {
			continue
		}
		if err := UnmarshalText(tag, f.Addr().Interface()); err != nil {
			return fmt.Errorf("invalid default value [%+v] for [%+v.%+v]", tag, t, t.Field(i).Name)
		}
	}
	return nil
}

