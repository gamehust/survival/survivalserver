package util

import (
	"bytes"
	"os/exec"
	"strconv"
	"strings"
)

func GetCpuMemDisk() (int64, int64) {
	cmd := exec.Command("ps", "aux")
	var out bytes.Buffer
	cmd.Stdout = &out
	if err := cmd.Run(); err != nil {
		return 100, 100
	}
	var cpu, mem float64 = 0, 0
	for {
		psi, err := out.ReadString('\n')
		if err != nil {
			break
		}
		if strings.Contains(psi, "USER") && strings.Contains(psi, "PID") {
			continue
		}
		psis := strings.Fields(psi)
		cpui, _ := strconv.ParseFloat(psis[2], 64)
		memi, _ := strconv.ParseFloat(psis[3], 64)
		cpu += cpui
		mem += memi
	}
	return int64(cpu), int64(mem)
}
