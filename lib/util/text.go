package util

import (
	"encoding"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"
)

func UnmarshalText(text string, value interface{}) error {
	switch data := value.(type) {
	case *string:
		*data = text
	case *bool:
		if v, err := strconv.ParseBool(text); err != nil {
			return err
		} else {
			*data = v
		}
	case *int:
		if v, err := strconv.ParseInt(text, 10, strconv.IntSize); err != nil {
			return err
		} else {
			*data = int(v)
		}
	case *int8:
		if v, err := strconv.ParseInt(text, 10, 8); err != nil {
			return err
		} else {
			*data = int8(v)
		}
	case *int16:
		if v, err := strconv.ParseInt(text, 10, 16); err != nil {
			return err
		} else {
			*data = int16(v)
		}
	case *int32:
		if v, err := strconv.ParseInt(text, 10, 32); err != nil {
			return err
		} else {
			*data = int32(v)
		}
	case *int64:
		if v, err := strconv.ParseInt(text, 10, 64); err != nil {
			return err
		} else {
			*data = v
		}
	case *uint:
		if v, err := strconv.ParseUint(text, 10, strconv.IntSize); err != nil {
			return err
		} else {
			*data = uint(v)
		}
	case *uint8:
		if v, err := strconv.ParseUint(text, 10, 8); err != nil {
			return err
		} else {
			*data = uint8(v)
		}
	case *uint16:
		if v, err := strconv.ParseUint(text, 10, 16); err != nil {
			return err
		} else {
			*data = uint16(v)
		}
	case *uint32:
		if v, err := strconv.ParseUint(text, 10, 32); err != nil {
			return err
		} else {
			*data = uint32(v)
		}
	case *uint64:
		if v, err := strconv.ParseUint(text, 10, 64); err != nil {
			return err
		} else {
			*data = v
		}
	case *time.Duration:
		if v, err := time.ParseDuration(text); err != nil {
			return err
		} else {
			*data = v
		}
	case encoding.TextUnmarshaler:
		if err := data.UnmarshalText([]byte(text)); err != nil {
			return err
		}
	default:
		t := reflect.TypeOf(value)
		if t.Kind() != reflect.Ptr {
			return fmt.Errorf("value must be a pointer")
		}
		switch t.Elem().Kind() {
		case reflect.Slice:
			arr := strings.Split(text, ",")
			s := reflect.MakeSlice(t.Elem(), len(arr), len(arr))
			for i := range arr {
				if err := UnmarshalText(arr[i], s.Index(i).Addr().Interface()); err != nil {
					return err
				}
			}
			reflect.ValueOf(value).Elem().Set(s)
		case reflect.Map:
			te := t.Elem()
			m := reflect.MakeMap(te)
			kt := te.Key()
			vt := te.Elem()
			arr := strings.Split(text, ",")
			for i := range arr {
				pair := strings.Split(arr[i], ":")
				if len(pair) != 2 {
					return fmt.Errorf("invalid map: %s", text)
				}
				kv := reflect.New(kt)
				if err := UnmarshalText(pair[0], kv.Interface()); err != nil {
					return err
				}
				vv := reflect.New(vt)
				if err := UnmarshalText(pair[1], vv.Interface()); err != nil {
					return err
				}
				m.SetMapIndex(kv.Elem(), vv.Elem())
			}
			reflect.ValueOf(value).Elem().Set(m)
		default:
			return fmt.Errorf("unsupported type: %+v", t)
		}
	}
	return nil
}

