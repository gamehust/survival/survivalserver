package util

import (
	"encoding/json"
	"fmt"
)

func PrettyJSON(obj interface{}) string {
	str, _ := json.MarshalIndent(obj, "", "\t")
	return fmt.Sprintf("%s", str)
}

func CompactJSON(obj interface{}) string {
	str, _ := json.Marshal(obj)
	return fmt.Sprintf("%s", str)
}
