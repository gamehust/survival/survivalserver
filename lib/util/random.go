package util

import "math/rand"

func RandomInt(values []int64, percents []int64) int64 {
	if len(values) < 1 && len(values) != len(percents) {
		return 0
	}
	var totalPercent int64 = 0
	for _, percent := range percents {
		totalPercent += percent
	}
	randomPercent := rand.Int63n(totalPercent)
	var nextPercent int64 = 0
	for i, percent := range percents {
		nextPercent += percent
		if randomPercent <= nextPercent {
			return values[i]
		}
	}
	return 0
}
