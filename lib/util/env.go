package util

import (
	"fmt"
	"reflect"
	"syscall"
)

func GetEnvironment(key string, value interface{}) error {
	envValue, found := syscall.Getenv(key)
	if !found {
		return nil
	}
	if err := UnmarshalText(envValue, value); err != nil {
		return fmt.Errorf("invalid environment: %s=%s", key, envValue)
	} else {
		return nil
	}
}

func ParseEnvironment(env interface{}) error {
	tp := reflect.TypeOf(env)
	if tp.Kind() != reflect.Ptr || tp.Elem().Kind() != reflect.Struct {
		return fmt.Errorf("invalid argument: expected kind *struct, got %+v", tp)
	}
	t := tp.Elem()
	v := reflect.Indirect(reflect.ValueOf(env))
	for i := 0; i < v.NumField(); i++ {
		f := v.Field(i)
		if !f.CanSet() {
			continue
		}
		if f.Kind() == reflect.Struct {
			if err := ParseEnvironment(f.Addr().Interface()); err != nil {
				return nil
			}
			continue
		}
		tag, found := t.Field(i).Tag.Lookup("env")
		if !found {
			continue
		}
		if err := GetEnvironment(tag, f.Addr().Interface()); err != nil {
			return err
		}
	}
	return nil
}

