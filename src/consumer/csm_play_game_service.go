package consumer

import (
	"context"
	"golang.org/x/oauth2"
	"google.golang.org/api/games/v1"
	"google.golang.org/api/option"
	"time"
)

func ExchangePlayGamesAuthCode(playerId, authCode string) (string, string, error) {
	config := &oauth2.Config{
		ClientID:     "981967211575-jsnv05kfehjb6t8b1808p8nuooc41fig.apps.googleusercontent.com",
		ClientSecret: "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",
		Endpoint: oauth2.Endpoint{
			AuthURL:   "https://accounts.google.com/o/oauth2/auth",
			TokenURL:  "https://www.googleapis.com/oauth2/v4/token",
			AuthStyle: 0,
		},
		RedirectURL: "https://legend-of-survivor.firebaseapp.com/__/auth/handler",
		Scopes:      []string{games.GamesScope},
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	token, err := config.Exchange(ctx, authCode)
	if err != nil {
		return "", "", err
	}
	gamesService, err := games.NewService(ctx, option.WithTokenSource(config.TokenSource(ctx, token)))
	if err != nil {
		return "", "", err
	}
	player, err := gamesService.Players.Get(playerId).Do()
	if err != nil {
		return "", "", err
	}
	return player.DisplayName, player.PlayerId, nil
}
