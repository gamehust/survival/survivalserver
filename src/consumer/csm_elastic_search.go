package consumer

import (
	"Survival/lib/log"
	"Survival/src/models"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

func SearchUserLogs(u, t string) ([]*log.ESLogApi, error) {
	url := fmt.Sprintf("%s/survival-user-logs-%s/_search", models.ENVConfig.ElasticUri, t)
	reqBody := map[string]interface{}{
		"from": 0, "size": 1000,
		"query": map[string]interface{}{
			"match": map[string]interface{}{
				"u": u}},
		"sort": []interface{}{map[string]interface{}{
			"t": map[string]interface{}{
				"order": "desc"}}}}
	reqData, err := json.Marshal(reqBody)
	if err != nil {
		return nil, err
	}
	rsp, err := http.Post(url, "application/json", bytes.NewReader(reqData))
	if err != nil || rsp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("%s-%s", err, rsp.Status)
	}
	rspData, err := io.ReadAll(rsp.Body)
	if err != nil {
		return nil, err
	}
	var esRsp log.ESResponse
	if err = json.Unmarshal(rspData, &esRsp); err != nil {
		return nil, err
	}
	var objs []*log.ESLogApi
	for _, v := range esRsp.Hits.Hits {
		detail, _ := json.Marshal(v.Source.Detail)
		objs = append(objs, &log.ESLogApi{Time: time.UnixMilli(v.Source.Time).Format(time.RFC3339), Action: v.Source.GetAction(), Detail: string(detail)})
	}
	return objs, nil
}
