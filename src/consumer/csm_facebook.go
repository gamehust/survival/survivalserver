package consumer

import (
	"Survival/src/models"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func ValidateFacebookToken(accessToken string) (string, string, error) {
	fbUrl := fmt.Sprintf("https://graph.facebook.com/me?access_token=%s", accessToken)
	rsp, err := http.Get(fbUrl)
	if err != nil {
		return "", "", err
	}
	rawBody, err := ioutil.ReadAll(rsp.Body)
	defer rsp.Body.Close()
	if err != nil {
		return "", "", err
	}
	var fbRsp models.FacebookTokenResponse
	if err = json.Unmarshal(rawBody, &fbRsp); err != nil {
		return "", "", err
	} else if fbRsp.Error != nil {
		return "", "", fmt.Errorf("%+v", fbRsp.Error)
	}
	return fbRsp.Name, fbRsp.Id, nil
}
