package main

import (
	"Survival/src/models"
	"fmt"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap/zapcore"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"

	"Survival/lib/log"
	"Survival/src/context"
	"Survival/src/controller"
	"Survival/src/repository"
)

func main() {
	models.InitENVConfig()
	models.InitUserConfig()
	log.InitZapLog(models.ENVConfig.LogLevel)
	repository.InitMongoRepo(models.ENVConfig.MongoDBUri)
	context.SaveServerInfo()
	context.SaveUserConfig()
	context.StartMonitor()
	startHTTPServer(fmt.Sprintf("%s, %s", models.ENVConfig.ExternalIPAddr, models.ENVConfig.PrivateIPAddr),
		models.ENVConfig.ExternalPort, models.ENVConfig.TLSCertPath, models.ENVConfig.TLSKeyPath)
}

func startHTTPServer(host string, port int64, tlsPem, tlsKey string) {
	router := gin.New()
	router.Use(gin.Recovery())
	router.Use(cors.Default())
	if log.GetLogLevel() < zapcore.WarnLevel {
		router.Use(gin.Logger())
	}
	// cms
	controller.NewCMSController().AddRouter(router)
	controller.NewCMSApiController().AddRouter(router)
	// user
	controller.NewUserConfigController().AddRouter(router)
	controller.NewUserLoginController().AddRouter(router)
	controller.NewUserDataController().AddRouter(router)
	controller.NewUserProfileController().AddRouter(router)
	controller.NewUserAssetController().AddRouter(router)
	controller.NewUserPlayController().AddRouter(router)
	controller.NewUserShopController().AddRouter(router)
	controller.NewUserEventController().AddRouter(router)
	controller.NewUserCommonController().AddRouter(router)

	server := http.Server{
		Addr:    fmt.Sprintf(":%d", port),
		Handler: h2c.NewHandler(router, &http2.Server{})}
	log.ZapLog.Infof("Server started at: %s :%d", host, port)
	log.ZapLog.Fatal(server.ListenAndServeTLS(tlsPem, tlsKey))
}
