package controller

import (
	"Survival/src/consumer"
	"github.com/gin-gonic/gin"
	"net/http"
)

type CMSApiController struct{}

func NewCMSApiController() *CMSApiController {
	return &CMSApiController{}
}

func (c *CMSApiController) AddRouter(engine *gin.Engine) *CMSApiController {
	AddRouter(engine, []Router{
		{
			http.MethodGet,
			"/",
			c.DashBoard,
		}, {
			http.MethodGet,
			"/user-logs/:i/:t",
			c.UserLogs,
		},
	}, "/N23q8CymDu/")
	return c
}

func (c *CMSApiController) DashBoard(ginCtx *gin.Context) {

}

func (c *CMSApiController) UserLogs(ginCtx *gin.Context) {
	_userId, _time := ginCtx.Param("i"), ginCtx.Param("t")
	if _userId == "" || _time == "" {
		ginCtx.JSON(http.StatusBadRequest, "InvalidParam")
		return
	}
	esRsp, err := consumer.SearchUserLogs(_userId, _time)
	if err != nil {
		ginCtx.JSON(http.StatusInternalServerError, err)
		return
	}
	ginCtx.JSON(http.StatusOK, esRsp)
}
