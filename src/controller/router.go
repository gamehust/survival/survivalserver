package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type Router struct {
	Method      string
	Pattern     string
	HandlerFunc gin.HandlerFunc
}

func AddRouter(engine *gin.Engine, routes []Router, apiRoot string) {
	group := engine.Group(apiRoot)
	for _, route := range routes {
		switch route.Method {
		case http.MethodPost:
			group.POST(route.Pattern, route.HandlerFunc)
		case http.MethodGet:
			group.GET(route.Pattern, route.HandlerFunc)
		case http.MethodDelete:
			group.DELETE(route.Pattern, route.HandlerFunc)
		case http.MethodPatch:
			group.PATCH(route.Pattern, route.HandlerFunc)
		case http.MethodPut:
			group.PUT(route.Pattern, route.HandlerFunc)
		case http.MethodOptions:
			group.OPTIONS(route.Pattern, route.HandlerFunc)
		case http.MethodHead:
			group.HEAD(route.Pattern, route.HandlerFunc)
		}
	}
}
