package controller

import (
	"Survival/src/models"
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"Survival/src/repository"
)

type UserDataController struct{}

func NewUserDataController() *UserDataController {
	return &UserDataController{}
}

func (c *UserDataController) AddRouter(engine *gin.Engine) *UserDataController {
	AddRouter(engine, []Router{
		{
			http.MethodGet,
			"/:userId",
			c.GetUserData,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONUserProfile + "/:targetId",
			c.GetProfile,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONUserProfile,
			c.GetProfile,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONMailBox,
			c.GetMailBox,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONGiftCode,
			c.GetGiftCode,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONAsset1,
			c.GetAsset1,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONAsset2,
			c.GetAsset2,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONCurrency,
			c.GetCurrency,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONStone,
			c.GetStone,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONHero,
			c.GetHero,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONDesign,
			c.GetDesign,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONEquipment,
			c.GetEquipment,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONTechPart,
			c.GetTechPart,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONPetPiece,
			c.GetPetPiece,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONPet,
			c.GetPet,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONTalent,
			c.GetTalent,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONCampaign,
			c.GetCampaign,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONBossMode,
			c.GetBossMode,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONDungeon,
			c.GetDungeon,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONExploration,
			c.GetExploration,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONDailyReward,
			c.GetDailyReward,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONDailyQuest,
			c.GetDailyQuest,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONWeeklyQuest,
			c.GetWeeklyQuest,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONAchievement,
			c.GetAchievement,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONRookieLogin,
			c.GetRookieLogin,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONQuest7Days,
			c.GetQuest7Days,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONShop,
			c.GetShop,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONDiscount,
			c.GetDiscount,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONMonthlyCard,
			c.GetMonthlyCard,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONBattlePass,
			c.GetBattlePass,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONLevelPass1,
			c.GetLevelPass1,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONLevelPass2,
			c.GetLevelPass2,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONCampaignPass,
			c.GetCampaignPass,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONDungeonPass,
			c.GetDungeonPass,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONGoldPiggy,
			c.GetGoldPiggy,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONEventNewYear,
			c.GetEventNewYear,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONEventValentine,
			c.GetEventValentine,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONEventAprilFool,
			c.GetEventAprilFool,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONEventChildrenDay,
			c.GetEventChildrenDay,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONEventAutumnLeaves,
			c.GetEventAutumnLeaves,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONEventHalloween,
			c.GetEventHalloween,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONEventChristmas,
			c.GetEventChristmas,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONEventWorldCup,
			c.GetEventWorldCup,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONTimezone,
			c.GetTimezone,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONLatestPlay,
			c.GetLatestPlay,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONCreatedTime,
			c.GetCreatedTime,
		}, {
			http.MethodGet,
			"/:userId/" + models.UserDataBSONUpdatedTime,
			c.GetUpdatedTime,
		},
	}, "/d")
	return c
}

func (c *UserDataController) GetUserData(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	_ = userData.AddEnergy(time.Now().UnixMilli(), 0)
	ginCtx.JSON(http.StatusOK, userData)
}

func (c *UserDataController) GetProfile(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var targetUser *models.UserData
	targetId := ginCtx.Param("targetId")
	if targetId != "" && targetId != userId {
		targetUser, err = repository.GetUserData(ctx, targetId)
		if err != nil {
			ginCtx.Status(http.StatusNotFound)
			return
		}
	} else {
		targetUser = userData
	}

	rankGlobal, err := repository.CountUserRankGlobal(ctx, targetUser.Profile.Campaign, targetUser.Profile.KillEnemy, targetUser.Profile.PlayTime)
	if err != nil {
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	rankCountry, err := repository.CountUserRankCountry(ctx, targetUser.Profile.Country, targetUser.Profile.Campaign, targetUser.Profile.KillEnemy, targetUser.Profile.PlayTime)
	if err != nil {
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	if targetUser.Profile.RankGlobal != rankGlobal || targetUser.Profile.RankCountry != rankCountry {
		targetUser.Profile.RankGlobal = rankGlobal
		targetUser.Profile.RankCountry = rankCountry
		if _, err = repository.UpdateUserRanking(ctx, targetUser); err != nil {
			ginCtx.Status(http.StatusInternalServerError)
			return
		}
	}
	ginCtx.JSON(http.StatusOK, targetUser.Profile)
}

func (c *UserDataController) GetMailBox(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.MailBox)
}

func (c *UserDataController) GetGiftCode(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.GiftCode)
}

func (c *UserDataController) GetAsset1(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Asset1)
}

func (c *UserDataController) GetAsset2(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Asset2)
}

func (c *UserDataController) GetCurrency(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Currency)
}

func (c *UserDataController) GetStone(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Stone)
}

func (c *UserDataController) GetHero(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Hero)
}

func (c *UserDataController) GetDesign(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Design)
}

func (c *UserDataController) GetEquipment(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Equipment)
}

func (c *UserDataController) GetTechPart(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.TechPart)
}

func (c *UserDataController) GetPetPiece(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.PetPiece)
}

func (c *UserDataController) GetPet(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Pet)
}

func (c *UserDataController) GetTalent(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Talent)
}

func (c *UserDataController) GetCampaign(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Campaign)
}

func (c *UserDataController) GetBossMode(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.BossMode)
}

func (c *UserDataController) GetDungeon(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Dungeon)
}

func (c *UserDataController) GetExploration(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Exploration)
}

func (c *UserDataController) GetDailyReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.DailyReward)
}

func (c *UserDataController) GetDailyQuest(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.DailyQuest)
}

func (c *UserDataController) GetWeeklyQuest(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.WeeklyQuest)
}

func (c *UserDataController) GetAchievement(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Achievement)
}

func (c *UserDataController) GetRookieLogin(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.RookieLogin)
}

func (c *UserDataController) GetQuest7Days(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Quest7Days)
}

func (c *UserDataController) GetShop(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Shop)
}

func (c *UserDataController) GetDiscount(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Discount)
}

func (c *UserDataController) GetMonthlyCard(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.MonthlyCard)
}

func (c *UserDataController) GetBattlePass(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.BattlePass)
}

func (c *UserDataController) GetLevelPass1(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.LevelPass1)
}

func (c *UserDataController) GetLevelPass2(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.LevelPass2)
}

func (c *UserDataController) GetCampaignPass(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.CampaignPass)
}

func (c *UserDataController) GetDungeonPass(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.DungeonPass)
}

func (c *UserDataController) GetGoldPiggy(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.GoldPiggy)
}

func (c *UserDataController) GetEventNewYear(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.EventNewYear)
}

func (c *UserDataController) GetEventValentine(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.EventValentine)
}

func (c *UserDataController) GetEventAprilFool(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.EventAprilFool)
}

func (c *UserDataController) GetEventChildrenDay(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.EventChildrenDay)
}

func (c *UserDataController) GetEventAutumnLeaves(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.EventAutumnLeaves)
}

func (c *UserDataController) GetEventHalloween(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.EventHalloween)
}

func (c *UserDataController) GetEventChristmas(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.EventChristmas)
}

func (c *UserDataController) GetEventWorldCup(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.EventWorldCup)
}

func (c *UserDataController) GetTimezone(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Timezone)
}

func (c *UserDataController) GetLatestPlay(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.LatestSaved)
}

func (c *UserDataController) GetCreatedTime(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.CreatedTime)
}

func (c *UserDataController) GetUpdatedTime(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.UpdatedTime)
}
