package controller

import (
	"Survival/src/models"
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"Survival/src/repository"
)

type UserCheatController struct{}

func NewUserCheatController() *UserCheatController {
	return &UserCheatController{}
}

func (c *UserCheatController) AddRouter(engine *gin.Engine) *UserCheatController {
	AddRouter(engine, []Router{
		{
			http.MethodPost,
			"/:userId/" + models.UserDataBSONCurrency,
			c.SetCurrency,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONStone,
			c.SetStone,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONHero,
			c.SetHero,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONDesign,
			c.SetDesign,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONEquipment,
			c.SetEquipment,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONTechPart,
			c.SetTechPart,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONPetPiece,
			c.SetPetPiece,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONPet,
			c.SetPet,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONTalent,
			c.SetTalent,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONCampaign,
			c.SetCampaign,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONBossMode,
			c.SetBossMode,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONDungeon,
			c.SetDungeon,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONExploration,
			c.SetExploration,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONDailyReward,
			c.SetDailyReward,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONDailyQuest,
			c.SetDailyQuest,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONWeeklyQuest,
			c.SetWeeklyQuest,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONAchievement,
			c.SetAchievement,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONRookieLogin,
			c.SetRookieLogin,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONQuest7Days,
			c.SetQuest7Days,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONShop,
			c.SetShop,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONDiscount,
			c.SetDiscount,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONMonthlyCard,
			c.SetMonthlyCard,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONBattlePass,
			c.SetBattlePass,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONLevelPass1,
			c.SetLevelPass1,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONLevelPass2,
			c.SetLevelPass2,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONCampaignPass,
			c.SetCampaignPass,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONDungeonPass,
			c.SetDungeonPass,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONGoldPiggy,
			c.SetGoldPiggy,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONEventNewYear,
			c.SetEventNewYear,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONEventValentine,
			c.SetEventValentine,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONEventAprilFool,
			c.SetEventAprilFool,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONEventChildrenDay,
			c.SetEventChildrenDay,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONEventAutumnLeaves,
			c.SetEventAutumnLeaves,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONEventHalloween,
			c.SetEventHalloween,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONEventChristmas,
			c.SetEventChristmas,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONEventWorldCup,
			c.SetEventWorldCup,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONTimezone,
			c.SetTimezone,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONLatestPlay,
			c.SetLatestPlay,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONCreatedTime,
			c.SetCreatedTime,
		}, {
			http.MethodPost,
			"/:userId/" + models.UserDataBSONUpdatedTime,
			c.SetUpdatedTime,
		},
	}, "/d")
	return c
}

func (c *UserCheatController) SetUserData(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	var userData models.UserData
	if err := ginCtx.ShouldBindJSON(&userData); err != nil {
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	oldData, err := repository.GetUserData(ctx, userId)
	if err != nil || oldData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userData.Id = oldData.Id
	userData.Info = oldData.Info
	userData.Profile.Id = oldData.Profile.Id
	if _, err = repository.UpsertUserData(ctx, oldData); err != nil {
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	ginCtx.Status(http.StatusOK)
}

func (c *UserCheatController) SetCurrency(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}

	ginCtx.JSON(http.StatusOK, userData.Currency)
}

func (c *UserCheatController) SetStone(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Stone)
}

func (c *UserCheatController) SetHero(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Hero)
}

func (c *UserCheatController) SetDesign(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Design)
}

func (c *UserCheatController) SetEquipment(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Equipment)
}

func (c *UserCheatController) SetTechPart(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.TechPart)
}

func (c *UserCheatController) SetPetPiece(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.PetPiece)
}

func (c *UserCheatController) SetPet(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Pet)
}

func (c *UserCheatController) SetTalent(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Talent)
}

func (c *UserCheatController) SetCampaign(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Campaign)
}

func (c *UserCheatController) SetBossMode(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.BossMode)
}

func (c *UserCheatController) SetDungeon(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Dungeon)
}

func (c *UserCheatController) SetExploration(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Exploration)
}

func (c *UserCheatController) SetDailyReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.DailyReward)
}

func (c *UserCheatController) SetDailyQuest(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.DailyQuest)
}

func (c *UserCheatController) SetWeeklyQuest(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.WeeklyQuest)
}

func (c *UserCheatController) SetAchievement(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Achievement)
}

func (c *UserCheatController) SetRookieLogin(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.RookieLogin)
}

func (c *UserCheatController) SetQuest7Days(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Quest7Days)
}

func (c *UserCheatController) SetShop(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Shop)
}

func (c *UserCheatController) SetDiscount(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Discount)
}

func (c *UserCheatController) SetMonthlyCard(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.MonthlyCard)
}

func (c *UserCheatController) SetBattlePass(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.BattlePass)
}

func (c *UserCheatController) SetLevelPass1(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.LevelPass1)
}

func (c *UserCheatController) SetLevelPass2(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.LevelPass2)
}

func (c *UserCheatController) SetCampaignPass(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.CampaignPass)
}

func (c *UserCheatController) SetDungeonPass(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.DungeonPass)
}

func (c *UserCheatController) SetGoldPiggy(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.GoldPiggy)
}

func (c *UserCheatController) SetEventNewYear(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.EventNewYear)
}

func (c *UserCheatController) SetEventValentine(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.EventValentine)
}

func (c *UserCheatController) SetEventAprilFool(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.EventAprilFool)
}

func (c *UserCheatController) SetEventChildrenDay(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.EventChildrenDay)
}

func (c *UserCheatController) SetEventAutumnLeaves(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.EventAutumnLeaves)
}

func (c *UserCheatController) SetEventHalloween(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.EventHalloween)
}

func (c *UserCheatController) SetEventChristmas(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.EventChristmas)
}

func (c *UserCheatController) SetEventWorldCup(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.EventWorldCup)
}

func (c *UserCheatController) SetTimezone(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.Timezone)
}

func (c *UserCheatController) SetLatestPlay(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.LatestSaved)
}

func (c *UserCheatController) SetCreatedTime(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.CreatedTime)
}

func (c *UserCheatController) SetUpdatedTime(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, userData.UpdatedTime)
}
