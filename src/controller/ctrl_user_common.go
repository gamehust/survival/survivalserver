package controller

import (
	"Survival/lib/log"
	"Survival/src/models"
	"Survival/src/repository"
	"context"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http"
	"time"
)

type UserCommonController struct{}

func NewUserCommonController() *UserCommonController {
	return &UserCommonController{}
}

func (c *UserCommonController) AddRouter(engine *gin.Engine) *UserCommonController {
	AddRouter(engine, []Router{
		{
			http.MethodGet,
			"/:userId/rg",
			c.GetMainGlobalRanking,
		}, {
			http.MethodGet,
			"/:userId/rc",
			c.GetMainCountryRanking,
		}, {
			http.MethodPost,
			"/:userId/mb/:mailId",
			c.ClaimMailBox,
		}, {
			http.MethodPost,
			"/:userId/gc/:code",
			c.ClaimGiftCode,
		}, {
			http.MethodPost,
			"/:userId/ws",
			c.WebSocket,
		},
	}, "/m")
	return c
}

func (c *UserCommonController) GetMainGlobalRanking(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ranking, err := repository.GetGlobalRanking(ctx)
	if err != nil && err != mongo.ErrNoDocuments {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	if ranking == nil || time.Since(time.UnixMilli(ranking.Time)) > 10*time.Minute {
		if ues, err := repository.CalculateGlobalRanking(ctx); err != nil {
			log.ZapLog.Error(err)
			ginCtx.Status(http.StatusInternalServerError)
			return
		} else {
			log.ZapLog.Debug(userData.Id, log.ActionGetMainGlobalRanking)
			log.ESLogSave(userData.Id, log.ActionGetMainGlobalRanking, nil)
			ginCtx.JSON(http.StatusOK, ues)
			return
		}
	}
	log.ZapLog.Debug(userData.Id, log.ActionGetMainGlobalRanking)
	log.ESLogSave(userData.Id, log.ActionGetMainGlobalRanking, nil)
	ginCtx.JSON(http.StatusOK, ranking.Pfs)
}

func (c *UserCommonController) GetMainCountryRanking(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ranking, err := repository.GetCountryRanking(ctx, userData.Profile.Country)
	if err != nil && err != mongo.ErrNoDocuments {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	if ranking == nil || time.Since(time.UnixMilli(ranking.Time)) > 10*time.Minute {
		if ues, err := repository.CalculateCountryRanking(ctx, userData.Profile.Country); err != nil {
			log.ZapLog.Error(err)
			ginCtx.Status(http.StatusInternalServerError)
			return
		} else {
			log.ZapLog.Debug(userData.Id, log.ActionGetMainCountryRanking, userData.Profile.Country)
			log.ESLogSave(userData.Id, log.ActionGetMainCountryRanking, userData.Profile.Country)
			ginCtx.JSON(http.StatusOK, ues)
			return
		}
	}
	log.ZapLog.Debug(userData.Id, log.ActionGetMainCountryRanking, userData.Profile.Country)
	log.ESLogSave(userData.Id, log.ActionGetMainCountryRanking, userData.Profile.Country)
	ginCtx.JSON(http.StatusOK, ranking.Pfs)
}

func (c *UserCommonController) ClaimMailBox(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	mailId := ginCtx.Param("mailId")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.MailBox == nil || userData.MailBox[mailId] == nil || userData.MailBox[mailId].Reward {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.MailBox[mailId].Reward = true
	for k, v := range userData.MailBox[mailId].Gift {
		switch k {
		case models.GiftNameEnergy:
			_ = userData.AddEnergy(time.Now().UnixMilli(), v)
		case models.GiftNameGem:
			_ = userData.AddGem(v)
		case models.GiftNameGold:
			_ = userData.AddGold(v)
		case models.GiftNameRareChest:
			_ = userData.AddKeyRareChest(v)
		case models.GiftNameMythChest:
			_ = userData.AddKeyMythChest(v)
		case models.GiftNameRevivalCoin:
			_ = userData.AddRevivalCoin(v)
		}
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimMailBox, userData.MailBox[mailId].Gift)
	log.ESLogSave(userData.Id, log.ActionClaimMailBox, userData.MailBox[mailId].Gift)
	ginCtx.Status(http.StatusOK)
}

func (c *UserCommonController) ClaimGiftCode(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	code := ginCtx.Param("code")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	giftcode := models.USERConfig.CFGiftCode[code]
	if giftcode == nil || giftcode.StartTime > time.Now().UnixMilli() || (giftcode.EndTime != 0 && giftcode.EndTime < time.Now().UnixMilli()) || (userData.GiftCode != nil && userData.GiftCode[code] != 0) {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	if userData.GiftCode == nil {
		userData.GiftCode = map[string]int64{}
	}
	userData.GiftCode[code] = time.Now().UnixMilli()
	for k, v := range giftcode.Reward {
		switch k {
		case models.GiftNameEnergy:
			_ = userData.AddEnergy(time.Now().UnixMilli(), v)
		case models.GiftNameGem:
			_ = userData.AddGem(v)
		case models.GiftNameGold:
			_ = userData.AddGold(v)
		case models.GiftNameRareChest:
			_ = userData.AddKeyRareChest(v)
		case models.GiftNameMythChest:
			_ = userData.AddKeyMythChest(v)
		case models.GiftNameRevivalCoin:
			_ = userData.AddRevivalCoin(v)
		}
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimGiftCode, giftcode.Reward)
	log.ESLogSave(userData.Id, log.ActionClaimGiftCode, giftcode.Reward)
	ginCtx.JSON(http.StatusOK, giftcode.Reward)
}

func (c *UserCommonController) WebSocket(ginCtx *gin.Context) {

}
