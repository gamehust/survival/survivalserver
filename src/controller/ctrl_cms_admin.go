package controller

import (
	"Survival/lib/log"
	"github.com/gin-gonic/gin"
	"html/template"
	"net/http"
)

type CMSController struct{}

func NewCMSController() *CMSController {
	return &CMSController{}
}

func (c *CMSController) AddRouter(engine *gin.Engine) *CMSController {
	engine.Static("/TfxK1RtfG5/", "./TfxK1RtfG5")
	AddRouter(engine, []Router{
		{
			http.MethodGet,
			"/",
			c.DashBoard,
		}, {
			http.MethodGet,
			"/user-config",
			c.UserConfig,
		}, {
			http.MethodGet,
			"/user-logs",
			c.UserLogs,
		},
	}, "/Z8r3PfA1nE")
	return c
}

func (c *CMSController) DashBoard(ginCtx *gin.Context) {
	templates := template.Must(template.ParseFiles("./TfxK1RtfG5/dashboard/dashboard.html"))
	if err := templates.ExecuteTemplate(ginCtx.Writer, "dashboard.html", nil); err != nil {
		log.ZapLog.Error(err)
		ginCtx.String(http.StatusInternalServerError, err.Error())
	}
}

func (c *CMSController) UserConfig(ginCtx *gin.Context) {
	templates := template.Must(template.ParseFiles("./TfxK1RtfG5/user-config/user-config.html"))
	if err := templates.ExecuteTemplate(ginCtx.Writer, "user-config.html", nil); err != nil {
		log.ZapLog.Error(err)
		ginCtx.String(http.StatusInternalServerError, err.Error())
	}
}

func (c *CMSController) UserLogs(ginCtx *gin.Context) {
	templates := template.Must(template.ParseFiles("./TfxK1RtfG5/user-logs/user-logs.html"))
	if err := templates.ExecuteTemplate(ginCtx.Writer, "user-logs.html", nil); err != nil {
		log.ZapLog.Error(err)
		ginCtx.String(http.StatusInternalServerError, err.Error())
	}
}
