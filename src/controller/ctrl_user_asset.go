package controller

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"

	"Survival/lib/log"
	"Survival/src/models"
	"Survival/src/repository"
)

type UserAssetController struct{}

func NewUserAssetController() *UserAssetController {
	return &UserAssetController{}
}

func (c *UserAssetController) AddRouter(engine *gin.Engine) *UserAssetController {
	AddRouter(engine, []Router{
		{
			http.MethodPost,
			"/:userId/a1",
			c.SaveAsset1,
		}, {
			http.MethodPost,
			"/:userId/a2",
			c.SaveAsset2,
		}, {
			http.MethodPost,
			"/:userId/hr/u/:index",
			c.UnlockHero,
		}, {
			http.MethodPost,
			"/:userId/hr/l/:index",
			c.UpgradeHeroLevel,
		}, {
			http.MethodPost,
			"/:userId/hr/t/:index",
			c.UpgradeHeroRarity,
		}, {
			http.MethodPost,
			"/:userId/pe/u/:index",
			c.UnlockPet,
		}, {
			http.MethodPost,
			"/:userId/pe/l/:index",
			c.UpgradePetLevel,
		}, {
			http.MethodPost,
			"/:userId/pe/t/:index",
			c.UpgradePetRarity,
		}, {
			http.MethodPost,
			"/:userId/eq/u/:id",
			c.UpgradeEquipment,
		}, {
			http.MethodPost,
			"/:userId/eq/u/:id/:level",
			c.UpgradeEquipmentToLevel,
		}, {
			http.MethodPost,
			"/:userId/eq/d/:id",
			c.DowngradeEquipment,
		}, {
			http.MethodPost,
			"/:userId/eq/m",
			c.MergeEquipment,
		}, {
			http.MethodPost,
			"/:userId/eq/s",
			c.SplitEquipment,
		}, {
			http.MethodPost,
			"/:userId/tp/u/:id",
			c.UpgradeTechPart,
		}, {
			http.MethodPost,
			"/:userId/tp/u/:id/:level",
			c.UpgradeTechPartToLevel,
		}, {
			http.MethodPost,
			"/:userId/tp/d/:id",
			c.DowngradeTechPart,
		}, {
			http.MethodPost,
			"/:userId/tp/m",
			c.MergeTechPart,
		}, {
			http.MethodPost,
			"/:userId/tp/s",
			c.SplitTechPart,
		}, {
			http.MethodPost,
			"/:userId/tl/n",
			c.UpgradeTalentNormal,
		}, {
			http.MethodPost,
			"/:userId/tl/s",
			c.UpgradeTalentSpecial,
		},
	}, "/a")
	return c
}

func (c *UserAssetController) SaveAsset1(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var a1 map[string]float64
	if err = ginCtx.ShouldBindJSON(a1); err != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	if userData.Asset1 == nil {
		userData.Asset1 = make(map[string]float64)
	}
	for k, v := range a1 {
		userData.Asset1[k] = v
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionSaveAsset1, a1)
	log.ESLogSave(userData.Id, log.ActionSaveAsset1, a1)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) SaveAsset2(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var a2 map[string]string
	if err = ginCtx.ShouldBindJSON(a2); err != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	if userData.Asset2 == nil {
		userData.Asset2 = make(map[string]string)
	}
	for k, v := range a2 {
		userData.Asset2[k] = v
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionSaveAsset2, a2)
	log.ESLogSave(userData.Id, log.ActionSaveAsset2, a2)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) UnlockHero(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	heroIndex, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	gemUnlock, _ := models.USERConfig.CFHero.GemUnlock[heroIndex]
	stoneUnlock, _ := models.USERConfig.CFHero.StoneUpgradeRarity[models.RarityNormal]
	if gemUnlock == 0 {
		log.ZapLog.Error("hero is not configurated")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	hero := userData.Hero[heroIndex]
	if (hero != nil && hero.Level != 0) ||
		userData.AddGem(-gemUnlock) != nil ||
		userData.AddStone(heroIndex, -stoneUnlock) != nil {
		log.ZapLog.Error("not enough currency")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	hero = &models.Hero{Index: heroIndex, Rarity: models.RarityNormal, Level: 1}
	userData.Hero[heroIndex] = hero
	userData.ChangeAchievement(models.AchievementIndexUnlockHero, 1)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionUnlockHero, hero)
	log.ESLogSave(userData.Id, log.ActionUnlockHero, hero)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) UpgradeHeroLevel(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	heroIndex, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	hero := userData.Hero[heroIndex]
	if hero == nil || hero.Level == 0 ||
		models.USERConfig.CFHero.RarityUnlockLevel[hero.Level+1] > hero.Rarity ||
		userData.AddGold(-models.USERConfig.CFHero.GoldUpgradeLevel[hero.Level+1]) != nil {
		log.ZapLog.Warn("not enough currency")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	hero.Level += 1
	userData.SetAchievement(models.AchievementIndexReachHeroLevel, hero.Level)
	if userData.Profile.Hero.Index == hero.Index {
		userData.Profile.Hero = hero
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionUpgradeHeroLevel, hero)
	log.ESLogSave(userData.Id, log.ActionUpgradeHeroLevel, hero)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) UpgradeHeroRarity(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	heroIndex, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	hero := userData.Hero[heroIndex]
	if hero == nil || hero.Level == 0 || hero.Rarity == models.RarityImmortal5 ||
		userData.AddGem(-models.USERConfig.CFHero.GemUpgradeRarity[hero.Rarity+1]) != nil ||
		userData.AddStone(hero.Index, -models.USERConfig.CFHero.StoneUpgradeRarity[hero.Rarity+1]) != nil {
		log.ZapLog.Warn("not enough currency")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	hero.Rarity += 1
	userData.SetAchievement(models.AchievementIndexReachHeroRarity, hero.Rarity)
	if userData.Profile.Hero.Index == hero.Index {
		userData.Profile.Hero = hero
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionUpgradeHeroRarity, hero)
	log.ESLogSave(userData.Id, log.ActionUpgradeHeroRarity, hero)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) UnlockPet(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	petIndex, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	gemUnlock, _ := models.USERConfig.CFPet.GemUnlock[petIndex]
	stoneUnlock, _ := models.USERConfig.CFPet.PetPieceUpgradeRarity[models.RarityNormal]
	if gemUnlock == 0 {
		log.ZapLog.Warn("pet is not configurated")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	pet := userData.Pet[petIndex]
	if (pet != nil && pet.Level != 0) ||
		userData.AddGem(-gemUnlock) != nil ||
		userData.AddPetPiece(petIndex, -stoneUnlock) != nil {
		log.ZapLog.Warn("not enough currency")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.Pet[petIndex] = &models.Pet{Index: petIndex, Rarity: models.RarityNormal, Level: 1}
	userData.ChangeAchievement(models.AchievementIndexUnlockPet, 1)
	userData.SetAchievement(models.AchievementIndexReachPetLevel, 1)
	userData.SetAchievement(models.AchievementIndexReachPetRarity, 1)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionUnlockPet, pet)
	log.ESLogSave(userData.Id, log.ActionUnlockPet, pet)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) UpgradePetLevel(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	petIndex, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	pet := userData.Pet[petIndex]
	if pet == nil || pet.Level == 0 ||
		models.USERConfig.CFPet.RarityUnlockLevel[pet.Level+1] > pet.Rarity ||
		userData.AddGold(-models.USERConfig.CFPet.GoldUpgradeLevel[pet.Level+1]) != nil {
		log.ZapLog.Warn("not enough currency")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	pet.Level += 1
	userData.SetAchievement(models.AchievementIndexReachPetLevel, pet.Level)
	if userData.Profile.Pet != nil {
		for _, pe := range userData.Profile.Pet {
			if pe.Index == pet.Index {
				pe.Level = pet.Level
				break
			}
		}
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionUpgradePetLevel, pet)
	log.ESLogSave(userData.Id, log.ActionUpgradePetLevel, pet)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) UpgradePetRarity(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	petIndex, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	pet := userData.Pet[petIndex]
	if pet == nil || pet.Level == 0 || pet.Rarity == models.RarityImmortal5 ||
		models.USERConfig.CFPet.RarityUnlockLevel[pet.Level+1] == pet.Rarity ||
		userData.AddGem(-models.USERConfig.CFPet.GemUpgradeRarity[pet.Rarity+1]) != nil ||
		userData.AddPetPiece(pet.Index, -models.USERConfig.CFPet.PetPieceUpgradeRarity[pet.Rarity+1]) != nil {
		log.ZapLog.Warn("not enough currency")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	pet.Rarity += 1
	userData.SetAchievement(models.AchievementIndexReachPetRarity, pet.Rarity)
	if userData.Profile.Pet != nil {
		for _, pe := range userData.Profile.Pet {
			if pe.Index == pet.Index {
				pe.Rarity = pet.Rarity
				break
			}
		}
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionUpgradePetRarity, pet)
	log.ESLogSave(userData.Id, log.ActionUpgradePetRarity, pet)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) UpgradeEquipment(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	equipmentId, _ := strconv.ParseInt(ginCtx.Param("id"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	equipment := userData.Equipment[equipmentId]
	if equipment == nil || equipment.Level >= 220 ||
		models.USERConfig.CFEquipment.RarityUnlockLevel[equipment.Level+1] > equipment.Rarity {
		log.ZapLog.Warn("invalid equipment data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	goldUpgrade := models.USERConfig.CFEquipment.GoldUpgradeLevel[equipment.Level+1]
	designUpgrade := models.USERConfig.CFEquipment.DesignUpgradeLevel[equipment.Level+1]
	if userData.AddGold(-goldUpgrade) != nil ||
		userData.AddDesign(equipment.Type, -designUpgrade) != nil {
		log.ZapLog.Warn("not enough currency")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	equipment.Level += 1
	userData.ChangeDailyQuest(models.DailyQuestIndexUpgradeEquipment, 1)
	userData.SetAchievement(models.AchievementIndexReachEquipmentLevel, equipment.Level)
	if userData.Profile.Equipment != nil {
		for _, eq := range userData.Profile.Equipment {
			if eq.Id == equipment.Id {
				eq.Level = equipment.Level
				break
			}
		}
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionUpgradeEquipment, equipment)
	log.ESLogSave(userData.Id, log.ActionUpgradeEquipment, equipment)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) UpgradeEquipmentToLevel(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	equipmentId, _ := strconv.ParseInt(ginCtx.Param("id"), 10, 64)
	targetLevel, _ := strconv.ParseInt(ginCtx.Param("level"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	equipment := userData.Equipment[equipmentId]
	if equipment == nil || equipment.Level >= targetLevel || targetLevel > 220 ||
		models.USERConfig.CFEquipment.RarityUnlockLevel[targetLevel] > equipment.Rarity {
		log.ZapLog.Warn("invalid eqpt data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	goldUpgrade := models.USERConfig.CFEquipment.TotalGoldUpgradeLevel[targetLevel] - models.USERConfig.CFEquipment.TotalGoldUpgradeLevel[equipment.Level]
	designUpgrade := models.USERConfig.CFEquipment.TotalDesignUpgradeLevel[targetLevel] - models.USERConfig.CFEquipment.TotalDesignUpgradeLevel[equipment.Level]
	if userData.AddGold(-goldUpgrade) != nil ||
		userData.AddDesign(equipment.Type, -designUpgrade) != nil {
		log.ZapLog.Warn("not enough currency")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	equipment.Level = targetLevel
	userData.ChangeDailyQuest(models.DailyQuestIndexUpgradeEquipment, 1)
	userData.SetAchievement(models.AchievementIndexReachEquipmentLevel, equipment.Level)
	if userData.Profile.Equipment != nil {
		for _, eq := range userData.Profile.Equipment {
			if eq.Id == equipment.Id {
				eq.Level = equipment.Level
				break
			}
		}
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionUpgradeEquipmentToLevel, equipment)
	log.ESLogSave(userData.Id, log.ActionUpgradeEquipmentToLevel, equipment)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) DowngradeEquipment(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	equipmentId, _ := strconv.ParseInt(ginCtx.Param("id"), 10, 64)
	equipment := userData.Equipment[equipmentId]
	if equipment == nil || equipment.Level == 1 {
		log.ZapLog.Warn("invalid eqpt data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	_ = userData.AddGold(models.USERConfig.CFEquipment.TotalGoldUpgradeLevel[equipment.Level] * 75 / 100)
	_ = userData.AddDesign(equipment.Type, models.USERConfig.CFEquipment.TotalDesignUpgradeLevel[equipment.Level]*75/100)
	equipment.Level = 1
	if userData.Profile.Equipment != nil {
		for _, eq := range userData.Profile.Equipment {
			if eq.Id == equipment.Id {
				eq.Level = equipment.Level
				break
			}
		}
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionDowngradeEquipment, equipment)
	log.ESLogSave(userData.Id, log.ActionDowngradeEquipment, equipment)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) MergeEquipment(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var merges []models.MergeEquipmentRequest
	if err = ginCtx.ShouldBindJSON(&merges); err != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	for _, merge := range merges {
		if len(merge.OldEquipments) < 2 {
			log.ZapLog.Warn("invalid eqpt data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		oldEquipment1, oldEquipment2 := userData.Equipment[merge.OldEquipments[0]], userData.Equipment[merge.OldEquipments[1]]
		if oldEquipment1 == nil || oldEquipment2 == nil {
			log.ZapLog.Warn("invalid eqpt data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		switch oldEquipment1.Rarity {
		case models.RarityNormal, models.RarityGood, models.RarityBetter,
			models.RarityExcellent1, models.RarityEpic2, models.RarityLegendary3:
			if len(merge.OldEquipments) != 3 {
				log.ZapLog.Warn("invalid eqpt data")
				ginCtx.Status(http.StatusBadRequest)
				return
			}
			oldEquipment3 := userData.Equipment[merge.OldEquipments[2]]
			if oldEquipment2 == nil || oldEquipment3 == nil ||
				oldEquipment2.Type != oldEquipment1.Type || oldEquipment3.Type != oldEquipment1.Type ||
				oldEquipment2.Index != oldEquipment1.Index || oldEquipment3.Index != oldEquipment1.Index ||
				oldEquipment2.Rarity != oldEquipment1.Rarity || oldEquipment3.Rarity != oldEquipment1.Rarity {
				log.ZapLog.Warn("invalid eqpt data")
				ginCtx.Status(http.StatusBadRequest)
				return
			}
			_ = userData.AddDesign(oldEquipment3.Type, models.USERConfig.CFEquipment.TotalDesignUpgradeLevel[oldEquipment3.Level])
			_ = userData.AddGold(models.USERConfig.CFEquipment.TotalGoldUpgradeLevel[oldEquipment3.Level])
			_ = userData.RemoveEquipment(oldEquipment3.Id)
		case models.RarityExcellent:
			if oldEquipment2.Type != oldEquipment1.Type || oldEquipment2.Rarity != models.RarityExcellent {
				log.ZapLog.Warn("invalid eqpt data")
				ginCtx.Status(http.StatusBadRequest)
				return
			}
		case models.RarityEpic, models.RarityEpic1:
			if oldEquipment2.Type != oldEquipment1.Type || oldEquipment2.Rarity != models.RarityEpic {
				log.ZapLog.Warn("invalid eqpt data")
				ginCtx.Status(http.StatusBadRequest)
				return
			}
		case models.RarityLegendary, models.RarityLegendary1, models.RarityLegendary2:
			if oldEquipment2.Type != oldEquipment1.Type || oldEquipment2.Rarity != models.RarityLegendary {
				log.ZapLog.Warn("invalid eqpt data")
				ginCtx.Status(http.StatusBadRequest)
				return
			}
		case models.RarityImmortal, models.RarityImmortal1, models.RarityImmortal2,
			models.RarityImmortal3, models.RarityImmortal4:
			if oldEquipment2.Type != oldEquipment1.Type || oldEquipment2.Rarity != models.RarityImmortal {
				log.ZapLog.Warn("invalid eqpt data")
				ginCtx.Status(http.StatusBadRequest)
				return
			}
		default:
			log.ZapLog.Warn("invalid eqpt data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		oldEquipment1.Rarity += 1
		_ = userData.AddDesign(oldEquipment2.Type, models.USERConfig.CFEquipment.TotalDesignUpgradeLevel[oldEquipment2.Level])
		_ = userData.AddGold(models.USERConfig.CFEquipment.TotalGoldUpgradeLevel[oldEquipment2.Level])
		_ = userData.RemoveEquipment(oldEquipment2.Id)
		userData.SetAchievement(models.AchievementIndexReachEquipmentRarity, oldEquipment1.Rarity)
		if userData.Profile.Equipment != nil {
			for _, eq := range userData.Profile.Equipment {
				if eq.Id == oldEquipment1.Id {
					eq.Rarity = oldEquipment1.Rarity
					break
				}
			}
		}
	}
	userData.ChangeWeeklyQuest(models.WeeklyQuestIndexMergeEquipment, int64(len(merges)))
	userData.ChangeAchievement(models.AchievementIndexMergeEquipment, int64(len(merges)))
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionMergeEquipment, merges)
	log.ESLogSave(userData.Id, log.ActionMergeEquipment, merges)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) SplitEquipment(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var merge models.MergeEquipmentRequest
	if err = ginCtx.ShouldBindJSON(&merge); err != nil ||
		len(merge.NewEquipments) != 2 || merge.NewEquipments[0] == nil || merge.NewEquipments[1] == nil ||
		userData.Equipment[merge.NewEquipments[0].Id] == nil {
		log.ZapLog.Warn("invalid eqpt data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	newEquipment1, newEquipment2 := merge.NewEquipments[0], merge.NewEquipments[1]
	oldEquipment := userData.Equipment[newEquipment1.Id]
	if oldEquipment.Type != newEquipment1.Type || oldEquipment.Type != newEquipment2.Type ||
		oldEquipment.Index != newEquipment1.Index || oldEquipment.Index != newEquipment2.Index ||
		oldEquipment.Rarity-1 != newEquipment1.Rarity ||
		oldEquipment.Level != newEquipment1.Level || newEquipment2.Level != 1 {
		log.ZapLog.Warn("invalid eqpt data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	switch oldEquipment.Rarity {
	case models.RarityExcellent1:
		if newEquipment2.Rarity != models.RarityExcellent {
			log.ZapLog.Warn("invalid eqpt data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case models.RarityEpic1, models.RarityEpic2:
		if newEquipment2.Rarity != models.RarityEpic {
			log.ZapLog.Warn("invalid eqpt data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case models.RarityLegendary1, models.RarityLegendary2, models.RarityLegendary3:
		if newEquipment2.Rarity != models.RarityLegendary {
			log.ZapLog.Warn("invalid eqpt data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case models.RarityImmortal1, models.RarityImmortal2, models.RarityImmortal3,
		models.RarityImmortal4, models.RarityImmortal5:
		if newEquipment2.Rarity != models.RarityImmortal {
			log.ZapLog.Warn("invalid eqpt data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	default:
		log.ZapLog.Warn("invalid eqpt data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	oldEquipment.Rarity -= 1
	_ = userData.AddEquipment(newEquipment2)
	if userData.Profile.Equipment != nil {
		for _, eq := range userData.Profile.Equipment {
			if eq.Id == oldEquipment.Id {
				eq.Rarity = oldEquipment.Rarity
				break
			}
		}
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionSplitEquipment, merge)
	log.ESLogSave(userData.Id, log.ActionSplitEquipment, merge)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) UpgradeTechPart(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	techPartId, _ := strconv.ParseInt(ginCtx.Param("id"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	techPart := userData.TechPart[techPartId]
	if techPart == nil || techPart.Level >= 220 ||
		models.USERConfig.CFTechPart.RarityUnlockLevel[techPart.Level+1] > techPart.Rarity {
		log.ZapLog.Warn("invalid tech data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	goldUpgrade := models.USERConfig.CFTechPart.GoldUpgradeLevel[techPart.Level+1]
	designUpgrade := models.USERConfig.CFTechPart.TechUsedUpgradeLevel[techPart.Level+1]
	if userData.AddGold(-goldUpgrade) != nil ||
		userData.AddDesign(techPart.Type, -designUpgrade) != nil {
		log.ZapLog.Debug("not enough currency")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	techPart.Level += 1
	userData.SetAchievement(models.AchievementIndexReachTechPartLevel, techPart.Level)
	if userData.Profile.TechPart != nil {
		for _, tp := range userData.Profile.TechPart {
			if tp.Id == techPart.Id {
				tp.Level = techPart.Level
				break
			}
		}
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionUpgradeTechPart, techPart)
	log.ESLogSave(userData.Id, log.ActionUpgradeTechPart, techPart)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) UpgradeTechPartToLevel(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	techPartId, _ := strconv.ParseInt(ginCtx.Param("id"), 10, 64)
	targetLevel, _ := strconv.ParseInt(ginCtx.Param("level"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	techPart := userData.TechPart[techPartId]
	if techPart == nil || techPart.Level >= targetLevel || targetLevel > 220 ||
		models.USERConfig.CFTechPart.RarityUnlockLevel[targetLevel] > techPart.Rarity {
		log.ZapLog.Warn("invalid tech data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	goldUpgrade := models.USERConfig.CFTechPart.TotalGoldUpgradeLevel[targetLevel] - models.USERConfig.CFTechPart.TotalGoldUpgradeLevel[techPart.Level]
	designUpgrade := models.USERConfig.CFTechPart.TotalTechUsedUpgradeLevel[targetLevel] - models.USERConfig.CFTechPart.TotalTechUsedUpgradeLevel[techPart.Level]
	if userData.AddGold(-goldUpgrade) != nil ||
		userData.AddDesign(techPart.Type, -designUpgrade) != nil {
		log.ZapLog.Debug("not enough currency")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	techPart.Level = targetLevel
	userData.SetAchievement(models.AchievementIndexReachTechPartLevel, techPart.Level)
	if userData.Profile.TechPart != nil {
		for _, tp := range userData.Profile.TechPart {
			if tp.Id == techPart.Id {
				tp.Level = techPart.Level
				break
			}
		}
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionUpgradeTechPartToLevel, techPart)
	log.ESLogSave(userData.Id, log.ActionUpgradeTechPartToLevel, techPart)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) DowngradeTechPart(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	techPartId, _ := strconv.ParseInt(ginCtx.Param("id"), 10, 64)
	techPart := userData.TechPart[techPartId]
	if techPart == nil || techPart.Level == 1 {
		log.ZapLog.Warn("invalid tech data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	_ = userData.AddGold(models.USERConfig.CFTechPart.TotalGoldUpgradeLevel[techPart.Level] * 75 / 100)
	_ = userData.AddDesign(techPart.Type, models.USERConfig.CFTechPart.TotalTechUsedUpgradeLevel[techPart.Level]*75/100)
	techPart.Level = 1
	if userData.Profile.TechPart != nil {
		for _, tp := range userData.Profile.TechPart {
			if tp.Id == techPart.Id {
				tp.Level = techPart.Level
				break
			}
		}
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionDowngradeTechPart, techPart)
	log.ESLogSave(userData.Id, log.ActionDowngradeTechPart, techPart)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) MergeTechPart(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var merges []models.MergeTechPartRequest
	if err = ginCtx.ShouldBindJSON(&merges); err != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	for _, merge := range merges {
		if len(merge.OldTechParts) < 2 {
			log.ZapLog.Warn("invalid tech data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		oldTechPart1, oldTechPart2 := userData.TechPart[merge.OldTechParts[0]], userData.TechPart[merge.OldTechParts[1]]
		if oldTechPart1 == nil || oldTechPart2 == nil {
			log.ZapLog.Warn("invalid tech data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		switch oldTechPart1.Rarity {
		case models.RarityNormal, models.RarityGood, models.RarityBetter,
			models.RarityExcellent1, models.RarityEpic2, models.RarityLegendary3:
			if len(merge.OldTechParts) != 3 {
				log.ZapLog.Warn("invalid tech data")
				ginCtx.Status(http.StatusBadRequest)
				return
			}
			oldTechPart3 := userData.TechPart[merge.OldTechParts[2]]
			if oldTechPart2 == nil || oldTechPart3 == nil ||
				oldTechPart2.Type != oldTechPart1.Type || oldTechPart3.Type != oldTechPart1.Type ||
				oldTechPart2.Index != oldTechPart1.Index || oldTechPart3.Index != oldTechPart1.Index ||
				oldTechPart2.Rarity != oldTechPart1.Rarity || oldTechPart3.Rarity != oldTechPart1.Rarity {
				log.ZapLog.Warn("invalid tech data")
				ginCtx.Status(http.StatusBadRequest)
				return
			}
			_ = userData.AddTechUsed(oldTechPart3.Type, models.USERConfig.CFTechPart.TotalTechUsedUpgradeLevel[oldTechPart3.Level])
			_ = userData.AddGold(models.USERConfig.CFTechPart.TotalGoldUpgradeLevel[oldTechPart3.Level])
			_ = userData.RemoveTechPart(oldTechPart3.Id)
		case models.RarityExcellent:
			if oldTechPart2.Type != oldTechPart1.Type || oldTechPart2.Rarity != models.RarityExcellent {
				log.ZapLog.Warn("invalid tech data")
				ginCtx.Status(http.StatusBadRequest)
				return
			}
		case models.RarityEpic, models.RarityEpic1:
			if oldTechPart2.Type != oldTechPart1.Type || oldTechPart2.Rarity != models.RarityEpic {
				log.ZapLog.Warn("invalid tech data")
				ginCtx.Status(http.StatusBadRequest)
				return
			}
		case models.RarityLegendary, models.RarityLegendary1, models.RarityLegendary2:
			if oldTechPart2.Type != oldTechPart1.Type || oldTechPart2.Rarity != models.RarityLegendary {
				log.ZapLog.Warn("invalid tech data")
				ginCtx.Status(http.StatusBadRequest)
				return
			}
		case models.RarityImmortal, models.RarityImmortal1, models.RarityImmortal2,
			models.RarityImmortal3, models.RarityImmortal4:
			if oldTechPart2.Type != oldTechPart1.Type || oldTechPart2.Rarity != models.RarityImmortal {
				log.ZapLog.Warn("invalid tech data")
				ginCtx.Status(http.StatusBadRequest)
				return
			}
		default:
			log.ZapLog.Warn("invalid tech data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		oldTechPart1.Rarity += 1
		_ = userData.AddDesign(oldTechPart2.Type, models.USERConfig.CFTechPart.TotalTechUsedUpgradeLevel[oldTechPart2.Level])
		_ = userData.AddGold(models.USERConfig.CFTechPart.TotalGoldUpgradeLevel[oldTechPart2.Level])
		_ = userData.RemoveTechPart(oldTechPart2.Id)
		userData.SetAchievement(models.AchievementIndexReachTechPartRarity, oldTechPart1.Rarity)
		if userData.Profile.TechPart != nil {
			for _, tp := range userData.Profile.TechPart {
				if tp.Id == oldTechPart1.Id {
					tp.Rarity = oldTechPart1.Rarity
					break
				}
			}
		}
	}
	userData.ChangeWeeklyQuest(models.WeeklyQuestIndexMergeTechPart, int64(len(merges)))
	userData.ChangeAchievement(models.AchievementIndexMergeTechPart, int64(len(merges)))
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionMergeTechPart, merges)
	log.ESLogSave(userData.Id, log.ActionMergeTechPart, merges)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) SplitTechPart(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var merge models.MergeTechPartRequest
	if err = ginCtx.ShouldBindJSON(&merge); err != nil ||
		len(merge.NewTechParts) != 2 || merge.NewTechParts[0] == nil || merge.NewTechParts[1] == nil ||
		userData.TechPart[merge.NewTechParts[0].Id] == nil {
		log.ZapLog.Warn("invalid tech data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	newTechPart1, newTechPart2 := merge.NewTechParts[0], merge.NewTechParts[1]
	oldTechPart := userData.TechPart[newTechPart1.Id]
	if oldTechPart.Type != newTechPart1.Type || oldTechPart.Type != newTechPart2.Type ||
		oldTechPart.Index != newTechPart1.Index || oldTechPart.Index != newTechPart2.Index ||
		oldTechPart.Rarity-1 != newTechPart1.Rarity ||
		oldTechPart.Level != newTechPart1.Level || newTechPart2.Level != 1 {
		log.ZapLog.Warn("invalid tech data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	switch oldTechPart.Rarity {
	case models.RarityExcellent1:
		if newTechPart2.Rarity != models.RarityExcellent {
			log.ZapLog.Warn("invalid tech data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case models.RarityEpic1, models.RarityEpic2:
		if newTechPart2.Rarity != models.RarityEpic {
			log.ZapLog.Warn("invalid tech data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case models.RarityLegendary1, models.RarityLegendary2, models.RarityLegendary3:
		if newTechPart2.Rarity != models.RarityLegendary {
			log.ZapLog.Warn("invalid tech data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case models.RarityImmortal1, models.RarityImmortal2, models.RarityImmortal3,
		models.RarityImmortal4, models.RarityImmortal5:
		if newTechPart2.Rarity != models.RarityImmortal {
			log.ZapLog.Warn("invalid tech data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	default:
		log.ZapLog.Warn("invalid tech data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	oldTechPart.Rarity -= 1
	_ = userData.AddTechPart(newTechPart2)
	if userData.Profile.TechPart != nil {
		for _, tp := range userData.Profile.TechPart {
			if tp.Id == oldTechPart.Id {
				tp.Rarity = oldTechPart.Rarity
				break
			}
		}
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionSplitTechPart, merge)
	log.ESLogSave(userData.Id, log.ActionSplitTechPart, merge)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) UpgradeTalentNormal(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.Profile.Level < models.USERConfig.CFTalent.LevelUnlockNormal[userData.Talent.Normal+1] {
		log.ZapLog.Warn("invalid user level")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	goldUpgrade := models.USERConfig.CFTalent.GoldUpgradeNormal[userData.Talent.Normal+1]
	if err = userData.AddGold(-goldUpgrade); err != nil {
		log.ZapLog.Warn("not enough currency")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.Talent.Normal += 1
	userData.SetAchievement(models.AchievementIndexUnlockTalentNormal, userData.Talent.Normal)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionUpgradeTalentNormal, userData.Talent.Normal)
	log.ESLogSave(userData.Id, log.ActionUpgradeTalentNormal, userData.Talent.Normal)
	ginCtx.Status(http.StatusOK)
}

func (c *UserAssetController) UpgradeTalentSpecial(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.Profile.Level < models.USERConfig.CFTalent.LevelUnlockSpecial[userData.Talent.Special+1] {
		log.ZapLog.Warn("invalid user level")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	gemUpgrade := models.USERConfig.CFTalent.GemUpgradeSpecial[userData.Talent.Special+1]
	if err = userData.AddGem(-gemUpgrade); err != nil {
		log.ZapLog.Warn("not enough currency")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.Talent.Special += 1
	userData.SetAchievement(models.AchievementIndexUnlockTalentSpecial, userData.Talent.Special)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionUpgradeTalentSpecial, userData.Talent.Special)
	log.ESLogSave(userData.Id, log.ActionUpgradeTalentSpecial, userData.Talent.Special)
	ginCtx.Status(http.StatusOK)
}
