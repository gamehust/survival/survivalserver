package controller

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"

	"Survival/lib/log"
	"Survival/src/models"
	"Survival/src/repository"
)

type UserPlayController struct{}

func NewUserPlayController() *UserPlayController {
	return &UserPlayController{}
}

func (c *UserPlayController) AddRouter(engine *gin.Engine) *UserPlayController {
	AddRouter(engine, []Router{
		{
			http.MethodPost,
			"/:userId/ca/p",
			c.PlayCampaign,
		}, {
			http.MethodPost,
			"/:userId/ca/c/:level/:step",
			c.ClaimCampaignReward,
		}, {
			http.MethodPost,
			"/:userId/bm/p",
			c.PlayBossMode,
		}, {
			http.MethodPost,
			"/:userId/bm/c/:level/:mode",
			c.ClaimBossModeReward,
		}, {
			http.MethodPost,
			"/:userId/dg/p",
			c.PlayDungeon,
		}, {
			http.MethodPost,
			"/:userId/dg/c/:level/:mode",
			c.ClaimDungeonReward,
		}, {
			http.MethodPost,
			"/:userId/ex/c",
			c.ClaimExploration,
		}, {
			http.MethodPost,
			"/:userId/ex/q",
			c.QuickExploration,
		}, {
			http.MethodPost,
			"/:userId/ex/a",
			c.QuickExplorationAds,
		}, {
			http.MethodPost,
			"/:userId/dr/r",
			c.ClaimDailyReward,
		}, {
			http.MethodPost,
			"/:userId/dr/b",
			c.ClaimDailyRewardBonus,
		}, {
			http.MethodPost,
			"/:userId/dq/r/:index",
			c.ClaimDailyQuestReward,
		}, {
			http.MethodPost,
			"/:userId/dq/b/:index",
			c.ClaimDailyQuestBonus,
		}, {
			http.MethodPost,
			"/:userId/wq/r/:index",
			c.ClaimWeeklyQuestReward,
		}, {
			http.MethodPost,
			"/:userId/wq/b/:index",
			c.ClaimWeeklyQuestBonus,
		}, {
			http.MethodPost,
			"/:userId/ac/r/:index",
			c.ClaimAchievementReward,
		}, {
			http.MethodPost,
			"/:userId/rl/r/:index",
			c.ClaimRookieLoginReward,
		}, {
			http.MethodPost,
			"/:userId/q7/r/:index",
			c.ClaimQuest7DaysReward,
		}, {
			http.MethodPost,
			"/:userId/q7/b/:index",
			c.ClaimQuest7DaysBonus,
		},
	}, "/p")
	return c
}

func (c *UserPlayController) PlayCampaign(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var req models.PlayCampaignRequest
	if err = ginCtx.ShouldBindJSON(&req); err != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	var countDesign, countEquipment int64 = 0, 0
	for k, v := range req.Designs {
		_ = userData.AddDesign(k, v)
		countDesign += v
	}
	for _, v := range req.Equipments {
		countEquipment += 1
		if v.Rarity != models.RarityNormal || v.Level != 1 ||
			userData.AddEquipment(v) != nil {
			log.ZapLog.Warn("invalid eqpt data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	}
	if req.Level < 1 || req.Level > userData.Campaign.Current || req.StartTime < userData.LatestSaved ||
		req.Gold > models.USERConfig.CFCampaign.GoldReward[req.Level] || req.Exp > models.USERConfig.CFCampaign.ExpReward[req.Level] ||
		countDesign > models.USERConfig.CFCampaign.DesignReward || countEquipment > models.USERConfig.CFCampaign.EquipmentReward ||
		req.KillEnemy > models.USERConfig.CFCampaign.MaxKillEnemy || req.KillBoss > models.USERConfig.CFCampaign.MaxKillBoss ||
		userData.AddEnergy(req.StartTime, -models.USERConfig.CFCampaign.EnergyPlay) != nil {
		log.ZapLog.Debug("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.LatestSaved = req.StartTime + req.PlayTime
	_ = userData.AddExp(req.Exp)
	_ = userData.AddGold(req.Gold)
	if req.PlayTime > userData.Campaign.PlayTime[req.Level] {
		userData.Campaign.PlayTime[req.Level] = req.PlayTime
	}

	if req.Level == userData.Campaign.Current {
		switch req.Win {
		case models.PlayStep3:
			userData.Profile.Campaign = userData.Campaign.Current
			userData.Profile.KillEnemy = req.KillEnemy
			userData.Profile.KillBoss = req.KillBoss
			userData.Profile.PlayTime = req.StartTime
			userData.Campaign.Current += 1
			if userData.Campaign.RewardStep3[req.Level] == models.StatusDoing {
				userData.Campaign.RewardStep3[req.Level] = models.StatusDone
				userData.BossMode.NormalReward[req.Level] = models.StatusDoing
				userData.BossMode.HardReward[req.Level] = models.StatusDoing
				userData.BossMode.CrazyReward[req.Level] = models.StatusDoing
				userData.Dungeon.NormalReward[req.Level] = models.StatusDoing
				userData.Dungeon.HardReward[req.Level] = models.StatusDoing
				userData.Dungeon.CrazyReward[req.Level] = models.StatusDoing
				userData.Campaign.PlayTime[userData.Campaign.Current] = 0
				userData.Campaign.RewardStep1[userData.Campaign.Current] = models.StatusDoing
				userData.Campaign.RewardStep2[userData.Campaign.Current] = models.StatusDoing
				userData.Campaign.RewardStep3[userData.Campaign.Current] = models.StatusDoing
			}
			fallthrough
		case models.PlayStep2:
			if userData.Campaign.RewardStep2[req.Level] == models.StatusDoing {
				userData.Campaign.RewardStep2[req.Level] = models.StatusDone
			}
			fallthrough
		case models.PlayStep1:
			if userData.Campaign.RewardStep1[req.Level] == models.StatusDoing {
				userData.Campaign.RewardStep1[req.Level] = models.StatusDone
			}
		}
	}

	if req.Win == models.PlayStep3 && req.Level == userData.Profile.Campaign {
		if req.KillEnemy >= userData.Profile.KillEnemy {
			userData.Profile.KillEnemy = req.KillEnemy
			userData.Profile.PlayTime = req.StartTime
		}
		if req.KillBoss >= userData.Profile.KillBoss {
			userData.Profile.KillBoss = req.KillBoss
			userData.Profile.PlayTime = req.StartTime
		}
	}

	userData.ChangeDailyQuest(models.DailyQuestIndexPlayCampaign, 1)
	userData.ChangeWeeklyQuest(models.WeeklyQuestIndexKillEnemies, req.KillEnemy)
	userData.ChangeAchievement(models.AchievementIndexKillEnemies, req.KillEnemy)
	userData.ChangeAchievement(models.AchievementIndexKillBosses, req.KillBoss)
	userData.SetAchievement(models.AchievementIndexPassCampaign, req.Level)
	userData.UpdateGoldPiggy(req.Gold / 1000)

	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionPlayCampaign, req)
	log.ESLogSave(userData.Id, log.ActionPlayCampaign, req)
	ginCtx.Status(http.StatusOK)
}

func (c *UserPlayController) ClaimCampaignReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	level, _ := strconv.ParseInt(ginCtx.Param("level"), 10, 64)
	step, _ := strconv.ParseInt(ginCtx.Param("step"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	switch step {
	case models.PlayStep1:
		if userData.Campaign.RewardStep1[level] != models.StatusDone {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		userData.Campaign.RewardStep1[level] = models.StatusClaimed
		_ = userData.AddKeyRareChest(models.USERConfig.CFCampaign.Step1Reward[models.GiftNameRareChest])
	case models.PlayStep2:
		if userData.Campaign.RewardStep2[level] != models.StatusDone {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		userData.Campaign.RewardStep2[level] = models.StatusClaimed
		_ = userData.AddGem(models.USERConfig.CFCampaign.Step2Reward[models.GiftNameGem])
	case models.PlayStep3:
		if userData.Campaign.RewardStep3[level] != models.StatusDone {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		userData.Campaign.RewardStep3[level] = models.StatusClaimed
		_ = userData.AddKeyMythChest(models.USERConfig.CFCampaign.ClearReward[models.GiftNameMythChest])
	default:
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimCampaignReward, level, step)
	log.ESLogSave(userData.Id, log.ActionClaimCampaignReward, map[string]int64{"Level": level, "Step": step})
	ginCtx.Status(http.StatusOK)
}

func (c *UserPlayController) PlayBossMode(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var req models.PlayBossModeRequest
	if err = ginCtx.ShouldBindJSON(&req); err != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	var countPetPiece int64 = 0
	for k, v := range req.PetPiece {
		_ = userData.AddPetPiece(k, v)
		countPetPiece += v
	}
	if req.Level < 1 || req.Level >= userData.Campaign.Current ||
		countPetPiece > models.USERConfig.CFBossMode.PetPieceReward[req.Mode] ||
		req.StartTime < userData.LatestSaved ||
		userData.AddEnergy(req.StartTime, -models.USERConfig.CFBossMode.EnergyPlay[req.Mode]) != nil {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.LatestSaved = req.StartTime + req.PlayTime
	if req.Win {
		switch req.Mode {
		case models.PlayModeNormal:
			if userData.BossMode.NormalReward[req.Level] == models.StatusDoing {
				userData.BossMode.NormalReward[req.Level] = models.StatusDone
			}
		case models.PlayModeHard:
			if userData.BossMode.HardReward[req.Level] == models.StatusDoing {
				userData.BossMode.HardReward[req.Level] = models.StatusDone
			}
		case models.PlayModeCrazy:
			if userData.BossMode.CrazyReward[req.Level] == models.StatusDoing {
				userData.BossMode.CrazyReward[req.Level] = models.StatusDone
				if userData.Profile.BossMode < req.Level {
					userData.Profile.BossMode = req.Level
				}
			}
		}
		userData.ChangeAchievement(models.AchievementIndexKillBosses, 1)
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionPlayBossMode, req)
	log.ESLogSave(userData.Id, log.ActionPlayBossMode, req)
	ginCtx.Status(http.StatusOK)
}

func (c *UserPlayController) ClaimBossModeReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	level, _ := strconv.ParseInt(ginCtx.Param("level"), 10, 64)
	mode, _ := strconv.ParseInt(ginCtx.Param("mode"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	switch mode {
	case models.PlayModeNormal:
		if userData.BossMode.NormalReward[level] != models.StatusDone {
			log.ZapLog.Warn("invalid req adata")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		userData.BossMode.NormalReward[level] = models.StatusClaimed
		_ = userData.AddKeyRareChest(models.USERConfig.CFBossMode.NormalReward[models.GiftNameRareChest])
	case models.PlayModeHard:
		if userData.BossMode.HardReward[level] != models.StatusDone {
			log.ZapLog.Warn("invalid req adata")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		userData.BossMode.HardReward[level] = models.StatusClaimed
		_ = userData.AddGem(models.USERConfig.CFBossMode.HardReward[models.GiftNameGem])
	case models.PlayModeCrazy:
		if userData.BossMode.CrazyReward[level] != models.StatusDone {
			log.ZapLog.Warn("invalid req adata")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		userData.BossMode.CrazyReward[level] = models.StatusClaimed
		_ = userData.AddKeyMythChest(models.USERConfig.CFBossMode.CrazyReward[models.GiftNameMythChest])
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimBossModeReward, level, mode)
	log.ESLogSave(userData.Id, log.ActionClaimBossModeReward, map[string]int64{"Level": level, "Mode": mode})
	ginCtx.Status(http.StatusOK)
}

func (c *UserPlayController) PlayDungeon(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var req models.PlayDungeonRequest
	if err = ginCtx.ShouldBindJSON(&req); err != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	var countStone, countDesign, countTechPart, countEquipment int64 = 0, 0, 0, 0
	for k, v := range req.Stones {
		_ = userData.AddStone(k, v)
		countStone += v
	}
	for k, v := range req.Designs {
		_ = userData.AddDesign(k, v)
		countDesign += v
	}
	for _, v := range req.TechParts {
		countTechPart += 1
		if v.Rarity != models.RarityNormal || v.Level != 1 ||
			userData.AddTechPart(v) != nil {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	}
	for _, v := range req.Equipments {
		countEquipment += 1
		if v.Rarity != models.RarityNormal || v.Level != 1 ||
			userData.AddEquipment(v) != nil {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	}
	if req.Level < 1 || req.Level >= userData.Campaign.Current ||
		req.StartTime < userData.LatestSaved ||
		req.Exp > models.USERConfig.CFDungeon.ExpReward[req.Level] ||
		req.Gold > models.USERConfig.CFDungeon.GoldReward[req.Level]*3 ||
		countStone > models.USERConfig.CFDungeon.StoneReward[req.Mode] ||
		countDesign > models.USERConfig.CFDungeon.DesignReward[req.Mode] ||
		countTechPart > models.USERConfig.CFDungeon.TechPartReward[req.Mode] ||
		countEquipment > models.USERConfig.CFDungeon.EquipmentReward[req.Mode] ||
		req.KillEnemy > models.USERConfig.CFDungeon.MaxKillEnemy ||
		req.KillBoss > models.USERConfig.CFDungeon.MaxKillBoss ||
		userData.AddEnergy(req.StartTime, -models.USERConfig.CFDungeon.EnergyPlay[req.Mode]) != nil {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.LatestSaved = req.StartTime + req.PlayTime
	_ = userData.AddExp(req.Exp)
	_ = userData.AddGold(req.Gold)
	if req.Win {
		switch req.Mode {
		case models.PlayModeNormal:
			if userData.Dungeon.NormalReward[req.Level] == models.StatusDoing {
				userData.Dungeon.NormalReward[req.Level] = models.StatusDone
			}
		case models.PlayModeHard:
			if userData.Dungeon.HardReward[req.Level] == models.StatusDoing {
				userData.Dungeon.HardReward[req.Level] = models.StatusDone
			}
		case models.PlayModeCrazy:
			if userData.Dungeon.CrazyReward[req.Level] == models.StatusDoing {
				userData.Dungeon.CrazyReward[req.Level] = models.StatusDone
				if userData.Profile.Dungeon < req.Level {
					userData.Profile.Dungeon = req.Level
				}
			}
		default:
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	}

	userData.ChangeWeeklyQuest(models.WeeklyQuestIndexKillEnemies, req.KillEnemy)
	userData.ChangeAchievement(models.AchievementIndexPlayDungeon, 1)
	userData.ChangeAchievement(models.AchievementIndexKillEnemies, req.KillEnemy)
	userData.ChangeAchievement(models.AchievementIndexKillBosses, req.KillBoss)

	userData.UpdateGoldPiggy(req.Gold / 1000)

	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionPlayDungeon, req)
	log.ESLogSave(userData.Id, log.ActionPlayDungeon, req)
	ginCtx.Status(http.StatusOK)
}

func (c *UserPlayController) ClaimDungeonReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	level, _ := strconv.ParseInt(ginCtx.Param("level"), 10, 64)
	mode, _ := strconv.ParseInt(ginCtx.Param("mode"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	switch mode {
	case models.PlayModeNormal:
		if userData.Dungeon.NormalReward[level] != models.StatusDone {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		userData.Dungeon.NormalReward[level] = models.StatusClaimed
		_ = userData.AddGem(models.USERConfig.CFDungeon.HardReward[models.GiftNameGem])
	case models.PlayModeHard:
		if userData.Dungeon.HardReward[level] != models.StatusDone {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		userData.Dungeon.HardReward[level] = models.StatusClaimed
		_ = userData.AddKeyMythChest(models.USERConfig.CFDungeon.CrazyReward[models.GiftNameMythChest])
	case models.PlayModeCrazy:
		if userData.Dungeon.CrazyReward[level] != models.StatusDone {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		userData.Dungeon.CrazyReward[level] = models.StatusClaimed
		_ = userData.AddGem(models.USERConfig.CFDungeon.HardReward[models.GiftNameGem])
		_ = userData.AddKeyMythChest(models.USERConfig.CFDungeon.CrazyReward[models.GiftNameMythChest])
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimDungeonReward, level, mode)
	log.ESLogSave(userData.Id, log.ActionClaimDungeonReward, map[string]int64{"Level": level, "Mode": mode})
	ginCtx.Status(http.StatusOK)
}

func (c *UserPlayController) ClaimExploration(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var req models.ClaimExplorationRequest
	if err = ginCtx.ShouldBindJSON(&req); err != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	if req.ClaimTime-userData.Exploration.Time < 600000 || req.ClaimTime > time.Now().UnixMilli() { // 10m
		log.ZapLog.Warn("invalid req	data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	patrolTime := float64(req.ClaimTime-userData.Exploration.Time) / 3600000
	if patrolTime > float64(models.USERConfig.CFExploration.MaxTimeAFK) {
		patrolTime = float64(models.USERConfig.CFExploration.MaxTimeAFK)
	}
	userData.Exploration.Time = req.ClaimTime

	expPatrol := int64(float64(models.USERConfig.CFExploration.ExpReward[userData.Campaign.Current]) * patrolTime)
	goldPatrol := int64(float64(models.USERConfig.CFExploration.GoldReward[userData.Campaign.Current]) * patrolTime)
	designPatrol := int64(models.USERConfig.CFExploration.DesignReward[userData.Campaign.Current] * patrolTime)
	equipmentPatrol := int64(models.USERConfig.CFExploration.EquipmentReward[userData.Campaign.Current] * patrolTime)

	_ = userData.AddExp(expPatrol)
	_ = userData.AddGold(goldPatrol)
	var countDesign int64 = 0
	for k, v := range req.Designs {
		countDesign += v
		_ = userData.AddDesign(k, v)
	}
	if countDesign != designPatrol || int64(len(req.Equipments)) != equipmentPatrol {
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	for _, v := range req.Equipments {
		if err = userData.AddEquipment(v); err != nil {
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	}
	userData.ChangeDailyQuest(models.DailyQuestIndexClaimExploration, 1)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimExploration, req)
	log.ESLogSave(userData.Id, log.ActionClaimExploration, req)
	ginCtx.Status(http.StatusOK)
}

func (c *UserPlayController) QuickExploration(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var req models.ClaimExplorationRequest
	if err = ginCtx.ShouldBindJSON(&req); err != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	if userData.Exploration.Quick <= 0 {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.Exploration.Quick -= 1
	if err = userData.AddEnergy(time.Now().UnixMilli(), -models.USERConfig.CFExploration.EnergyQuick); err != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusBadRequest)
		return
	}

	expPatrol := models.USERConfig.CFExploration.ExpReward[userData.Campaign.Current] * models.USERConfig.CFExploration.TimeOfQuick
	goldPatrol := models.USERConfig.CFExploration.GoldReward[userData.Campaign.Current] * models.USERConfig.CFExploration.TimeOfQuick
	designPatrol := int64(models.USERConfig.CFExploration.DesignReward[userData.Campaign.Current] * float64(models.USERConfig.CFExploration.TimeOfQuick))
	equipmentPatrol := int64(models.USERConfig.CFExploration.EquipmentReward[userData.Campaign.Current] * float64(models.USERConfig.CFExploration.TimeOfQuick))

	_ = userData.AddExp(expPatrol)
	_ = userData.AddGold(goldPatrol)
	var countDesign int64 = 0
	for k, v := range req.Designs {
		countDesign += v
		_ = userData.AddDesign(k, v)
	}
	if countDesign != designPatrol || int64(len(req.Equipments)) != equipmentPatrol {
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	for _, v := range req.Equipments {
		_ = userData.AddEquipment(v)
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionQuickExploration, req)
	log.ESLogSave(userData.Id, log.ActionQuickExploration, req)
	ginCtx.Status(http.StatusOK)
}

func (c *UserPlayController) QuickExplorationAds(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var req models.ClaimExplorationRequest
	if err = ginCtx.ShouldBindJSON(&req); err != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	if userData.Exploration.Ads <= 0 {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.Exploration.Ads -= 1
	expPatrol := models.USERConfig.CFExploration.ExpReward[userData.Campaign.Current] * models.USERConfig.CFExploration.TimeOfQuick
	goldPatrol := models.USERConfig.CFExploration.GoldReward[userData.Campaign.Current] * models.USERConfig.CFExploration.TimeOfQuick
	designPatrol := int64(models.USERConfig.CFExploration.DesignReward[userData.Campaign.Current] * float64(models.USERConfig.CFExploration.TimeOfQuick))
	equipmentPatrol := int64(models.USERConfig.CFExploration.EquipmentReward[userData.Campaign.Current] * float64(models.USERConfig.CFExploration.TimeOfQuick))

	_ = userData.AddExp(expPatrol)
	_ = userData.AddGold(goldPatrol)
	var countDesign int64 = 0
	for k, v := range req.Designs {
		countDesign += v
		_ = userData.AddDesign(k, v)
	}
	if countDesign != designPatrol || int64(len(req.Equipments)) != equipmentPatrol {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	for _, v := range req.Equipments {
		_ = userData.AddEquipment(v)
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionQuickExplorationAds, req)
	log.ESLogSave(userData.Id, log.ActionQuickExplorationAds, req)
	ginCtx.Status(http.StatusOK)
}

func (c *UserPlayController) ClaimDailyReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.DailyReward.Reward != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.DailyReward.Reward = models.StatusClaimed
	switch userData.DailyReward.Day % 7 {
	case 1, 3, 5:
		reward := models.USERConfig.CFDailyReward.Reward[1]
		_ = userData.AddRevivalCoin(reward[models.GiftNameRevivalCoin])
	case 2, 4, 6:
		reward := models.USERConfig.CFDailyReward.Reward[2]
		_ = userData.AddKeyRareChest(reward[models.GiftNameRareChest])
	case 0:
		reward := models.USERConfig.CFDailyReward.Reward[7]
		_ = userData.AddGem(reward[models.GiftNameGem])
		goldPatrol := models.USERConfig.CFExploration.GoldReward[userData.Campaign.Current]
		_ = userData.AddGold(reward[models.GiftNameGoldPatrol] * goldPatrol)
		_ = userData.AddKeyMythChest(reward[models.GiftNameMythChest])
		_ = userData.AddRevivalCoin(reward[models.GiftNameRevivalCoin])
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimDailyReward, userData.DailyReward.Day)
	log.ESLogSave(userData.Id, log.ActionClaimDailyReward, userData.DailyReward.Day)
	ginCtx.Status(http.StatusOK)
}

func (c *UserPlayController) ClaimDailyRewardBonus(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.DailyReward.Bonus != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.DailyReward.Bonus = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFDailyReward.Bonus[models.GiftNameGem])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimDailyRewardBonus, nil)
	log.ESLogSave(userData.Id, log.ActionClaimDailyRewardBonus, nil)
	ginCtx.Status(http.StatusOK)
}

func (c *UserPlayController) ClaimDailyQuestReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.DailyQuest.Reward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.DailyQuest.Reward[index] = models.StatusClaimed
	userData.DailyQuest.Point += models.USERConfig.CFDailyQuest.Point[index]
	for i := int64(1); i <= userData.DailyQuest.Point; i++ {
		if userData.DailyQuest.Bonus[i] == models.StatusDoing {
			userData.DailyQuest.Bonus[i] = models.StatusDone
		}
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimDailyQuestReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimDailyQuestReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserPlayController) ClaimDailyQuestBonus(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.DailyQuest.Bonus[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.DailyQuest.Bonus[index] = models.StatusClaimed
	bonus := models.USERConfig.CFDailyQuest.Bonus[index]
	goldPatrol := models.USERConfig.CFExploration.GoldReward[userData.Campaign.Current]
	_ = userData.AddGold(goldPatrol * bonus[models.GiftNameGoldPatrol])
	switch index {
	case 1, 2, 4:
		design := make(map[int64]int64)
		if err = ginCtx.ShouldBindJSON(&design); err != nil {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		var countDesign int64 = 0
		for k, v := range design {
			_ = userData.AddDesign(k, v)
			countDesign += v
		}
		if countDesign != bonus[models.GiftNameDesignRandom] {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case 3:
		_ = userData.AddGem(bonus[models.GiftNameGem])
	case 5:
		_ = userData.AddGem(bonus[models.GiftNameGem])
		_ = userData.AddKeyRareChest(bonus[models.GiftNameRareChest])
	default:
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimDailyQuestBonus, index)
	log.ESLogSave(userData.Id, log.ActionClaimDailyQuestBonus, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserPlayController) ClaimWeeklyQuestReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.WeeklyQuest.Reward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.WeeklyQuest.Reward[index] = models.StatusClaimed
	userData.WeeklyQuest.Point += models.USERConfig.CFWeeklyQuest.Point[index]
	for i := int64(1); i <= userData.WeeklyQuest.Point; i++ {
		if userData.WeeklyQuest.Bonus[i] == models.StatusDoing {
			userData.WeeklyQuest.Bonus[i] = models.StatusDone
		}
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimWeeklyQuestReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimWeeklyQuestReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserPlayController) ClaimWeeklyQuestBonus(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.WeeklyQuest.Bonus[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	bonus := models.USERConfig.CFWeeklyQuest.Bonus[index]
	goldPatrol := models.USERConfig.CFExploration.GoldReward[userData.Campaign.Current]
	_ = userData.AddGold(goldPatrol * bonus[models.GiftNameGoldPatrol])
	switch index {
	case 1, 2, 4:
		design := make(map[int64]int64)
		if err = ginCtx.ShouldBindJSON(&design); err != nil {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		var countDesign int64 = 0
		for k, v := range design {
			_ = userData.AddDesign(k, v)
			countDesign += v
		}
		if countDesign != bonus[models.GiftNameDesignRandom] {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case 3:
		_ = userData.AddGem(bonus[models.GiftNameGem])
	case 5:
		_ = userData.AddGem(bonus[models.GiftNameGem])
		_ = userData.AddKeyMythChest(bonus[models.GiftNameMythChest])
	default:
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimWeeklyQuestBonus, index)
	log.ESLogSave(userData.Id, log.ActionClaimWeeklyQuestBonus, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserPlayController) ClaimAchievementReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.Achievement.Reward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	reward := models.USERConfig.CFAchievement.Reward[userData.Achievement.Target[index]]
	_ = userData.AddGem(reward)
	if userData.Achievement.Target[index] < 10 {
		userData.Achievement.Target[index] += 1
		userData.Achievement.Reward[index] = models.StatusDoing
		userData.ChangeAchievement(index, 0)
	} else {
		userData.Achievement.Reward[index] = models.StatusClaimed
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimAchievementReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimAchievementReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserPlayController) ClaimRookieLoginReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.RookieLogin.Reward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.RookieLogin.Reward[index] = models.StatusClaimed
	reward := models.USERConfig.CFRookieLogin.Reward[index]
	for k, v := range reward {
		switch k {
		case models.GiftNameEnergy:
			_ = userData.AddEnergy(time.Now().UnixMilli(), v)
		case models.GiftNameRevivalCoin:
			_ = userData.AddRevivalCoin(v)
		case models.GiftNameGem:
			_ = userData.AddGem(v)
		case models.GiftNameGold:
			_ = userData.AddGold(v)
		case models.GiftNameRareChest:
			_ = userData.AddKeyRareChest(v)
		case models.GiftNameMythChest:
			_ = userData.AddKeyMythChest(v)
		}
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimRookieLoginReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimRookieLoginReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserPlayController) ClaimQuest7DaysReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.Quest7Days.Rewards[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.Quest7Days.Rewards[index] = models.StatusClaimed

	reward := models.USERConfig.CFQuest7Days.Reward[index]
	_ = userData.AddGem(reward)
	userData.Quest7Days.Point += reward
	for k, v := range models.USERConfig.CFQuest7Days.Point {
		if userData.Quest7Days.Point >= v && userData.Quest7Days.Bonus[k] == models.StatusDoing {
			userData.Quest7Days.Bonus[k] = models.StatusDone
		}
	}

	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimQuest7DaysReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimQuest7DaysReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserPlayController) ClaimQuest7DaysBonus(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var req models.ClaimQuest7DaysBonusRequest
	if err = ginCtx.ShouldBindJSON(&req); err != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	if userData.Quest7Days.Day > 8 || userData.Quest7Days.Bonus[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req	data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.Quest7Days.Bonus[index] = models.StatusClaimed
	reward := models.USERConfig.CFQuest7Days.Bonus[index]
	_ = userData.AddGem(reward[models.GiftNameGem])
	_ = userData.AddKeyMythChest(reward[models.GiftNameMythChest])
	for k, v := range req.Stones {
		_ = userData.AddStone(k, v)
	}
	for k, v := range req.Designs {
		_ = userData.AddDesign(k, v)
	}
	for _, equipment := range req.Equipments {
		_ = userData.AddEquipment(equipment)
	}
	for _, tech := range req.TechParts {
		_ = userData.AddTechPart(tech)
	}

	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimQuest7DaysBonus, index)
	log.ESLogSave(userData.Id, log.ActionClaimQuest7DaysBonus, index)
	ginCtx.Status(http.StatusOK)
}
