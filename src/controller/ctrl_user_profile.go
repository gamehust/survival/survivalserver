package controller

import (
	"Survival/lib/log"
	"Survival/src/models"
	"Survival/src/repository"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"regexp"
	"strconv"
	"time"
)

type UserProfileController struct{}

func NewUserProfileController() *UserProfileController {
	return &UserProfileController{}
}

func (c *UserProfileController) AddRouter(engine *gin.Engine) *UserProfileController {
	AddRouter(engine, []Router{
		{
			http.MethodPost,
			"/:userId/n/:newName",
			c.ChangeName,
		}, {
			http.MethodPost,
			"/:userId/a/:newAvatar",
			c.ChangeAvatar,
		}, {
			http.MethodPost,
			"/:userId/hr/:index",
			c.SelectHero,
		}, {
			http.MethodPost,
			"/:userId/pee/:pos/:index",
			c.EquipPet,
		}, {
			http.MethodPost,
			"/:userId/peu/:pos",
			c.UnequipPet,
		}, {
			http.MethodPost,
			"/:userId/eqe/:id",
			c.EquipEquipment,
		}, {
			http.MethodPost,
			"/:userId/equ/:pos",
			c.UnequipEquipment,
		}, {
			http.MethodPost,
			"/:userId/tpe/:pos/:id",
			c.EquipTechPart,
		}, {
			http.MethodPost,
			"/:userId/tpu/:pos",
			c.UnequipTechPart,
		},
	}, "/f")
	return c
}

func (c *UserProfileController) ChangeName(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	newName := ginCtx.Param("newName")
	if !regexp.MustCompile(models.IdRegEx).MatchString(newName) {
		log.ZapLog.Warn("name not valid regexp")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userData.Profile.Name = newName
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionChangeName, newName)
	log.ESLogSave(userData.Id, log.ActionChangeName, newName)
	ginCtx.Status(http.StatusOK)
}

func (c *UserProfileController) ChangeAvatar(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	newAvatar, _ := strconv.ParseInt(ginCtx.Param("newAvatar"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userData.Profile.Avatar = newAvatar
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionChangeAvatar, newAvatar)
	log.ESLogSave(userData.Id, log.ActionChangeAvatar, newAvatar)
	ginCtx.Status(http.StatusOK)
}

func (c *UserProfileController) SelectHero(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	heroIndex, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	hero := userData.Hero[heroIndex]
	if hero == nil || hero.Level == 0 {
		log.ZapLog.Warn("hero is not owned yet")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.Profile.Hero = hero
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionSelectHero, hero)
	log.ESLogSave(userData.Id, log.ActionSelectHero, hero)
	ginCtx.Status(http.StatusOK)
}

func (c *UserProfileController) EquipPet(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	pos, _ := strconv.ParseInt(ginCtx.Param("pos"), 10, 64)
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	pet := userData.Pet[index]
	if pet == nil {
		log.ZapLog.Warn("pet is not owned yet")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	if userData.Profile.Pet == nil {
		userData.Profile.Pet = map[int64]*models.Pet{}
	}
	switch pos {
	case 1:
		if temp := userData.Profile.Pet[2]; temp != nil && temp.Index == pet.Index {
			delete(userData.Profile.Pet, 2)
		}
	case 2:
		if temp := userData.Profile.Pet[1]; temp != nil && temp.Index == pet.Index {
			delete(userData.Profile.Pet, 1)
		}
	}
	userData.Profile.Pet[pos] = pet
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionEquipPet, pet)
	log.ESLogSave(userData.Id, log.ActionEquipPet, pet)
	ginCtx.Status(http.StatusOK)
}

func (c *UserProfileController) UnequipPet(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	pos, _ := strconv.ParseInt(ginCtx.Param("pos"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	delete(userData.Profile.Pet, pos)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionUnequipPet, pos)
	log.ESLogSave(userData.Id, log.ActionUnequipPet, pos)
	ginCtx.Status(http.StatusOK)
}

func (c *UserProfileController) EquipEquipment(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	eqptId, _ := strconv.ParseInt(ginCtx.Param("id"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	eqpt := userData.Equipment[eqptId]
	if eqpt == nil {
		log.ZapLog.Warn("eqpt is not owned yet")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	if userData.Profile.Equipment == nil {
		userData.Profile.Equipment = map[int64]*models.Equipment{}
	}
	userData.Profile.Equipment[eqpt.Type] = eqpt
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionEquipEquipment, eqpt)
	log.ESLogSave(userData.Id, log.ActionEquipEquipment, eqpt)
	ginCtx.Status(http.StatusOK)
}

func (c *UserProfileController) UnequipEquipment(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	pos, _ := strconv.ParseInt(ginCtx.Param("pos"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	delete(userData.Profile.Equipment, pos)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionUnequipEquipment, pos)
	log.ESLogSave(userData.Id, log.ActionUnequipEquipment, pos)
	ginCtx.Status(http.StatusOK)
}

func (c *UserProfileController) EquipTechPart(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	pos, _ := strconv.ParseInt(ginCtx.Param("pos"), 10, 64)
	id, _ := strconv.ParseInt(ginCtx.Param("id"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	tech := userData.TechPart[id]
	if tech == nil {
		log.ZapLog.Warn("tech is not owned yet")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	if userData.Profile.TechPart == nil {
		userData.Profile.TechPart = map[int64]*models.TechPart{}
	}
	userData.Profile.TechPart[pos] = tech
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionEquipTechPart, tech)
	log.ESLogSave(userData.Id, log.ActionEquipTechPart, tech)
	ginCtx.Status(http.StatusOK)
}

func (c *UserProfileController) UnequipTechPart(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	pos, _ := strconv.ParseInt(ginCtx.Param("pos"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	delete(userData.Profile.TechPart, pos)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionUnequipTechPart, pos)
	log.ESLogSave(userData.Id, log.ActionUnequipTechPart, pos)
	ginCtx.Status(http.StatusOK)
}
