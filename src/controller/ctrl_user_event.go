package controller

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"

	"Survival/lib/log"
	"Survival/src/models"
	"Survival/src/repository"
)

type UserEventController struct{}

func NewUserEventController() *UserEventController {
	return &UserEventController{}
}

func (c *UserEventController) AddRouter(engine *gin.Engine) *UserEventController {
	AddRouter(engine, []Router{
		{
			http.MethodPost,
			"/:userId/mc/bn",
			c.BuyMonthlyCardNormal,
		}, {
			http.MethodPost,
			"/:userId/mc/bs",
			c.BuyMonthlyCardSuper,
		}, {
			http.MethodPost,
			"/:userId/mc/rn",
			c.ClaimMonthlyCardNormalReward,
		}, {
			http.MethodPost,
			"/:userId/mc/rs",
			c.ClaimMonthlyCardSuperReward,
		}, {
			http.MethodPost,
			"/:userId/bp/bn",
			c.BuyBattlePassNormal,
		}, {
			http.MethodPost,
			"/:userId/bp/bs",
			c.BuyBattlePassSuper,
		}, {
			http.MethodPost,
			"/:userId/bp/rf/:index",
			c.ClaimBattlePassFreeReward,
		}, {
			http.MethodPost,
			"/:userId/bp/rn/:index",
			c.ClaimBattlePassNormalReward,
		}, {
			http.MethodPost,
			"/:userId/bp/rs/:index",
			c.ClaimBattlePassSuperReward,
		}, {
			http.MethodPost,
			"/:userId/l1/bn",
			c.BuyLevelPass1Normal,
		}, {
			http.MethodPost,
			"/:userId/l1/bs",
			c.BuyLevelPass1Super,
		}, {
			http.MethodPost,
			"/:userId/l1/rf/:index",
			c.ClaimLevelPass1FreeReward,
		}, {
			http.MethodPost,
			"/:userId/l1/rn/:index",
			c.ClaimLevelPass1NormalReward,
		}, {
			http.MethodPost,
			"/:userId/l1/rs/:index",
			c.ClaimLevelPass1SuperReward,
		}, {
			http.MethodPost,
			"/:userId/l2/bn",
			c.BuyLevelPass2Normal,
		}, {
			http.MethodPost,
			"/:userId/l2/bs",
			c.BuyLevelPass2Super,
		}, {
			http.MethodPost,
			"/:userId/l2/rf/:index",
			c.ClaimLevelPass2FreeReward,
		}, {
			http.MethodPost,
			"/:userId/l2/rn/:index",
			c.ClaimLevelPass2NormalReward,
		}, {
			http.MethodPost,
			"/:userId/l2/rs/:index",
			c.ClaimLevelPass2SuperReward,
		}, {
			http.MethodPost,
			"/:userId/cp/bn",
			c.BuyCampaignPassNormal,
		}, {
			http.MethodPost,
			"/:userId/cp/bs",
			c.BuyCampaignPassSuper,
		}, {
			http.MethodPost,
			"/:userId/cp/rf/:index",
			c.ClaimCampaignPassFreeReward,
		}, {
			http.MethodPost,
			"/:userId/cp/rn/:index",
			c.ClaimCampaignPassNormalReward,
		}, {
			http.MethodPost,
			"/:userId/cp/rs/:index",
			c.ClaimCampaignPassSuperReward,
		}, {
			http.MethodPost,
			"/:userId/dp/bh",
			c.BuyDungeonPassHard,
		}, {
			http.MethodPost,
			"/:userId/dp/bc",
			c.BuyDungeonPassCrazy,
		}, {
			http.MethodPost,
			"/:userId/dp/rn/:index",
			c.ClaimDungeonPassNormalReward,
		}, {
			http.MethodPost,
			"/:userId/dp/rh/:index",
			c.ClaimDungeonPassHardReward,
		}, {
			http.MethodPost,
			"/:userId/dp/rc/:index",
			c.ClaimDungeonPassCrazyReward,
		}, {
			http.MethodPost,
			"/:userId/gp",
			c.BuyGoldPiggy,
		},
	}, "/e")
	return c
}

func (c *UserEventController) BuyMonthlyCardNormal(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	_ = userData.AddVipPoint(models.USERConfig.CFMonthlyCard.NormalPrice)
	_ = userData.AddGem(models.USERConfig.CFMonthlyCard.NormalPurchaseReward)
	userData.MonthlyCard.TimeNormal += 30
	userData.MonthlyCard.RewardNormal = models.StatusDone
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyMonthlyCardNormal, nil)
	log.ESLogSave(userData.Id, log.ActionBuyMonthlyCardNormal, nil)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) BuyMonthlyCardSuper(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	_ = userData.AddVipPoint(models.USERConfig.CFMonthlyCard.SuperPrice)
	_ = userData.AddGem(models.USERConfig.CFMonthlyCard.SuperPurchaseReward)
	userData.MonthlyCard.TimeSuper += 30
	userData.MonthlyCard.RewardSuper = models.StatusDone
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyMonthlyCardSuper, nil)
	log.ESLogSave(userData.Id, log.ActionBuyMonthlyCardSuper, nil)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) ClaimMonthlyCardNormalReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.MonthlyCard.RewardNormal != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.MonthlyCard.RewardNormal = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFMonthlyCard.NormalDailyReward)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimMonthlyCardNormalReward, nil)
	log.ESLogSave(userData.Id, log.ActionClaimMonthlyCardNormalReward, nil)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) ClaimMonthlyCardSuperReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.MonthlyCard.RewardSuper != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.MonthlyCard.RewardSuper = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFMonthlyCard.SuperDailyReward)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimMonthlyCardSuperReward, nil)
	log.ESLogSave(userData.Id, log.ActionClaimMonthlyCardSuperReward, nil)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) BuyBattlePassNormal(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.BattlePass.NormalActive {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.BattlePass.NormalActive = true
	_ = userData.AddVipPoint(models.USERConfig.CFBattlePass.NormalPrice)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyBattlePassNormal, nil)
	log.ESLogSave(userData.Id, log.ActionBuyBattlePassNormal, nil)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) BuyBattlePassSuper(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.BattlePass.SuperActive {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.BattlePass.SuperActive = true
	_ = userData.AddVipPoint(models.USERConfig.CFBattlePass.SuperPrice)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyBattlePassSuper, nil)
	log.ESLogSave(userData.Id, log.ActionBuyBattlePassSuper, nil)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) ClaimBattlePassFreeReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.BattlePass.FreeReward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.BattlePass.FreeReward[index] = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFBattlePass.FreeReward[index][models.GiftNameGem])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimBattlePassFreeReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimBattlePassFreeReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) ClaimBattlePassNormalReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if !userData.BattlePass.NormalActive || userData.BattlePass.NormalReward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.BattlePass.NormalReward[index] = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFBattlePass.NormalReward[index][models.GiftNameGem])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimBattlePassNormalReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimBattlePassNormalReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) ClaimBattlePassSuperReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if !userData.BattlePass.SuperActive || userData.BattlePass.SuperReward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.BattlePass.SuperReward[index] = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFBattlePass.SuperReward[index][models.GiftNameGem])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimBattlePassSuperReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimBattlePassSuperReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) BuyLevelPass1Normal(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.LevelPass1.NormalActive {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.LevelPass1.NormalActive = true
	_ = userData.AddVipPoint(models.USERConfig.CFLevelPass1.NormalPrice)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyLevelPass1Normal, nil)
	log.ESLogSave(userData.Id, log.ActionBuyLevelPass1Normal, nil)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) BuyLevelPass1Super(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.LevelPass1.SuperActive {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.LevelPass1.SuperActive = true
	_ = userData.AddVipPoint(models.USERConfig.CFLevelPass1.SuperPrice)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyLevelPass1Super, nil)
	log.ESLogSave(userData.Id, log.ActionBuyLevelPass1Super, nil)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) ClaimLevelPass1FreeReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.LevelPass1.FreeReward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.LevelPass1.FreeReward[index] = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFLevelPass1.FreeReward[index][models.GiftNameGem])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimLevelPass1FreeReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimLevelPass1FreeReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) ClaimLevelPass1NormalReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if !userData.LevelPass1.NormalActive || userData.LevelPass1.NormalReward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.LevelPass1.NormalReward[index] = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFLevelPass1.NormalReward[index][models.GiftNameGem])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimLevelPass1NormalReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimLevelPass1NormalReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) ClaimLevelPass1SuperReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if !userData.LevelPass1.SuperActive || userData.LevelPass1.SuperReward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.LevelPass1.SuperReward[index] = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFLevelPass1.SuperReward[index][models.GiftNameGem])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimLevelPass1SuperReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimLevelPass1SuperReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) BuyLevelPass2Normal(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.LevelPass2.NormalActive {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.LevelPass2.NormalActive = true
	_ = userData.AddVipPoint(models.USERConfig.CFLevelPass2.NormalPrice)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyLevelPass2Normal, nil)
	log.ESLogSave(userData.Id, log.ActionBuyLevelPass2Normal, nil)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) BuyLevelPass2Super(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.LevelPass2.SuperActive {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.LevelPass2.SuperActive = true
	_ = userData.AddVipPoint(models.USERConfig.CFLevelPass2.SuperPrice)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyLevelPass2Super, nil)
	log.ESLogSave(userData.Id, log.ActionBuyLevelPass2Super, nil)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) ClaimLevelPass2FreeReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.LevelPass2.FreeReward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.LevelPass2.FreeReward[index] = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFLevelPass2.FreeReward[index][models.GiftNameGem])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimLevelPass2FreeReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimLevelPass2FreeReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) ClaimLevelPass2NormalReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if !userData.LevelPass2.NormalActive || userData.LevelPass2.NormalReward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.LevelPass2.NormalReward[index] = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFLevelPass2.NormalReward[index][models.GiftNameGem])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimLevelPass2NormalReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimLevelPass2NormalReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) ClaimLevelPass2SuperReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if !userData.LevelPass2.SuperActive || userData.LevelPass2.SuperReward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.LevelPass2.SuperReward[index] = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFLevelPass2.SuperReward[index][models.GiftNameGem])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimLevelPass2SuperReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimLevelPass2SuperReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) BuyCampaignPassNormal(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.CampaignPass.NormalActive {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.CampaignPass.NormalActive = true
	_ = userData.AddVipPoint(models.USERConfig.CFCampaignPass.NormalPrice)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyCampaignPassNormal, nil)
	log.ESLogSave(userData.Id, log.ActionBuyCampaignPassNormal, nil)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) BuyCampaignPassSuper(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.CampaignPass.SuperActive {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.CampaignPass.SuperActive = true
	_ = userData.AddVipPoint(models.USERConfig.CFCampaignPass.SuperPrice)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyCampaignPassSuper, nil)
	log.ESLogSave(userData.Id, log.ActionBuyCampaignPassSuper, nil)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) ClaimCampaignPassFreeReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.CampaignPass.FreeReward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.CampaignPass.FreeReward[index] = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFCampaignPass.FreeReward[index][models.GiftNameGem])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimCampaignPassFreeReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimCampaignPassFreeReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) ClaimCampaignPassNormalReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if !userData.CampaignPass.NormalActive || userData.CampaignPass.NormalReward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.CampaignPass.NormalReward[index] = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFCampaignPass.NormalReward[index][models.GiftNameGem])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimCampaignPassNormalReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimCampaignPassNormalReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) ClaimCampaignPassSuperReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if !userData.CampaignPass.SuperActive || userData.CampaignPass.SuperReward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.CampaignPass.SuperReward[index] = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFCampaignPass.SuperReward[index][models.GiftNameGem])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimCampaignPassSuperReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimCampaignPassSuperReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) BuyDungeonPassHard(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.DungeonPass.HardActive {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.DungeonPass.HardActive = true
	_ = userData.AddVipPoint(models.USERConfig.CFDungeonPass.HardPrice)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyDungeonPassHard, nil)
	log.ESLogSave(userData.Id, log.ActionBuyDungeonPassHard, nil)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) BuyDungeonPassCrazy(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.DungeonPass.CrazyActive {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.DungeonPass.CrazyActive = true
	_ = userData.AddVipPoint(models.USERConfig.CFDungeonPass.CrazyPrice)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyDungeonPassCrazy, nil)
	log.ESLogSave(userData.Id, log.ActionBuyDungeonPassCrazy, nil)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) ClaimDungeonPassNormalReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.DungeonPass.NormalReward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.DungeonPass.NormalReward[index] = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFDungeonPass.NormalReward[index][models.GiftNameGem])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimDungeonPassNormalReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimDungeonPassNormalReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) ClaimDungeonPassHardReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if !userData.DungeonPass.HardActive || userData.DungeonPass.HardReward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.DungeonPass.HardReward[index] = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFDungeonPass.HardReward[index][models.GiftNameGem])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimDungeonPassHardReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimDungeonPassHardReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) ClaimDungeonPassCrazyReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if !userData.DungeonPass.CrazyActive || userData.DungeonPass.CrazyReward[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.DungeonPass.CrazyReward[index] = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFDungeonPass.CrazyReward[index][models.GiftNameGem])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimDungeonPassCrazyReward, index)
	log.ESLogSave(userData.Id, log.ActionClaimDungeonPassCrazyReward, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserEventController) BuyGoldPiggy(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if userData.GoldPiggy.Counter < models.USERConfig.CFGoldPiggy.RewardMin[userData.GoldPiggy.Size] {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	price := models.USERConfig.CFGoldPiggy.Price[userData.GoldPiggy.Size]
	_ = userData.AddVipPoint(price)
	_ = userData.AddGem(userData.GoldPiggy.Counter)
	userData.GoldPiggy.Counter = 0
	if userData.GoldPiggy.Size < models.GoldPiggySizeXXL {
		userData.GoldPiggy.Size += 1
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyGoldPiggy, nil)
	log.ESLogSave(userData.Id, log.ActionBuyGoldPiggy, nil)
	ginCtx.Status(http.StatusOK)
}
