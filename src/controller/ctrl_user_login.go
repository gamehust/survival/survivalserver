package controller

import (
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"

	"Survival/lib/log"
	"Survival/src/consumer"
	"Survival/src/models"
	"Survival/src/repository"
)

type UserLoginController struct{}

func NewUserLoginController() *UserLoginController {
	return &UserLoginController{}
}

func (c *UserLoginController) AddRouter(engine *gin.Engine) *UserLoginController {
	AddRouter(engine, []Router{
		{
			http.MethodPost,
			"/dv",
			c.LoginDevice,
		}, {
			http.MethodPost,
			"/fb",
			c.LoginFacebook,
		}, {
			http.MethodPost,
			"/gg",
			c.LoginGoogle,
		}, {
			http.MethodPost,
			"/ap",
			c.LoginApple,
		}, {
			http.MethodPost,
			"/pg",
			c.LoginPlayGames,
		}, {
			http.MethodPost,
			"/gc",
			c.LoginGameCenter,
		}, {
			http.MethodDelete,
			"/dl/:userId",
			c.DeleteAccount,
		},
	}, "/l")
	return c
}

func (c *UserLoginController) LoginDevice(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var req models.LoginRequest
	if err := ginCtx.ShouldBindJSON(&req); err != nil || req.DeviceId == "" {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserDataByDeviceId(ctx, req.DeviceId)
	if err != nil && err == mongo.ErrNoDocuments {
		userData = new(models.UserData)
		userData.GetDefault()
		userData.Info.DeviceId = req.DeviceId
		userData.Profile.Country = req.CountryCode
		userData.Timezone = req.Timezone
	} else if err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	if err = userData.GenerateToken(); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	rsp := models.LoginResponse{Id: userData.Id, Token: userData.Info.Token}
	log.ZapLog.Debug(userData.Id, log.ActionLoginDevice, req, rsp)
	log.ESLogSave(userData.Id, log.ActionLoginDevice, req)
	ginCtx.JSON(http.StatusOK, &rsp)
	return
}

func (c *UserLoginController) LoginFacebook(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var req models.LoginRequest
	if err := ginCtx.ShouldBindJSON(&req); err != nil || req.FacebookToken == "" {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	fbName, fbId, err := consumer.ValidateFacebookToken(req.FacebookToken)
	if err != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserDataByFacebookId(ctx, fbId)
	if err != nil && err == mongo.ErrNoDocuments {
		userData = new(models.UserData)
		userData.GetDefault()
		userData.Info.FacebookId = fbId
		userData.Profile.Name = fbName
		userData.Profile.Country = req.CountryCode
		userData.Timezone = req.Timezone
	} else if err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	if err = userData.GenerateToken(); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	rsp := models.LoginResponse{Id: userData.Id, Token: userData.Info.Token}
	log.ZapLog.Debug(userData.Id, log.ActionLoginFacebook, req, rsp)
	log.ESLogSave(userData.Id, log.ActionLoginFacebook, req)
	ginCtx.JSON(http.StatusOK, &rsp)
	return
}

func (c *UserLoginController) LoginGoogle(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	log.ZapLog.Warn("StatusNotImplemented")
	ginCtx.Status(http.StatusNotImplemented)
	return
}

func (c *UserLoginController) LoginApple(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	log.ZapLog.Warn("StatusNotImplemented")
	ginCtx.Status(http.StatusNotImplemented)
	return
}

func (c *UserLoginController) LoginPlayGames(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var req models.LoginRequest
	if err := ginCtx.ShouldBindJSON(&req); err != nil || req.PlayGamesId == "" || req.PlayGamesAuthCode == "" {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	pgName, pgId, err := consumer.ExchangePlayGamesAuthCode(req.PlayGamesId, req.PlayGamesAuthCode)
	if err != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserDataByPlayGamesId(ctx, pgId)
	if err != nil && err == mongo.ErrNoDocuments {
		userData = new(models.UserData)
		userData.GetDefault()
		userData.Info.PlayGamesId = pgId
		userData.Profile.Name = pgName
		userData.Profile.Country = req.CountryCode
		userData.Timezone = req.Timezone
	} else if err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	if err = userData.GenerateToken(); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	rsp := models.LoginResponse{Id: userData.Id, Token: userData.Info.Token}
	log.ZapLog.Debug(userData.Id, log.ActionLoginPlayGames, req, rsp)
	log.ESLogSave(userData.Id, log.ActionLoginPlayGames, req)
	ginCtx.JSON(http.StatusOK, &rsp)
	return
}

func (c *UserLoginController) LoginGameCenter(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	log.ZapLog.Warn("StatusNotImplemented")
	ginCtx.Status(http.StatusNotImplemented)
	return
}

func (c *UserLoginController) DeleteAccount(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, ueToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(ueToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if _, err = repository.DeleteUserData(ctx, userId); err != nil {
		log.ZapLog.Warn(err)
	}
	log.ESLogSave(userData.Id, log.ActionDeleteAccount, nil)
	ginCtx.Status(http.StatusOK)
}
