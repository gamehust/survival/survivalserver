package controller

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"

	"Survival/lib/log"
	"Survival/src/models"
	"Survival/src/repository"
)

type UserShopController struct{}

func NewUserShopController() *UserShopController {
	return &UserShopController{}
}

func (c *UserShopController) AddRouter(engine *gin.Engine) *UserShopController {
	AddRouter(engine, []Router{
		{
			http.MethodPost,
			"/:userId/mi",
			c.ClaimMonthlyInAppReward,
		}, {
			http.MethodPost,
			"/:userId/ct/:index",
			c.BuyChapterPack,
		}, {
			http.MethodPost,
			"/:userId/dl/:index",
			c.BuyDailySale,
		}, {
			http.MethodPost,
			"/:userId/rc/a",
			c.OpenRareChestAds,
		}, {
			http.MethodPost,
			"/:userId/rc/k",
			c.OpenRareChestKey,
		}, {
			http.MethodPost,
			"/:userId/rc/g",
			c.OpenRareChestGem,
		}, {
			http.MethodPost,
			"/:userId/mc/a",
			c.OpenMythChestAds,
		}, {
			http.MethodPost,
			"/:userId/mc/k",
			c.OpenMythChestKey,
		}, {
			http.MethodPost,
			"/:userId/mc/g",
			c.OpenMythChestGem,
		}, {
			http.MethodPost,
			"/:userId/m10",
			c.Open10MythChest,
		}, {
			http.MethodPost,
			"/:userId/ge/:index",
			c.BuyGemPack,
		}, {
			http.MethodPost,
			"/:userId/go/:index",
			c.BuyGoldPack,
		}, {
			http.MethodPost,
			"/:userId/ey/:index",
			c.BuyEnergyPack,
		}, {
			http.MethodPost,
			"/:userId/dd/:index",
			c.BuyDailyDiscount,
		}, {
			http.MethodPost,
			"/:userId/ddb/:index",
			c.ClaimDailyDiscountBonus,
		}, {
			http.MethodPost,
			"/:userId/wd/:index",
			c.BuyWeeklyDiscount,
		}, {
			http.MethodPost,
			"/:userId/wdb/:index",
			c.ClaimWeeklyDiscountBonus,
		}, {
			http.MethodPost,
			"/:userId/md/:index",
			c.BuyMonthlyDiscount,
		}, {
			http.MethodPost,
			"/:userId/mdb/:index",
			c.ClaimMonthlyDiscountBonus,
		},
	}, "/s")
	return c
}

func (c *UserShopController) ClaimMonthlyInAppReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var equipment models.Equipment
	if err = ginCtx.ShouldBindJSON(&equipment); err != nil ||
		equipment.Level != 1 || equipment.Rarity != models.RarityExcellent ||
		userData.Shop.MonthlyInApp != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.Shop.MonthlyInApp = models.StatusClaimed
	reward := models.USERConfig.CFShop.MonthlyInAppReward
	_ = userData.AddGem(reward[models.GiftNameGem])
	_ = userData.AddGold(reward[models.GiftNameGold])
	if err = userData.AddEquipment(&equipment); err != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimMonthlyInAppReward, equipment)
	log.ESLogSave(userData.Id, log.ActionClaimMonthlyInAppReward, equipment)
	ginCtx.Status(http.StatusOK)
}

func (c *UserShopController) BuyChapterPack(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	stones := make(map[int64]int64)
	if err = ginCtx.ShouldBindJSON(&stones); err != nil ||
		index < 1 || index >= userData.Campaign.Current || userData.Shop.ChapterPacks[index] {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	price := models.USERConfig.CFShop.ChapterPacksPrice[index]
	reward := models.USERConfig.CFShop.ChapterPacksReward[index]
	var countStone int64 = 0
	for k, v := range stones {
		_ = userData.AddStone(k, v)
		countStone += v
	}
	if countStone != reward[models.GiftNameStoneRandom] {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	_ = userData.AddVipPoint(price)
	_ = userData.AddGem(reward[models.GiftNameGem])
	_ = userData.AddGold(reward[models.GiftNameGold])
	_ = userData.AddKeyMythChest(reward[models.GiftNameMythChest])
	userData.Shop.ChapterPacks[index] = true
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyChapterPack, stones)
	log.ESLogSave(userData.Id, log.ActionBuyChapterPack, stones)
	ginCtx.Status(http.StatusOK)
}

func (c *UserShopController) BuyDailySale(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if index < 1 || index > 6 || userData.Shop.DailySales[index] ||
		userData.AddGem(-models.USERConfig.CFShop.DailySalesPrice[index]) != nil {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	reward := models.USERConfig.CFShop.DailySalesReward[index]
	log.ZapLog.Debugf("index: %d", index)
	switch index {
	case 1:
		_ = userData.AddRevivalCoin(reward[models.GiftNameRevivalCoin])
	case 2:
		var designType int64
		if err = ginCtx.ShouldBindJSON(&designType); err != nil {
			log.ZapLog.Warn(err)
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		_ = userData.AddDesign(designType, reward[models.GiftNameDesignRandom])
	case 3:
		var stoneIndex int64
		if err = ginCtx.ShouldBindJSON(&stoneIndex); err != nil {
			log.ZapLog.Warn(err)
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		_ = userData.AddStone(stoneIndex, reward[models.GiftNameStoneRandom])
	case 4:
		var petIndex int64
		if err = ginCtx.ShouldBindJSON(&petIndex); err != nil {
			log.ZapLog.Warn(err)
			ginCtx.Status(http.StatusBadRequest)
			return
		}
		_ = userData.AddPetPiece(petIndex, reward[models.GiftNamePetPieceRandom])
	case 5:
		var techPart models.TechPart
		if err = ginCtx.ShouldBindJSON(&techPart); err != nil ||
			techPart.Rarity != models.RarityBetter || techPart.Level != 1 ||
			userData.AddTechPart(&techPart) != nil {
			log.ZapLog.Warn(err)
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case 6:
		var equipment models.Equipment
		if err = ginCtx.ShouldBindJSON(&equipment); err != nil ||
			equipment.Rarity != models.RarityBetter || equipment.Level != 1 ||
			userData.AddEquipment(&equipment) != nil {
			log.ZapLog.Warn(err)
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	}
	userData.Shop.DailySales[index] = true
	userData.ChangeWeeklyQuest(models.WeeklyQuestIndexBuyDailySale, 1)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyDailySale, index)
	log.ESLogSave(userData.Id, log.ActionBuyDailySale, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserShopController) OpenRareChestAds(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var equipment models.Equipment
	if err = ginCtx.ShouldBindJSON(&equipment); err != nil ||
		equipment.Rarity < models.RarityNormal || equipment.Rarity > models.RarityGood || equipment.Level != 1 ||
		userData.AddEquipment(&equipment) != nil ||
		userData.Shop.AdsRareChest <= 0 {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.Shop.AdsRareChest -= 1
	userData.ChangeDailyQuest(models.DailyQuestIndexOpenChest, 1)
	userData.ChangeWeeklyQuest(models.WeeklyQuestIndexOpenChest, 1)
	userData.ChangeAchievement(models.AchievementIndexOpenChest, 1)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionOpenRareChestAds, equipment)
	log.ESLogSave(userData.Id, log.ActionOpenRareChestAds, equipment)
	ginCtx.Status(http.StatusOK)
}

func (c *UserShopController) OpenRareChestKey(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var equipment models.Equipment
	if err = ginCtx.ShouldBindJSON(&equipment); err != nil ||
		equipment.Rarity < models.RarityNormal || equipment.Rarity > models.RarityGood || equipment.Level != 1 ||
		userData.AddEquipment(&equipment) != nil ||
		userData.AddKeyRareChest(-1) != nil {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.ChangeDailyQuest(models.DailyQuestIndexOpenChest, 1)
	userData.ChangeWeeklyQuest(models.WeeklyQuestIndexOpenChest, 1)
	userData.ChangeAchievement(models.AchievementIndexOpenChest, 1)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionOpenRareChestKey, equipment)
	log.ESLogSave(userData.Id, log.ActionOpenRareChestKey, equipment)
	ginCtx.Status(http.StatusOK)
}

func (c *UserShopController) OpenRareChestGem(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var equipment models.Equipment
	if err = ginCtx.ShouldBindJSON(&equipment); err != nil ||
		equipment.Rarity < models.RarityNormal || equipment.Rarity > models.RarityGood || equipment.Level != 1 ||
		userData.AddEquipment(&equipment) != nil ||
		userData.AddGem(-models.USERConfig.CFShop.RareChestPrice) != nil {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.ChangeDailyQuest(models.DailyQuestIndexOpenChest, 1)
	userData.ChangeWeeklyQuest(models.WeeklyQuestIndexOpenChest, 1)
	userData.ChangeAchievement(models.AchievementIndexOpenChest, 1)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionOpenRareChestGem, equipment)
	log.ESLogSave(userData.Id, log.ActionOpenRareChestGem, equipment)
	ginCtx.Status(http.StatusOK)
}

func (c *UserShopController) OpenMythChestAds(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var equipment models.Equipment
	if err = ginCtx.ShouldBindJSON(&equipment); err != nil ||
		equipment.Rarity < models.RarityGood || equipment.Rarity > models.RarityExcellent || equipment.Level != 1 ||
		userData.AddEquipment(&equipment) != nil ||
		userData.Shop.AdsMythChest <= 0 {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.Shop.AdsMythChest -= 1
	userData.Shop.CountMythChest++
	userData.ChangeDailyQuest(models.DailyQuestIndexOpenChest, 1)
	userData.ChangeWeeklyQuest(models.WeeklyQuestIndexOpenChest, 1)
	userData.ChangeAchievement(models.AchievementIndexOpenChest, 1)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionOpenMythChestAds, equipment)
	log.ESLogSave(userData.Id, log.ActionOpenMythChestAds, equipment)
	ginCtx.Status(http.StatusOK)
}

func (c *UserShopController) OpenMythChestKey(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var equipment models.Equipment
	if err = ginCtx.ShouldBindJSON(&equipment); err != nil ||
		equipment.Rarity < models.RarityGood || equipment.Rarity > models.RarityExcellent || equipment.Level != 1 ||
		userData.AddEquipment(&equipment) != nil ||
		userData.AddKeyMythChest(-1) != nil {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.Shop.CountMythChest++
	userData.ChangeDailyQuest(models.DailyQuestIndexOpenChest, 1)
	userData.ChangeWeeklyQuest(models.WeeklyQuestIndexOpenChest, 1)
	userData.ChangeAchievement(models.AchievementIndexOpenChest, 1)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionOpenMythChestKey, equipment)
	log.ESLogSave(userData.Id, log.ActionOpenMythChestKey, equipment)
	ginCtx.Status(http.StatusOK)
}

func (c *UserShopController) OpenMythChestGem(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var equipment models.Equipment
	if err = ginCtx.ShouldBindJSON(&equipment); err != nil ||
		equipment.Rarity < models.RarityGood || equipment.Rarity > models.RarityExcellent || equipment.Level != 1 ||
		userData.AddEquipment(&equipment) != nil ||
		userData.AddGem(-models.USERConfig.CFShop.MythChestPrice) != nil {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.Shop.CountMythChest++
	userData.ChangeDailyQuest(models.DailyQuestIndexOpenChest, 1)
	userData.ChangeWeeklyQuest(models.WeeklyQuestIndexOpenChest, 1)
	userData.ChangeAchievement(models.AchievementIndexOpenChest, 1)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionOpenMythChestGem, equipment)
	log.ESLogSave(userData.Id, log.ActionOpenMythChestGem, equipment)
	ginCtx.Status(http.StatusOK)
}

func (c *UserShopController) Open10MythChest(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var equipments []*models.Equipment
	if err = ginCtx.ShouldBindJSON(&equipments); err != nil || len(equipments) != 10 ||
		userData.AddGem(-models.USERConfig.CFShop.Myth10ChestPrice) != nil {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	for _, equipment := range equipments {
		if equipment.Rarity < models.RarityGood || equipment.Rarity > models.RarityExcellent || equipment.Level != 1 ||
			userData.AddEquipment(equipment) != nil {
			log.ZapLog.Warn("indalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	}
	userData.Shop.CountMythChest += 10
	userData.ChangeDailyQuest(models.DailyQuestIndexOpenChest, 10)
	userData.ChangeWeeklyQuest(models.WeeklyQuestIndexOpenChest, 10)
	userData.ChangeAchievement(models.AchievementIndexOpenChest, 10)
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionOpen10MythChest, equipments)
	log.ESLogSave(userData.Id, log.ActionOpen10MythChest, equipments)
	ginCtx.Status(http.StatusOK)
}

func (c *UserShopController) BuyGemPack(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if index < 1 || index > 9 || userData.Shop.GemPacks[index] {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	if index >= 1 && index <= 3 {
		userData.Shop.GemPacks[index] = true
	}
	_ = userData.AddVipPoint(models.USERConfig.CFShop.GemPacksPrice[index])
	_ = userData.AddGem(models.USERConfig.CFShop.GemPacksReward[index])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyGemPack, index)
	log.ESLogSave(userData.Id, log.ActionBuyGemPack, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserShopController) BuyGoldPack(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if index < 1 || index > 5 || userData.Shop.GoldPacks[index] ||
		userData.AddGem(-models.USERConfig.CFShop.GoldPacksPrice[index]) != nil {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	if index >= 1 && index <= 2 {
		userData.Shop.GoldPacks[index] = true
	}
	reward := models.USERConfig.CFShop.GoldPacksReward[index]
	goldPatrol := models.USERConfig.CFExploration.GoldReward[userData.Campaign.Current]
	_ = userData.AddGold(goldPatrol * reward)
	userData.ChangeAchievement(models.AchievementIndexSpendGem, models.USERConfig.CFShop.GoldPacksPrice[index])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyGoldPack, index)
	log.ESLogSave(userData.Id, log.ActionBuyGoldPack, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserShopController) BuyEnergyPack(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if index < 1 || index > 4 || userData.Shop.EnergyPacks[index] ||
		userData.AddGem(-models.USERConfig.CFShop.EnergyPacksPrice[index]) != nil {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	if index >= 1 && index <= 3 {
		userData.Shop.EnergyPacks[index] = true
	}
	_ = userData.AddEnergy(time.Now().UnixMilli(), models.USERConfig.CFShop.EnergyPacksReward[index])
	userData.ChangeDailyQuest(models.DailyQuestIndexBuyEnergy, 1)
	userData.ChangeAchievement(models.AchievementIndexSpendGem, models.USERConfig.CFShop.EnergyPacksPrice[index])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyEnergyPack, index)
	log.ESLogSave(userData.Id, log.ActionBuyEnergyPack, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserShopController) BuyDailyDiscount(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var req models.BuyDiscountRequest
	if err = ginCtx.ShouldBindJSON(&req); err != nil ||
		index < 1 || index > 6 || userData.Discount.DailyReward[index] {
		log.ZapLog.Warn("invalida req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	_ = userData.AddVipPoint(models.USERConfig.CFDiscount.DailyPrice[index])
	reward := models.USERConfig.CFDiscount.DailyReward[index]
	goldPatrol := models.USERConfig.CFExploration.GoldReward[userData.Campaign.Current]
	_ = userData.AddGem(reward[models.GiftNameGem])
	switch index {
	case 1:
		_ = userData.AddGold(reward[models.GiftNameGoldPatrol] * goldPatrol)
		_ = userData.AddRevivalCoin(reward[models.GiftNameRevivalCoin])
	case 2:
		var countStone, countDesign int64 = 0, 0
		for k, v := range req.Stone {
			_ = userData.AddStone(k, v)
			countStone += v
		}
		for k, v := range req.Design {
			_ = userData.AddDesign(k, v)
			countDesign += v
		}
		if countStone != reward[models.GiftNameStoneRandom] || countDesign != reward[models.GiftNameDesignRandom] {
			log.ZapLog.Warn("invalida req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case 3:
		var countPetPiece, countDesign int64 = 0, 0
		for k, v := range req.PetPiece {
			_ = userData.AddPetPiece(k, v)
			countPetPiece += v
		}
		for k, v := range req.Design {
			_ = userData.AddDesign(k, v)
			countDesign += v
		}
		if countPetPiece != reward[models.GiftNamePetPieceRandom] || countDesign != reward[models.GiftNameDesignRandom] {
			log.ZapLog.Warn("invalida req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case 4:
		_ = userData.AddKeyRareChest(reward[models.GiftNameRareChest])
		var countDesign int64 = 0
		for k, v := range req.Design {
			_ = userData.AddDesign(k, v)
			countDesign += v
		}
		if countDesign != reward[models.GiftNameDesignRandom] {
			log.ZapLog.Warn("invalida req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case 5:
		_ = userData.AddKeyMythChest(reward[models.GiftNameMythChest])
		var countStone int64 = 0
		for k, v := range req.Stone {
			_ = userData.AddStone(k, v)
			countStone += v
		}
		if countStone != reward[models.GiftNameStoneRandom] {
			log.ZapLog.Warn("invalida req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case 6:
		_ = userData.AddKeyMythChest(reward[models.GiftNameMythChest])
		var countPetPiece int64 = 0
		for k, v := range req.PetPiece {
			_ = userData.AddPetPiece(k, v)
			countPetPiece += v
		}
		if countPetPiece != reward[models.GiftNamePetPieceRandom] {
			log.ZapLog.Warn("invalida req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	}
	userData.Discount.DailyReward[index] = true
	userData.Discount.DailyCount += 1
	if userData.Discount.DailyCount >= 2 && userData.Discount.DailyBonus[1] == models.StatusDoing {
		userData.Discount.DailyBonus[1] = models.StatusDone
	}
	if userData.Discount.DailyCount >= 4 && userData.Discount.DailyBonus[2] == models.StatusDoing {
		userData.Discount.DailyBonus[2] = models.StatusDone
	}
	if userData.Discount.DailyCount >= 6 && userData.Discount.DailyBonus[3] == models.StatusDoing {
		userData.Discount.DailyBonus[3] = models.StatusDone
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyDailyDiscount, index)
	log.ESLogSave(userData.Id, log.ActionBuyDailyDiscount, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserShopController) ClaimDailyDiscountBonus(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if index < 1 || index > 3 || userData.Discount.DailyBonus[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.Discount.DailyBonus[index] = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFDiscount.DailyBonus[index])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimDailyDiscountBonus, index)
	log.ESLogSave(userData.Id, log.ActionClaimDailyDiscountBonus, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserShopController) BuyWeeklyDiscount(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var req models.BuyDiscountRequest
	if err = ginCtx.ShouldBindJSON(&req); err != nil ||
		index < 1 || index > 6 || userData.Discount.WeeklyReward[index] {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	_ = userData.AddVipPoint(models.USERConfig.CFDiscount.WeeklyPrice[index])
	reward := models.USERConfig.CFDiscount.WeeklyReward[index]
	goldPatrol := models.USERConfig.CFExploration.GoldReward[userData.Campaign.Current]
	_ = userData.AddGem(reward[models.GiftNameGem])
	switch index {
	case 1:
		_ = userData.AddGold(reward[models.GiftNameGoldPatrol] * goldPatrol)
		_ = userData.AddRevivalCoin(reward[models.GiftNameRevivalCoin])
	case 2:
		var countStone int64 = 0
		for k, v := range req.Stone {
			_ = userData.AddStone(k, v)
			countStone += v
		}
		if countStone != reward[models.GiftNameStoneRandom] ||
			req.TechPart == nil || req.TechPart.Rarity != models.RarityGood || req.TechPart.Level != 1 ||
			userData.AddTechPart(req.TechPart) != nil {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case 3:
		var countPetPiece int64 = 0
		for k, v := range req.PetPiece {
			_ = userData.AddPetPiece(k, v)
			countPetPiece += v
		}
		if countPetPiece != reward[models.GiftNamePetPieceRandom] ||
			req.TechPart == nil || req.TechPart.Rarity != models.RarityGood || req.TechPart.Level != 1 ||
			userData.AddTechPart(req.TechPart) != nil {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case 4:
		var countStone int64 = 0
		for k, v := range req.Stone {
			_ = userData.AddStone(k, v)
			countStone += v
		}
		if countStone != reward[models.GiftNameStoneRandom] ||
			req.TechPart == nil || req.TechPart.Rarity != models.RarityBetter || req.TechPart.Level != 1 ||
			userData.AddTechPart(req.TechPart) != nil {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case 5:
		var countPetPiece int64 = 0
		for k, v := range req.PetPiece {
			_ = userData.AddPetPiece(k, v)
			countPetPiece += v
		}
		if countPetPiece != reward[models.GiftNamePetPieceRandom] ||
			req.TechPart == nil || req.TechPart.Rarity != models.RarityBetter || req.TechPart.Level != 1 ||
			userData.AddTechPart(req.TechPart) != nil {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case 6:
		_ = userData.AddKeyMythChest(reward[models.GiftNameMythChest])
		if req.TechPart == nil || req.TechPart.Rarity != models.RarityExcellent || req.TechPart.Level != 1 ||
			userData.AddTechPart(req.TechPart) != nil {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	}
	userData.Discount.WeeklyReward[index] = true
	userData.Discount.WeeklyCount += 1
	if userData.Discount.WeeklyCount >= 2 && userData.Discount.WeeklyBonus[1] == models.StatusDoing {
		userData.Discount.WeeklyBonus[1] = models.StatusDone
	}
	if userData.Discount.WeeklyCount >= 4 && userData.Discount.WeeklyBonus[2] == models.StatusDoing {
		userData.Discount.WeeklyBonus[2] = models.StatusDone
	}
	if userData.Discount.WeeklyCount >= 6 && userData.Discount.WeeklyBonus[3] == models.StatusDoing {
		userData.Discount.WeeklyBonus[3] = models.StatusDone
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyWeeklyDiscount, index)
	log.ESLogSave(userData.Id, log.ActionBuyWeeklyDiscount, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserShopController) ClaimWeeklyDiscountBonus(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if index < 1 || index > 3 || userData.Discount.WeeklyBonus[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.Discount.WeeklyBonus[index] = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFDiscount.WeeklyBonus[index])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimWeeklyDiscountBonus, index)
	log.ESLogSave(userData.Id, log.ActionClaimWeeklyDiscountBonus, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserShopController) BuyMonthlyDiscount(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	var req models.BuyDiscountRequest
	if err = ginCtx.ShouldBindJSON(&req); err != nil ||
		index < 1 || index > 6 || userData.Discount.MonthlyReward[index] {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	_ = userData.AddVipPoint(models.USERConfig.CFDiscount.MonthlyPrice[index])
	reward := models.USERConfig.CFDiscount.MonthlyReward[index]
	goldPatrol := models.USERConfig.CFExploration.GoldReward[userData.Campaign.Current]
	_ = userData.AddGem(reward[models.GiftNameGem])
	switch index {
	case 1:
		_ = userData.AddGold(reward[models.GiftNameGoldPatrol] * goldPatrol)
		_ = userData.AddRevivalCoin(reward[models.GiftNameRevivalCoin])
	case 2:
		_ = userData.AddKeyMythChest(reward[models.GiftNameMythChest])
		if req.TechPart == nil || req.TechPart.Rarity != models.RarityExcellent || req.TechPart.Level != 1 ||
			userData.AddTechPart(req.TechPart) != nil {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case 3:
		_ = userData.AddKeyMythChest(reward[models.GiftNameMythChest])
		if req.TechPart == nil || req.TechPart.Rarity != models.RarityExcellent || req.TechPart.Level != 1 ||
			userData.AddTechPart(req.TechPart) != nil {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case 4:
		var countStone int64 = 0
		for k, v := range req.Stone {
			_ = userData.AddStone(k, v)
			countStone += v
		}
		if countStone != reward[models.GiftNameStoneRandom] ||
			req.TechPart == nil || req.TechPart.Rarity != models.RarityExcellent1 || req.TechPart.Level != 1 ||
			userData.AddTechPart(req.TechPart) != nil {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case 5:
		var countPetPiece int64 = 0
		for k, v := range req.PetPiece {
			_ = userData.AddPetPiece(k, v)
			countPetPiece += v
		}
		if countPetPiece != reward[models.GiftNamePetPieceRandom] ||
			req.TechPart == nil || req.TechPart.Rarity != models.RarityExcellent1 || req.TechPart.Level != 1 ||
			userData.AddTechPart(req.TechPart) != nil {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	case 6:
		if req.TechPart == nil || req.TechPart.Rarity != models.RarityEpic || req.TechPart.Level != 1 ||
			userData.AddTechPart(req.TechPart) != nil ||
			req.Equipment == nil || req.Equipment.Rarity != models.RarityEpic || req.Equipment.Level != 1 ||
			userData.AddEquipment(req.Equipment) != nil {
			log.ZapLog.Warn("invalid req data")
			ginCtx.Status(http.StatusBadRequest)
			return
		}
	}
	userData.Discount.MonthlyReward[index] = true
	userData.Discount.MonthlyCount += 1
	if userData.Discount.MonthlyCount >= 2 && userData.Discount.MonthlyBonus[1] == models.StatusDoing {
		userData.Discount.MonthlyBonus[1] = models.StatusDone
	}
	if userData.Discount.MonthlyCount >= 4 && userData.Discount.MonthlyBonus[2] == models.StatusDoing {
		userData.Discount.MonthlyBonus[2] = models.StatusDone
	}
	if userData.Discount.MonthlyCount >= 6 && userData.Discount.MonthlyBonus[3] == models.StatusDoing {
		userData.Discount.MonthlyBonus[3] = models.StatusDone
	}
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionBuyMonthlyDiscount, index)
	log.ESLogSave(userData.Id, log.ActionBuyMonthlyDiscount, index)
	ginCtx.Status(http.StatusOK)
}

func (c *UserShopController) ClaimMonthlyDiscountBonus(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		log.ZapLog.Warn("StatusForbidden")
		ginCtx.Status(http.StatusForbidden)
		return
	}
	userId, userToken := ginCtx.Param("userId"), ginCtx.GetHeader("UserToken")
	index, _ := strconv.ParseInt(ginCtx.Param("index"), 10, 64)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	userData, err := repository.GetUserData(ctx, userId)
	if err != nil || userData.ValidateToken(userToken) != nil {
		log.ZapLog.Warn(err)
		ginCtx.Status(http.StatusForbidden)
		return
	}
	if index < 1 || index > 3 || userData.Discount.MonthlyBonus[index] != models.StatusDone {
		log.ZapLog.Warn("invalid req data")
		ginCtx.Status(http.StatusBadRequest)
		return
	}
	userData.Discount.MonthlyBonus[index] = models.StatusClaimed
	_ = userData.AddGem(models.USERConfig.CFDiscount.MonthlyBonus[index])
	if _, err = repository.UpsertUserData(ctx, userData); err != nil {
		log.ZapLog.Error(err)
		ginCtx.Status(http.StatusInternalServerError)
		return
	}
	log.ZapLog.Debug(userData.Id, log.ActionClaimMonthlyDiscountBonus, index)
	log.ESLogSave(userData.Id, log.ActionClaimMonthlyDiscountBonus, index)
	ginCtx.Status(http.StatusOK)
}
