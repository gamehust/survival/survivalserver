package controller

import (
	"Survival/src/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type UserConfigController struct{}

func NewUserConfigController() *UserConfigController {
	return &UserConfigController{}
}

func (c *UserConfigController) AddRouter(engine *gin.Engine) *UserConfigController {
	AddRouter(engine, []Router{
		{
			http.MethodGet,
			"",
			c.GetUserConfig,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONVersion,
			c.GetVersion,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFCurrency,
			c.GetCFCurrency,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFHero,
			c.GetCFHero,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFEquipment,
			c.GetCFEquipment,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFTechPart,
			c.GetCFTechPart,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFPet,
			c.GetCFPet,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFTalent,
			c.GetCFTalent,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFCampaign,
			c.GetCFCampaign,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFBossMode,
			c.GetCFBossMode,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFDungeon,
			c.GetCFDungeon,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFExploration,
			c.GetCFExploration,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFDailyReward,
			c.GetCFDailyReward,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFDailyQuest,
			c.GetCFDailyQuest,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFWeeklyQuest,
			c.GetCFWeeklyQuest,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFAchievement,
			c.GetCFAchievement,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFRookieLogin,
			c.GetCFRookieLogin,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFQuest7Days,
			c.GetCFQuest7Days,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFShop,
			c.GetCFShop,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFDiscount,
			c.GetCFDiscount,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFMonthlyCard,
			c.GetCFMonthlyCard,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFBattlePass,
			c.GetCFBattlePass,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFLevelPass1,
			c.GetCFLevelPass1,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFLevelPass2,
			c.GetCFLevelPass2,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFCampaignPass,
			c.GetCFCampaignPass,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFDungeonPass,
			c.GetCFDungeonPass,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFGoldPiggy,
			c.GetCFGoldPiggy,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFEventNewYear,
			c.GetCFEventNewYear,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFEventValentine,
			c.GetCFEventValentine,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFEventAprilFool,
			c.GetCFEventAprilFool,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFEventChildrenDay,
			c.GetCFEventChildrenDay,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFEventAutumnLeaves,
			c.GetCFEventAutumnLeaves,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFEventHalloween,
			c.GetCFEventHalloween,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFEventChristmas,
			c.GetCFEventChristmas,
		}, {
			http.MethodGet,
			"/" + models.UserConfigBSONCFEventWorldCup,
			c.GetCFEventWorldCup,
		},
	}, "/c")
	return c
}

func (c *UserConfigController) GetUserConfig(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig)
}

func (c *UserConfigController) GetVersion(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.Version)
}

func (c *UserConfigController) GetCFCurrency(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFCurrency)
}

func (c *UserConfigController) GetCFHero(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFHero)
}

func (c *UserConfigController) GetCFEquipment(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFEquipment)
}

func (c *UserConfigController) GetCFTechPart(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFTechPart)
}

func (c *UserConfigController) GetCFPet(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFPet)
}

func (c *UserConfigController) GetCFTalent(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFTalent)
}

func (c *UserConfigController) GetCFCampaign(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFCampaign)
}

func (c *UserConfigController) GetCFBossMode(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFBossMode)
}

func (c *UserConfigController) GetCFDungeon(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFDungeon)
}

func (c *UserConfigController) GetCFExploration(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFExploration)
}

func (c *UserConfigController) GetCFDailyReward(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFDailyReward)
}

func (c *UserConfigController) GetCFDailyQuest(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFDailyQuest)
}

func (c *UserConfigController) GetCFWeeklyQuest(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFWeeklyQuest)
}

func (c *UserConfigController) GetCFAchievement(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFAchievement)
}

func (c *UserConfigController) GetCFRookieLogin(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFRookieLogin)
}

func (c *UserConfigController) GetCFQuest7Days(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFQuest7Days)
}

func (c *UserConfigController) GetCFShop(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFShop)
}

func (c *UserConfigController) GetCFDiscount(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFDiscount)
}

func (c *UserConfigController) GetCFMonthlyCard(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFMonthlyCard)
}

func (c *UserConfigController) GetCFBattlePass(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFBattlePass)
}

func (c *UserConfigController) GetCFLevelPass1(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFLevelPass1)
}

func (c *UserConfigController) GetCFLevelPass2(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFLevelPass2)
}

func (c *UserConfigController) GetCFCampaignPass(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFCampaignPass)
}

func (c *UserConfigController) GetCFDungeonPass(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFDungeonPass)
}

func (c *UserConfigController) GetCFGoldPiggy(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFGoldPiggy)
}

func (c *UserConfigController) GetCFEventNewYear(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFEventNewYear)
}

func (c *UserConfigController) GetCFEventValentine(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFEventValentine)
}

func (c *UserConfigController) GetCFEventAprilFool(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFEventAprilFool)
}

func (c *UserConfigController) GetCFEventChildrenDay(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFEventChildrenDay)
}

func (c *UserConfigController) GetCFEventAutumnLeaves(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFEventAutumnLeaves)
}

func (c *UserConfigController) GetCFEventHalloween(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFEventHalloween)
}

func (c *UserConfigController) GetCFEventChristmas(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFEventChristmas)
}

func (c *UserConfigController) GetCFEventWorldCup(ginCtx *gin.Context) {
	if token := ginCtx.GetHeader("AccessToken"); token != models.ENVConfig.AccessToken {
		ginCtx.Status(http.StatusForbidden)
		return
	}
	ginCtx.JSON(http.StatusOK, models.USERConfig.CFEventWorldCup)
}
