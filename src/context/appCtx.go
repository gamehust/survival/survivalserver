package context

import (
	"context"
	"time"

	"Survival/lib/log"
	"Survival/src/models"
	"Survival/src/repository"
)

var serverInfo *models.ServerInfo

func SaveServerInfo() {
	serverInfo = &models.ServerInfo{
		Id:             models.ENVConfig.ServerID,
		ExternalIPAddr: models.ENVConfig.ExternalIPAddr,
		PrivateIPAddr:  models.ENVConfig.PrivateIPAddr,
		ExternalPort:   models.ENVConfig.ExternalPort,
	}
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	if _, err := repository.UpsertServerInfo(ctx, serverInfo); err != nil {
		log.ZapLog.Panic(err)
	}
}

func SaveUserConfig() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	if _, err := repository.UpsertUserConfig(ctx, &models.USERConfig); err != nil {
		log.ZapLog.Panic(err)
	}
}

func StartMonitor() {

}
