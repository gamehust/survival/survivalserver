package models

type UserConfig struct {
	Id      string           `json:"-" bson:"_id"`
	Version map[string]int64 `json:"vs" bson:"vs"`
	// common
	CFGiftCode map[string]*CFGiftCode `json:"gc" bson:"gc"`
	// asset
	CFCurrency  *CFCurrency  `json:"cc" bson:"cc"`
	CFHero      *CFHero      `json:"hr" bson:"hr"`
	CFPet       *CFPet       `json:"pe" bson:"pe"`
	CFEquipment *CFEquipment `json:"eq" bson:"eq"`
	CFTechPart  *CFTechPart  `json:"tp" bson:"tp"`
	CFTalent    *CFTalent    `json:"tl" bson:"tl"`
	// play
	CFCampaign    *CFCampaign    `json:"ca" bson:"ca"`
	CFBossMode    *CFBossMode    `json:"bm" bson:"bm"`
	CFDungeon     *CFDungeon     `json:"dg" bson:"dg"`
	CFExploration *CFExploration `json:"ex" bson:"ex"`
	// quest
	CFDailyReward *CFDailyReward `json:"dr" bson:"dr"`
	CFDailyQuest  *CFDailyQuest  `json:"dq" bson:"dq"`
	CFWeeklyQuest *CFWeeklyQuest `json:"wq" bson:"wq"`
	CFAchievement *CFAchievement `json:"ac" bson:"ac"`
	CFRookieLogin *CFRookieLogin `json:"rl" bson:"rl"`
	CFQuest7Days  *CFQuest7Days  `json:"q7" bson:"q7"`
	// shop
	CFShop     *CFShop     `json:"sh" bson:"sh"`
	CFDiscount *CFDiscount `json:"dc" bson:"dc"`
	// event - always on
	CFMonthlyCard  *CFMonthlyCard  `json:"mc" bson:"mc"`
	CFBattlePass   *CFBattlePass   `json:"bp" bson:"bp"`
	CFLevelPass1   *CFLevelPass    `json:"l1" bson:"l1"`
	CFLevelPass2   *CFLevelPass    `json:"l2" bson:"l2"`
	CFCampaignPass *CFCampaignPass `json:"cp" bson:"cp"`
	CFDungeonPass  *CFDungeonPass  `json:"dp" bson:"dp"`
	CFGoldPiggy    *CFGoldPiggy    `json:"gp" bson:"gp"`
	// event - on off
	CFEventNewYear      *CFEventNewYear      `json:"ny" bson:"ny"`
	CFEventValentine    *CFEventValentine    `json:"vl" bson:"vl"`
	CFEventAprilFool    *CFEventAprilFool    `json:"af" bson:"af"`
	CFEventChildrenDay  *CFEventChildrenDay  `json:"cd" bson:"cd"`
	CFEventAutumnLeaves *CFEventAutumnLeaves `json:"al" bson:"al"`
	CFEventHalloween    *CFEventHalloween    `json:"hl" bson:"hl"`
	CFEventChristmas    *CFEventChristmas    `json:"cr" bson:"cr"`
	CFEventWorldCup     *CFEventWorldCup     `json:"wc" bson:"wc"`
}

const (
	UserConfigBSONId                  = "_id"
	UserConfigBSONVersion             = "vs"
	UserConfigBSONCFGiftCode          = "gc"
	UserConfigBSONCFCurrency          = "cc"
	UserConfigBSONCFHero              = "hr"
	UserConfigBSONCFEquipment         = "eq"
	UserConfigBSONCFTechPart          = "tp"
	UserConfigBSONCFPet               = "pe"
	UserConfigBSONCFTalent            = "tl"
	UserConfigBSONCFCampaign          = "ca"
	UserConfigBSONCFBossMode          = "bm"
	UserConfigBSONCFDungeon           = "dg"
	UserConfigBSONCFExploration       = "ex"
	UserConfigBSONCFDailyReward       = "dr"
	UserConfigBSONCFDailyQuest        = "dq"
	UserConfigBSONCFWeeklyQuest       = "wq"
	UserConfigBSONCFAchievement       = "ac"
	UserConfigBSONCFRookieLogin       = "rl"
	UserConfigBSONCFQuest7Days        = "q7"
	UserConfigBSONCFShop              = "sh"
	UserConfigBSONCFDiscount          = "dc"
	UserConfigBSONCFMonthlyCard       = "mc"
	UserConfigBSONCFBattlePass        = "bp"
	UserConfigBSONCFLevelPass1        = "l1"
	UserConfigBSONCFLevelPass2        = "l2"
	UserConfigBSONCFCampaignPass      = "cp"
	UserConfigBSONCFDungeonPass       = "dp"
	UserConfigBSONCFGoldPiggy         = "gp"
	UserConfigBSONCFEventNewYear      = "ny"
	UserConfigBSONCFEventValentine    = "vl"
	UserConfigBSONCFEventAprilFool    = "af"
	UserConfigBSONCFEventChildrenDay  = "cd"
	UserConfigBSONCFEventAutumnLeaves = "al"
	UserConfigBSONCFEventHalloween    = "hl"
	UserConfigBSONCFEventChristmas    = "cr"
	UserConfigBSONCFEventWorldCup     = "wc"
)

type CFGiftCode struct {
	Code      string          `json:"c" bson:"c"`
	StartTime int64           `json:"s" bson:"s"`
	EndTime   int64           `json:"e" bson:"e"`
	Reward    map[int64]int64 `json:"r" bson:"r"`
}

type CFCurrency struct {
	MaxEnergy       int64           `json:"mey" bson:"mey"`
	MaxLevel        int64           `json:"mlv" bson:"mlv"`
	EnergyRecover   int64           `json:"eyr" bson:"eyr"`
	ExpPassLevel    map[int64]int64 `json:"epl" bson:"epl"`
	GemPassLevel    int64           `json:"gpl" bson:"gpl"`
	EnergyPassLevel int64           `json:"ypl" bson:"ypl"`
}

type CFHero struct {
	GemUnlock          map[int64]int64 `json:"gu" bson:"gu"`
	RarityUnlockLevel  map[int64]int64 `json:"rul" bson:"rul"`
	GoldUpgradeLevel   map[int64]int64 `json:"gul" bson:"gul"`
	GemUpgradeRarity   map[int64]int64 `json:"gur" bson:"gur"`
	StoneUpgradeRarity map[int64]int64 `json:"sur" bson:"sur"`
}

type CFPet struct {
	GemUnlock             map[int64]int64 `json:"gu" bson:"gu"`
	RarityUnlockLevel     map[int64]int64 `json:"rul" bson:"rul"`
	GoldUpgradeLevel      map[int64]int64 `json:"gul" bson:"gul"`
	GemUpgradeRarity      map[int64]int64 `json:"gur" bson:"gur"`
	PetPieceUpgradeRarity map[int64]int64 `json:"pur" bson:"pur"`
}

type CFEquipment struct {
	RarityUnlockLevel       map[int64]int64 `json:"rul" bson:"rul"`
	GoldUpgradeLevel        map[int64]int64 `json:"gul" bson:"gul"`
	TotalGoldUpgradeLevel   map[int64]int64 `json:"tgu" bson:"tgu"`
	DesignUpgradeLevel      map[int64]int64 `json:"dul" bson:"dul"`
	TotalDesignUpgradeLevel map[int64]int64 `json:"tdu" bson:"tdu"`
}

type CFTechPart struct {
	RarityUnlockLevel         map[int64]int64 `json:"rul" bson:"rul"`
	GoldUpgradeLevel          map[int64]int64 `json:"gul" bson:"gul"`
	TotalGoldUpgradeLevel     map[int64]int64 `json:"tgu" bson:"tgu"`
	TechUsedUpgradeLevel      map[int64]int64 `json:"tul" bson:"tul"`
	TotalTechUsedUpgradeLevel map[int64]int64 `json:"ttu" bson:"ttu"`
}

type CFTalent struct {
	LevelUnlockNormal  map[int64]int64 `json:"lun" bson:"lun"`
	GoldUpgradeNormal  map[int64]int64 `json:"gun" bson:"gun"`
	LevelUnlockSpecial map[int64]int64 `json:"lus" bson:"lus"`
	GemUpgradeSpecial  map[int64]int64 `json:"gus" bson:"gus"`
}

type CFCampaign struct {
	MaxLevel        int64           `json:"mlv" bson:"mlv"`
	EnergyPlay      int64           `json:"eyp" bson:"eyp"`
	MaxKillEnemy    int64           `json:"mke" bson:"mke"`
	MaxKillBoss     int64           `json:"mkb" bson:"mkb"`
	GoldReward      map[int64]int64 `json:"gor" bson:"gor"`
	ExpReward       map[int64]int64 `json:"exr" bson:"exr"`
	DesignReward    int64           `json:"der" bson:"der"`
	EquipmentReward int64           `json:"eqr" bson:"eqr"`
	Step1Reward     map[int64]int64 `json:"s1r" bson:"s1r"` // rare chest
	Step2Reward     map[int64]int64 `json:"s2r" bson:"s2r"` // gem
	ClearReward     map[int64]int64 `json:"s3r" bson:"s3r"` // myth chest
}

type CFBossMode struct {
	EnergyPlay     map[int64]int64 `json:"eyp" bson:"eyp"`
	PetPieceReward map[int64]int64 `json:"ppr" bson:"ppr"`
	NormalReward   map[int64]int64 `json:"nr" bson:"nr"` // rare chest
	HardReward     map[int64]int64 `json:"hr" bson:"hr"` // gem
	CrazyReward    map[int64]int64 `json:"cr" bson:"cr"` // myth chest
}

type CFDungeon struct {
	EnergyPlay      map[int64]int64 `json:"eyp" bson:"eyp"`
	MaxKillEnemy    int64           `json:"mke" bson:"mke"`
	MaxKillBoss     int64           `json:"mkb" bson:"mkb"`
	GoldReward      map[int64]int64 `json:"gor" bson:"gor"`
	ExpReward       map[int64]int64 `json:"exr" bson:"exr"`
	StoneReward     map[int64]int64 `json:"str" bson:"str"`
	DesignReward    map[int64]int64 `json:"der" bson:"der"`
	EquipmentReward map[int64]int64 `json:"eqr" bson:"eqr"`
	TechPartReward  map[int64]int64 `json:"tpr" bson:"tpr"`
	NormalReward    map[int64]int64 `json:"nr" bson:"nr"` // rare chest
	HardReward      map[int64]int64 `json:"hr" bson:"hr"` // gem
	CrazyReward     map[int64]int64 `json:"cr" bson:"cr"` // myth chest
}

type CFExploration struct {
	EnergyQuick     int64             `json:"eq" bson:"eq"`
	NumberOfQuick   int64             `json:"nq" bson:"nq"`
	NumberOfAds     int64             `json:"na" bson:"na"`
	TimeOfQuick     int64             `json:"tq" bson:"tq"`
	MaxTimeAFK      int64             `json:"mt" bson:"mt"`
	GoldReward      map[int64]int64   `json:"gor" bson:"gor"`
	ExpReward       map[int64]int64   `json:"exr" bson:"exr"`
	DesignReward    map[int64]float64 `json:"der" bson:"der"`
	EquipmentReward map[int64]float64 `json:"eqr" bson:"eqr"`
}

type CFDailyReward struct {
	Reward map[int64]map[int64]int64 `json:"r" bson:"r"`
	Bonus  map[int64]int64           `json:"b" bson:"b"`
}

type CFDailyQuest struct {
	Target map[int64]int64           `json:"t" bson:"t"`
	Point  map[int64]int64           `json:"p" bson:"p"`
	Bonus  map[int64]map[int64]int64 `json:"b" bson:"b"`
}

type CFWeeklyQuest struct {
	Target map[int64]int64           `json:"t" bson:"t"`
	Point  map[int64]int64           `json:"p" bson:"p"`
	Bonus  map[int64]map[int64]int64 `json:"b" bson:"b"`
}

type CFAchievement struct {
	Target map[int64]map[int64]int64 `json:"t" bson:"t"`
	Reward map[int64]int64           `json:"r" bson:"r"`
}

type CFRookieLogin struct {
	Reward map[int64]map[int64]int64 `json:"r" bson:"r"`
}

type CFQuest7Days struct {
	Target map[int64]int64           `json:"t" bson:"t"`
	Reward map[int64]int64           `json:"r" bson:"r"`
	Point  map[int64]int64           `json:"p" bson:"p"`
	Bonus  map[int64]map[int64]int64 `json:"b" bson:"b"`
}

type CFShop struct {
	MonthlyInAppReward map[int64]int64           `json:"mia" bson:"mia"`
	ChapterPacksPrice  map[int64]int64           `json:"cpp" bson:"cpp"`
	ChapterPacksReward map[int64]map[int64]int64 `json:"cpr" bson:"cpr"`
	DailySalesPrice    map[int64]int64           `json:"dlp" bson:"dlp"`
	DailySalesReward   map[int64]map[int64]int64 `json:"dlr" bson:"dlr"`
	AdsRareChestNewDay int64                     `json:"arc" bson:"arc"`
	AdsMythChestNewDay int64                     `json:"amc" bson:"amc"`
	RareChestPrice     int64                     `json:"rcp" bson:"rcp"`
	MythChestPrice     int64                     `json:"mcp" bson:"mcp"`
	Myth10ChestPrice   int64                     `json:"m10" bson:"m10"`
	CountMythChest     int64                     `json:"cmc" bson:"cmc"`
	CountMythSChest    int64                     `json:"cms" bson:"cms"`
	CountMythSSChest   int64                     `json:"css" bson:"css"`
	GemPacksPrice      map[int64]int64           `json:"gep" bson:"gep"`
	GemPacksReward     map[int64]int64           `json:"ger" bson:"ger"`
	GoldPacksPrice     map[int64]int64           `json:"gop" bson:"gop"`
	GoldPacksReward    map[int64]int64           `json:"gor" bson:"gor"`
	EnergyPacksPrice   map[int64]int64           `json:"eyp" bson:"eyp"`
	EnergyPacksReward  map[int64]int64           `json:"eyr" bson:"eyr"`
}

type CFDiscount struct {
	DailyPrice    map[int64]int64           `json:"dp" bson:"dp"`
	DailyReward   map[int64]map[int64]int64 `json:"dr" bson:"dr"`
	DailyBonus    map[int64]int64           `json:"db" bson:"db"`
	WeeklyPrice   map[int64]int64           `json:"wp" bson:"wp"`
	WeeklyReward  map[int64]map[int64]int64 `json:"wr" bson:"wr"`
	WeeklyBonus   map[int64]int64           `json:"wb" bson:"wb"`
	MonthlyPrice  map[int64]int64           `json:"mp" bson:"mp"`
	MonthlyReward map[int64]map[int64]int64 `json:"mr" bson:"mr"`
	MonthlyBonus  map[int64]int64           `json:"mb" bson:"mb"`
}

type CFMonthlyCard struct {
	NormalPrice                 int64 `json:"np" bson:"np"`
	NormalPurchaseReward        int64 `json:"npr" bson:"npr"`
	NormalDailyReward           int64 `json:"ndr" bson:"ndr"`
	NormalIncreasePatrolEarning int64 `json:"nip" bson:"nip"`
	NormalIncreaseQuickPatrol   int64 `json:"niq" bson:"niq"`
	NormalIncreaseMaxEnergy     int64 `json:"nie" bson:"nie"`

	SuperPrice                 int64 `json:"sp" bson:"sp"`
	SuperPurchaseReward        int64 `json:"spr" bson:"spr"`
	SuperDailyReward           int64 `json:"sdr" bson:"sdr"`
	SuperIncreasePatrolEarning int64 `json:"sip" bson:"sip"`
	SuperIncreaseQuickPatrol   int64 `json:"siq" bson:"siq"`
	SuperIncreaseMaxEnergy     int64 `json:"sie" bson:"sie"`
}

type CFBattlePass struct {
	TargetPoint  map[int64]int64           `json:"tp" bson:"tp"`
	NormalPrice  int64                     `json:"np" bson:"np"`
	SuperPrice   int64                     `json:"sp" bson:"sp"`
	FreeReward   map[int64]map[int64]int64 `json:"fr" bson:"fr"`
	NormalReward map[int64]map[int64]int64 `json:"nr" bson:"nr"`
	SuperReward  map[int64]map[int64]int64 `json:"sr" bson:"sr"`
}

type CFLevelPass struct {
	TargetLevel  map[int64]int64           `json:"tl" bson:"tl"`
	NormalPrice  int64                     `json:"np" bson:"np"`
	SuperPrice   int64                     `json:"sp" bson:"sp"`
	FreeReward   map[int64]map[int64]int64 `json:"fr" bson:"fr"`
	NormalReward map[int64]map[int64]int64 `json:"nr" bson:"nr"`
	SuperReward  map[int64]map[int64]int64 `json:"sr" bson:"sr"`
}

type CFCampaignPass struct {
	TargetLevel  map[int64]int64           `json:"tl" bson:"tl"`
	NormalPrice  int64                     `json:"np" bson:"np"`
	SuperPrice   int64                     `json:"sp" bson:"sp"`
	FreeReward   map[int64]map[int64]int64 `json:"fr" bson:"fr"`
	NormalReward map[int64]map[int64]int64 `json:"nr" bson:"nr"`
	SuperReward  map[int64]map[int64]int64 `json:"sr" bson:"sr"`
}

type CFDungeonPass struct {
	TargetLevel  map[int64]int64           `json:"tl" bson:"tl"`
	HardPrice    int64                     `json:"hp" bson:"hp"`
	CrazyPrice   int64                     `json:"cp" bson:"cp"`
	NormalReward map[int64]map[int64]int64 `json:"nr" bson:"nr"`
	HardReward   map[int64]map[int64]int64 `json:"hr" bson:"hr"`
	CrazyReward  map[int64]map[int64]int64 `json:"cr" bson:"cr"`
}

type CFGoldPiggy struct {
	Price     map[int64]int64 `json:"p" bson:"p"`
	RewardMin map[int64]int64 `json:"i" bson:"i"`
	RewardMax map[int64]int64 `json:"a" bson:"a"`
}

type CFEventNewYear struct {
	QuestTarget    map[int64]map[int64]int64           `json:"qt" bson:"qt"`
	QuestReward    map[int64]map[int64]map[int64]int64 `json:"qr" bson:"qr"`
	DiscountPrice  map[int64]int64                     `json:"dp" bson:"dp"`
	DiscountReward map[int64]map[int64]int64           `json:"dr" bson:"dr"`
	LotteryReward  map[int64]map[int64]int64           `json:"lr" bson:"lr"`
	LotteryBonus   map[int64]map[int64]int64           `json:"lb" bson:"lb"`
	ExchangePrice  map[int64]int64                     `json:"ep" bson:"ep"`
	ExchangeReward map[int64]map[int64]int64           `json:"er" bson:"er"`
	ExchangeLimit  map[int64]int64                     `json:"el" bson:"el"`
}

type CFEventValentine struct {
}

type CFEventAprilFool struct {
}

type CFEventChildrenDay struct {
}

type CFEventAutumnLeaves struct {
}

type CFEventHalloween struct {
}

type CFEventChristmas struct {
}

type CFEventWorldCup struct {
}
