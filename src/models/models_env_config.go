package models

import "Survival/lib/util"

var ENVConfig struct {
	ServerID       string `env:"SERVER_ID" default:"SVDev"`
	Version        string `env:"VERSION" default:"1.0.0"`
	ExternalIPAddr string `env:"EXTERNAL_IPADDR" default:"127.0.0.1"`
	PrivateIPAddr  string `env:"PRIVATE_IPADDR" default:"127.0.0.1"`
	ExternalPort   int64  `env:"EXTERNAL_PORT" default:"443"`
	AccessToken    string `env:"ACCESS_TOKEN" default:"1qazXSW@3edcVFR$"`
	TLSCertPath    string `env:"TLS_CERT_PATH" default:"/survival/lib/tls/cert.pem"`
	TLSKeyPath     string `env:"TLS_KEY_PATH" default:"/survival/lib/tls/key.pem"`
	LogLevel       string `env:"LOG_LEVEL" default:"debug"`
	MongoDBUri     string `env:"MONGODB_URI" default:"mongodb://127.0.0.1:27017/"`
	ElasticUri     string `env:"ELASTIC_URI" default:"http://127.0.0.1:9200"`
}

func InitENVConfig() {
	if err := util.Default(&ENVConfig); err != nil {
		panic(err)
	} else if err = util.ParseEnvironment(&ENVConfig); err != nil {
		panic(err)
	}
}
