package models

type Ranking struct {
	Id   string         `json:"-" bson:"_id"`
	Pfs  []*UserProfile `json:"-" bson:"r"`
	Time int64          `json:"-" bson:"t"`
}
