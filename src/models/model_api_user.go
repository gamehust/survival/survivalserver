package models

type LoginRequest struct {
	DeviceId            string `json:"dv,omitempty"`
	FacebookToken       string `json:"ftk,omitempty"`
	GoogleToken         string `json:"gtk,omitempty"`
	AppleToken          string `json:"atk,omitempty"`
	PlayGamesId         string `json:"pgi,omitempty"`
	PlayGamesAuthCode   string `json:"pga,omitempty"`
	GamesCenterId       string `json:"gci,omitempty"`
	GamesCenterAuthCode string `json:"gca,omitempty"`
	CountryCode         string `json:"cc,omitempty"`
	Timezone            string `json:"tz,omitempty"`
}

type LoginResponse struct {
	Id    string `json:"id,omitempty"`
	Token string `json:"tk,omitempty"`
}

type FacebookTokenResponse struct {
	Name  string                      `json:"name,omitempty"`
	Id    string                      `json:"id,omitempty"`
	Error *FacebookTokenResponseError `json:"error,omitempty"`
}

type FacebookTokenResponseError struct {
	Message string `json:"message,omitempty"`
	Type    string `json:"type,omitempty"`
	Code    int    `json:"code,omitempty"`
}

type MergeEquipmentRequest struct {
	OldEquipments []int64      `json:"o,omitempty"`
	NewEquipments []*Equipment `json:"n,omitempty"`
}

type MergeTechPartRequest struct {
	OldTechParts []int64     `json:"o,omitempty"`
	NewTechParts []*TechPart `json:"n,omitempty"`
}

type PlayCampaignRequest struct {
	Level      int64           `json:"lv,omitempty"`
	StartTime  int64           `json:"st,omitempty"`
	PlayTime   int64           `json:"pt,omitempty"`
	Win        int64           `json:"wi,omitempty"`
	Exp        int64           `json:"ex,omitempty"`
	Gold       int64           `json:"go,omitempty"`
	Designs    map[int64]int64 `json:"de,omitempty"`
	Equipments []*Equipment    `json:"eq,omitempty"`
	KillEnemy  int64           `json:"ke,omitempty"`
	KillBoss   int64           `json:"kb,omitempty"`
}

type PlayBossModeRequest struct {
	Level     int64           `json:"lv,omitempty"`
	Mode      int64           `json:"mo,omitempty"`
	StartTime int64           `json:"st,omitempty"`
	PlayTime  int64           `json:"pt,omitempty"`
	Win       bool            `json:"wi,omitempty"`
	PetPiece  map[int64]int64 `json:"pp,omitempty"`
}

type PlayDungeonRequest struct {
	Level      int64           `json:"lv,omitempty"`
	Mode       int64           `json:"mo,omitempty"`
	StartTime  int64           `json:"st,omitempty"`
	PlayTime   int64           `json:"pt,omitempty"`
	Win        bool            `json:"wi,omitempty"`
	Exp        int64           `json:"ex,omitempty"`
	Gold       int64           `json:"go,omitempty"`
	Stones     map[int64]int64 `json:"ss,omitempty"`
	Designs    map[int64]int64 `json:"de,omitempty"`
	Equipments []*Equipment    `json:"eq,omitempty"`
	TechParts  []*TechPart     `json:"tp,omitempty"`
	KillEnemy  int64           `json:"ke,omitempty"`
	KillBoss   int64           `json:"kb,omitempty"`
}

type ClaimExplorationRequest struct {
	ClaimTime  int64           `json:"ct,omitempty"`
	Designs    map[int64]int64 `json:"de,omitempty"`
	Equipments []*Equipment    `json:"eq,omitempty"`
}

type ClaimQuest7DaysBonusRequest struct {
	Stones     map[int64]int64      `json:"stones,omitempty"`
	Designs    map[int64]int64      `json:"designs,omitempty"`
	Equipments map[int64]*Equipment `json:"equipments,omitempty"`
	TechParts  map[int64]*TechPart  `json:"techParts,omitempty"`
}

type BuyDiscountRequest struct {
	Stone     map[int64]int64 `json:"st,omitempty"`
	Design    map[int64]int64 `json:"de,omitempty"`
	PetPiece  map[int64]int64 `json:"pp,omitempty"`
	Equipment *Equipment      `json:"eq,omitempty"`
	TechPart  *TechPart       `json:"tp,omitempty"`
}
