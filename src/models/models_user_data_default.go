package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

func (m *UserData) ResetDataNewDay() bool {
	m.Location, _ = time.LoadLocation(m.Timezone)
	latest := time.UnixMilli(m.UpdatedTime).In(m.Location)
	timeNow := time.Now().In(m.Location)
	oldYear, oldWeek := latest.ISOWeek()
	yearNow, weekNow := timeNow.ISOWeek()
	isChange := false
	if oldYear != yearNow {
		m.resetDataNewYear()
		m.resetDataNewMonth()
		m.resetDataNewDay()
		isChange = true
	} else if latest.Month() != timeNow.Month() {
		m.resetDataNewMonth()
		m.resetDataNewDay()
		isChange = true
	} else if latest.YearDay() != timeNow.YearDay() {
		m.resetDataNewDay()
		isChange = true
	}
	if oldWeek != weekNow {
		m.resetDataNewWeek()
		isChange = true
	}
	return isChange
}

func (m *UserData) resetDataNewYear() {}

func (m *UserData) resetDataNewMonth() {
	m.Shop.MonthlyInApp = StatusDoing
	m.Discount.MonthlyCount = 0
	m.Discount.MonthlyReward = map[int64]bool{1: false, 2: false, 3: false, 4: false, 5: false, 6: false}
	m.Discount.MonthlyBonus = map[int64]int64{1: StatusDoing, 2: StatusDoing, 3: StatusDoing}
	m.BattlePass = &BattlePass{
		Point: 0,
		FreeReward: map[int64]int64{
			1: StatusDoing, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing,
			7: StatusDoing, 8: StatusDoing, 9: StatusDoing, 10: StatusDoing, 11: StatusDoing, 12: StatusDoing,
			13: StatusDoing, 14: StatusDoing, 15: StatusDoing, 16: StatusDoing, 17: StatusDoing, 18: StatusDoing,
			19: StatusDoing, 20: StatusDoing},
		NormalActive: false,
		NormalReward: map[int64]int64{
			1: StatusDoing, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing,
			7: StatusDoing, 8: StatusDoing, 9: StatusDoing, 10: StatusDoing, 11: StatusDoing, 12: StatusDoing,
			13: StatusDoing, 14: StatusDoing, 15: StatusDoing, 16: StatusDoing, 17: StatusDoing, 18: StatusDoing,
			19: StatusDoing, 20: StatusDoing},
		SuperActive: false,
		SuperReward: map[int64]int64{
			1: StatusDoing, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing,
			7: StatusDoing, 8: StatusDoing, 9: StatusDoing, 10: StatusDoing, 11: StatusDoing, 12: StatusDoing,
			13: StatusDoing, 14: StatusDoing, 15: StatusDoing, 16: StatusDoing, 17: StatusDoing, 18: StatusDoing,
			19: StatusDoing, 20: StatusDoing}}
}

func (m *UserData) resetDataNewWeek() {
	m.WeeklyQuest = &WeeklyQuest{
		Counter: map[int64]int64{
			WeeklyQuestIndexClearDungeon:   0,
			WeeklyQuestIndexMergeEquipment: 0,
			WeeklyQuestIndexMergeTechPart:  0,
			WeeklyQuestIndexKillEnemies:    0,
			WeeklyQuestIndexBuyDailySale:   0,
			WeeklyQuestIndexOpenChest:      0},
		Reward: map[int64]int64{
			WeeklyQuestIndexClearDungeon:   StatusDoing,
			WeeklyQuestIndexMergeEquipment: StatusDoing,
			WeeklyQuestIndexMergeTechPart:  StatusDoing,
			WeeklyQuestIndexKillEnemies:    StatusDoing,
			WeeklyQuestIndexBuyDailySale:   StatusDoing,
			WeeklyQuestIndexOpenChest:      StatusDoing},
		Point: 0,
		Bonus: map[int64]int64{
			1: StatusDoing,
			2: StatusDoing,
			3: StatusDoing,
			4: StatusDoing,
			5: StatusDoing}}
	m.Discount.WeeklyCount = 0
	m.Discount.WeeklyReward = map[int64]bool{1: false, 2: false, 3: false, 4: false, 5: false, 6: false}
	m.Discount.WeeklyBonus = map[int64]int64{1: StatusDoing, 2: StatusDoing, 3: StatusDoing}
}

func (m *UserData) resetDataNewDay() {
	m.Currency.MaxEnergy = USERConfig.CFCurrency.MaxEnergy

	m.Exploration.Quick = USERConfig.CFExploration.NumberOfQuick
	m.Exploration.Ads = USERConfig.CFExploration.NumberOfAds

	m.DailyReward.Day += 1
	m.DailyReward.Reward = StatusDone
	m.DailyReward.Bonus = StatusDone

	m.DailyQuest = &DailyQuest{
		Counter: map[int64]int64{
			DailyQuestIndexPlayCampaign:     0,
			DailyQuestIndexUpgradeEquipment: 0,
			DailyQuestIndexClaimExploration: 0,
			DailyQuestIndexBuyEnergy:        0,
			DailyQuestIndexOpenChest:        0,
			DailyQuestIndexMakePurchase:     0},
		Reward: map[int64]int64{
			DailyQuestIndexPlayCampaign:     StatusDoing,
			DailyQuestIndexUpgradeEquipment: StatusDoing,
			DailyQuestIndexClaimExploration: StatusDoing,
			DailyQuestIndexBuyEnergy:        StatusDoing,
			DailyQuestIndexOpenChest:        StatusDoing,
			DailyQuestIndexMakePurchase:     StatusDoing},
		Point: 0,
		Bonus: map[int64]int64{
			1: StatusDoing,
			2: StatusDoing,
			3: StatusDoing,
			4: StatusDoing,
			5: StatusDoing}}
	m.ChangeAchievement(AchievementIndexLogin, 1)

	m.RookieLogin.Day += 1
	if m.RookieLogin.Day <= 7 {
		for i := int64(1); i <= m.RookieLogin.Day; i++ {
			if m.RookieLogin.Reward[m.RookieLogin.Day] == StatusDoing {
				m.RookieLogin.Reward[m.RookieLogin.Day] = StatusDone
			}
		}
	}

	m.Quest7Days.Day += 1
	// TODO m.Quest7Days

	m.Shop.DailySales = map[int64]bool{
		1: false, 2: false, 3: false, 4: false, 5: false, 6: false}
	m.Shop.AdsRareChest = USERConfig.CFShop.AdsRareChestNewDay
	m.Shop.AdsMythChest = USERConfig.CFShop.AdsMythChestNewDay
	m.Shop.GemPacks = map[int64]bool{
		1: false, 2: false, 3: false}
	m.Shop.GoldPacks = map[int64]bool{
		1: false, 2: false}
	m.Shop.EnergyPacks = map[int64]bool{
		1: false, 2: false, 3: false, 4: false, 5: false, 6: false}

	m.Discount.DailyCount = 0
	m.Discount.DailyReward = map[int64]bool{1: false, 2: false, 3: false, 4: false, 5: false, 6: false}
	m.Discount.DailyBonus = map[int64]int64{1: StatusDoing, 2: StatusDoing, 3: StatusDoing}

	if m.MonthlyCard.TimeNormal > 0 {
		m.MonthlyCard.TimeNormal -= 1
		if m.MonthlyCard.TimeNormal > 0 {
			m.MonthlyCard.RewardNormal = StatusDone
			m.Exploration.Quick += USERConfig.CFMonthlyCard.NormalIncreaseQuickPatrol
			m.Currency.MaxEnergy += USERConfig.CFMonthlyCard.NormalIncreaseMaxEnergy
		}
	}
	if m.MonthlyCard.TimeSuper > 0 {
		m.MonthlyCard.TimeSuper -= 1
		if m.MonthlyCard.TimeSuper > 0 {
			m.MonthlyCard.RewardSuper = StatusDone
			m.Exploration.Quick += USERConfig.CFMonthlyCard.SuperIncreaseQuickPatrol
			m.Currency.MaxEnergy += USERConfig.CFMonthlyCard.SuperIncreaseMaxEnergy
		}
	}
}

func (m *UserData) GetDefault() {
	m.Id = primitive.NewObjectID().Hex()
	m.Info = &UserInfo{
		DeviceId: "", FacebookId: "", GoogleId: "",
		AppleId: "", PlayGamesId: "", GameCenterId: "",
		Token: ""}
	m.Profile = &UserProfile{
		Id: m.Id, Block: false, Name: m.Id, Avatar: 0, VipPoint: 0, Exp: 0, Level: 1, Country: "",
		Hero: &Hero{Index: 1, Rarity: 1, Level: 1}, Equipment: nil, TechPart: nil, Pet: nil,
		Campaign: 0, BossMode: 0, Dungeon: 0, PvpWin: 0, KillEnemy: 0, KillBoss: 0,
		PlayTime: 0, RankGlobal: 0, RankCountry: 0}
	m.MailBox = map[string]*MailBox{}
	m.Currency = &Currency{
		Gem: 0, Gold: 0, Energy: 50, EnergyTime: time.Now().UnixMilli(), MaxEnergy: 50, RevivalCoin: 0}
	m.Stone = map[int64]int64{}
	m.Hero = map[int64]*Hero{
		1: {Index: 1, Rarity: 1, Level: 1}}
	m.PetPiece = map[int64]int64{}
	m.Pet = map[int64]*Pet{}
	m.Design = map[int64]int64{
		EquipmentTypeWeapon: 0, EquipmentTypeSuit: 0,
		EquipmentTypePendant: 0, EquipmentTypeBelt: 0,
		EquipmentTypeGlove: 0, EquipmentTypeBoots: 0}
	m.Equipment = map[int64]*Equipment{}
	m.Design = map[int64]int64{
		TechPartTypeAtk: 0, TechPartTypeHp: 0}
	m.TechUsed = map[int64]int64{}
	m.TechPart = map[int64]*TechPart{}
	m.Talent = &Talent{
		Normal: 0, Special: 0}
	m.Campaign = &Campaign{
		Current:     1,
		PlayTime:    map[int64]int64{1: 0},
		RewardStep1: map[int64]int64{1: StatusDoing},
		RewardStep2: map[int64]int64{1: StatusDoing},
		RewardStep3: map[int64]int64{1: StatusDoing}}
	m.BossMode = &BossMode{
		NormalReward: map[int64]int64{},
		HardReward:   map[int64]int64{},
		CrazyReward:  map[int64]int64{}}
	m.Dungeon = &Dungeon{
		NormalReward: map[int64]int64{},
		HardReward:   map[int64]int64{},
		CrazyReward:  map[int64]int64{}}
	m.Exploration = &Exploration{
		Time:  time.Now().UnixMilli(),
		Quick: USERConfig.CFExploration.NumberOfQuick,
		Ads:   USERConfig.CFExploration.NumberOfAds}
	m.DailyReward = &DailyReward{
		Day:    1,
		Reward: StatusDone,
		Bonus:  StatusDone}
	m.DailyQuest = &DailyQuest{
		Counter: map[int64]int64{
			DailyQuestIndexPlayCampaign:     0,
			DailyQuestIndexUpgradeEquipment: 0,
			DailyQuestIndexClaimExploration: 0,
			DailyQuestIndexBuyEnergy:        0,
			DailyQuestIndexOpenChest:        0,
			DailyQuestIndexMakePurchase:     0},
		Reward: map[int64]int64{
			DailyQuestIndexPlayCampaign:     StatusDoing,
			DailyQuestIndexUpgradeEquipment: StatusDoing,
			DailyQuestIndexClaimExploration: StatusDoing,
			DailyQuestIndexBuyEnergy:        StatusDoing,
			DailyQuestIndexOpenChest:        StatusDoing,
			DailyQuestIndexMakePurchase:     StatusDoing},
		Point: 0,
		Bonus: map[int64]int64{
			1: StatusDoing,
			2: StatusDoing,
			3: StatusDoing,
			4: StatusDoing,
			5: StatusDoing}}
	m.WeeklyQuest = &WeeklyQuest{
		Counter: map[int64]int64{
			WeeklyQuestIndexClearDungeon:   0,
			WeeklyQuestIndexMergeEquipment: 0,
			WeeklyQuestIndexMergeTechPart:  0,
			WeeklyQuestIndexKillEnemies:    0,
			WeeklyQuestIndexBuyDailySale:   0,
			WeeklyQuestIndexOpenChest:      0},
		Reward: map[int64]int64{
			WeeklyQuestIndexClearDungeon:   StatusDoing,
			WeeklyQuestIndexMergeEquipment: StatusDoing,
			WeeklyQuestIndexMergeTechPart:  StatusDoing,
			WeeklyQuestIndexKillEnemies:    StatusDoing,
			WeeklyQuestIndexBuyDailySale:   StatusDoing,
			WeeklyQuestIndexOpenChest:      StatusDoing},
		Point: 0,
		Bonus: map[int64]int64{
			1: StatusDoing,
			2: StatusDoing,
			3: StatusDoing,
			4: StatusDoing,
			5: StatusDoing}}
	m.Achievement = &Achievement{
		Counter: map[int64]int64{
			AchievementIndexLogin:                1,
			AchievementIndexReachUserLevel:       1,
			AchievementIndexPassCampaign:         0,
			AchievementIndexPlayDungeon:          0,
			AchievementIndexUnlockTalentNormal:   0,
			AchievementIndexUnlockTalentSpecial:  0,
			AchievementIndexMakePurchase:         0,
			AchievementIndexOpenChest:            0,
			AchievementIndexSpendGem:             0,
			AchievementIndexSpendGold:            0,
			AchievementIndexKillEnemies:          0,
			AchievementIndexKillBosses:           0,
			AchievementIndexUnlockHero:           1,
			AchievementIndexReachHeroRarity:      1,
			AchievementIndexReachHeroLevel:       1,
			AchievementIndexMergeEquipment:       0,
			AchievementIndexReachEquipmentRarity: 0,
			AchievementIndexReachEquipmentLevel:  0,
			AchievementIndexMergeTechPart:        0,
			AchievementIndexReachTechPartRarity:  0,
			AchievementIndexReachTechPartLevel:   0,
			AchievementIndexUnlockPet:            0,
			AchievementIndexReachPetRarity:       0,
			AchievementIndexReachPetLevel:        0},
		Target: map[int64]int64{
			AchievementIndexLogin:                1,
			AchievementIndexReachUserLevel:       1,
			AchievementIndexPassCampaign:         1,
			AchievementIndexPlayDungeon:          1,
			AchievementIndexUnlockTalentNormal:   1,
			AchievementIndexUnlockTalentSpecial:  1,
			AchievementIndexMakePurchase:         1,
			AchievementIndexOpenChest:            1,
			AchievementIndexSpendGem:             1,
			AchievementIndexSpendGold:            1,
			AchievementIndexKillEnemies:          1,
			AchievementIndexKillBosses:           1,
			AchievementIndexUnlockHero:           1,
			AchievementIndexReachHeroRarity:      1,
			AchievementIndexReachHeroLevel:       1,
			AchievementIndexMergeEquipment:       1,
			AchievementIndexReachEquipmentRarity: 1,
			AchievementIndexReachEquipmentLevel:  1,
			AchievementIndexMergeTechPart:        1,
			AchievementIndexReachTechPartRarity:  1,
			AchievementIndexReachTechPartLevel:   1,
			AchievementIndexUnlockPet:            1,
			AchievementIndexReachPetRarity:       1,
			AchievementIndexReachPetLevel:        1},
		Reward: map[int64]int64{
			AchievementIndexLogin:                StatusDone,
			AchievementIndexReachUserLevel:       StatusDoing,
			AchievementIndexPassCampaign:         StatusDoing,
			AchievementIndexPlayDungeon:          StatusDoing,
			AchievementIndexUnlockTalentNormal:   StatusDoing,
			AchievementIndexUnlockTalentSpecial:  StatusDoing,
			AchievementIndexMakePurchase:         StatusDoing,
			AchievementIndexOpenChest:            StatusDoing,
			AchievementIndexSpendGem:             StatusDoing,
			AchievementIndexSpendGold:            StatusDoing,
			AchievementIndexKillEnemies:          StatusDoing,
			AchievementIndexKillBosses:           StatusDoing,
			AchievementIndexUnlockHero:           StatusDone,
			AchievementIndexReachHeroRarity:      StatusDoing,
			AchievementIndexReachHeroLevel:       StatusDoing,
			AchievementIndexMergeEquipment:       StatusDoing,
			AchievementIndexReachEquipmentRarity: StatusDoing,
			AchievementIndexReachEquipmentLevel:  StatusDoing,
			AchievementIndexMergeTechPart:        StatusDoing,
			AchievementIndexReachTechPartRarity:  StatusDoing,
			AchievementIndexReachTechPartLevel:   StatusDoing,
			AchievementIndexUnlockPet:            StatusDoing,
			AchievementIndexReachPetRarity:       StatusDoing,
			AchievementIndexReachPetLevel:        StatusDoing}}
	m.RookieLogin = &RookieLogin{
		Day: 1,
		Reward: map[int64]int64{
			1: StatusDone, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing, 7: StatusDoing}}
	m.Quest7Days = &Quest7Days{
		Day: 1,
		Counters: map[int64]int64{
			Quest7DaysDay1Login:                   0,
			Quest7DaysDay1ReachLevel:              0,
			Quest7DaysDay1TalentNormalLevel:       0,
			Quest7DaysDay1TalentSpecialLevel:      0,
			Quest7DaysDay1EquipNormalEquipment:    0,
			Quest7DaysDay1EquipEquipmentLevel2:    0,
			Quest7DaysDay1KillEnemies:             0,
			Quest7DaysDay1ObtainGold:              0,
			Quest7DaysDay1ObtainGem:               0,
			Quest7DaysDay1OpenMythChest:           0,
			Quest7DaysDay1PassChapter:             0,
			Quest7DaysDay2Login:                   0,
			Quest7DaysDay2ReachLevel:              0,
			Quest7DaysDay2TalentNormalLevel:       0,
			Quest7DaysDay2TalentSpecialLevel:      0,
			Quest7DaysDay2EquipNormalEquipment:    0,
			Quest7DaysDay2EquipEquipmentLevel3:    0,
			Quest7DaysDay2KillEnemies:             0,
			Quest7DaysDay2ObtainGold:              0,
			Quest7DaysDay2ObtainGem:               0,
			Quest7DaysDay2OpenMythChest:           0,
			Quest7DaysDay2PassChapter:             0,
			Quest7DaysDay3Login:                   0,
			Quest7DaysDay3ReachLevel:              0,
			Quest7DaysDay3TalentNormalLevel:       0,
			Quest7DaysDay3TalentSpecialLevel:      0,
			Quest7DaysDay3EquipBetterEquipment:    0,
			Quest7DaysDay3EquipGoodEquipment:      0,
			Quest7DaysDay3EquipEquipmentLevel8:    0,
			Quest7DaysDay3EquipEquipmentLevel5:    0,
			Quest7DaysDay3KillEnemies:             0,
			Quest7DaysDay3ObtainGold:              0,
			Quest7DaysDay3ObtainGem:               0,
			Quest7DaysDay3OpenMythChest:           0,
			Quest7DaysDay3PassChapter:             0,
			Quest7DaysDay4Login:                   0,
			Quest7DaysDay4ReachLevel:              0,
			Quest7DaysDay4TalentNormalLevel:       0,
			Quest7DaysDay4TalentSpecialLevel:      0,
			Quest7DaysDay4EquipExcellentEquipment: 0,
			Quest7DaysDay4EquipBetterEquipment:    0,
			Quest7DaysDay4EquipEquipmentLevel10:   0,
			Quest7DaysDay4EquipEquipmentLevel8:    0,
			Quest7DaysDay4KillEnemies:             0,
			Quest7DaysDay4ObtainGold:              0,
			Quest7DaysDay4ObtainGem:               0,
			Quest7DaysDay4OpenMythChest:           0,
			Quest7DaysDay4PassChapter:             0,
			Quest7DaysDay5Login:                   0,
			Quest7DaysDay5ReachLevel:              0,
			Quest7DaysDay5TalentNormalLevel:       0,
			Quest7DaysDay5TalentSpecialLevel:      0,
			Quest7DaysDay5EquipExcellentEquipment: 0,
			Quest7DaysDay5EquipEquipmentLevel15:   0,
			Quest7DaysDay5EquipEquipmentLevel14:   0,
			Quest7DaysDay5EquipEquipmentLevel10:   0,
			Quest7DaysDay5KillEnemies:             0,
			Quest7DaysDay5ObtainGold:              0,
			Quest7DaysDay5ObtainGem:               0,
			Quest7DaysDay5OpenMythChest:           0,
			Quest7DaysDay5PassChapter:             0,
			Quest7DaysDay6Login:                   0,
			Quest7DaysDay6ReachLevel:              0,
			Quest7DaysDay6TalentNormalLevel:       0,
			Quest7DaysDay6TalentSpecialLevel:      0,
			Quest7DaysDay6EquipEpicEquipment:      0,
			Quest7DaysDay6EquipEquipmentLevel20:   0,
			Quest7DaysDay6EquipEquipmentLevel18:   0,
			Quest7DaysDay6KillEnemies:             0,
			Quest7DaysDay6ObtainGold1:             0,
			Quest7DaysDay6ObtainGold2:             0,
			Quest7DaysDay6ObtainGem1:              0,
			Quest7DaysDay6ObtainGem2:              0,
			Quest7DaysDay6OpenMythChest:           0,
			Quest7DaysDay6PassChapter:             0,
			Quest7DaysDay7Login:                   0,
			Quest7DaysDay7ReachLevel:              0,
			Quest7DaysDay7TalentNormalLevel:       0,
			Quest7DaysDay7TalentSpecialLevel:      0,
			Quest7DaysDay7EquipEquipmentLevel25:   0,
			Quest7DaysDay7EquipEquipmentLevel20:   0,
			Quest7DaysDay7KillEnemies1:            0,
			Quest7DaysDay7KillEnemies2:            0,
			Quest7DaysDay7ObtainGold1:             0,
			Quest7DaysDay7ObtainGold2:             0,
			Quest7DaysDay7ObtainGem1:              0,
			Quest7DaysDay7ObtainGem2:              0,
			Quest7DaysDay7ObtainGem3:              0,
			Quest7DaysDay7OpenMythChest1:          0,
			Quest7DaysDay7OpenMythChest2:          0,
			Quest7DaysDay7OpenMythChest3:          0,
			Quest7DaysDay7PassChapter:             0},
		Rewards: map[int64]int64{
			Quest7DaysDay1Login:                   StatusDoing,
			Quest7DaysDay1ReachLevel:              StatusDoing,
			Quest7DaysDay1TalentNormalLevel:       StatusDoing,
			Quest7DaysDay1TalentSpecialLevel:      StatusDoing,
			Quest7DaysDay1EquipNormalEquipment:    StatusDoing,
			Quest7DaysDay1EquipEquipmentLevel2:    StatusDoing,
			Quest7DaysDay1KillEnemies:             StatusDoing,
			Quest7DaysDay1ObtainGold:              StatusDoing,
			Quest7DaysDay1ObtainGem:               StatusDoing,
			Quest7DaysDay1OpenMythChest:           StatusDoing,
			Quest7DaysDay1PassChapter:             StatusDoing,
			Quest7DaysDay2Login:                   StatusDoing,
			Quest7DaysDay2ReachLevel:              StatusDoing,
			Quest7DaysDay2TalentNormalLevel:       StatusDoing,
			Quest7DaysDay2TalentSpecialLevel:      StatusDoing,
			Quest7DaysDay2EquipNormalEquipment:    StatusDoing,
			Quest7DaysDay2EquipEquipmentLevel3:    StatusDoing,
			Quest7DaysDay2KillEnemies:             StatusDoing,
			Quest7DaysDay2ObtainGold:              StatusDoing,
			Quest7DaysDay2ObtainGem:               StatusDoing,
			Quest7DaysDay2OpenMythChest:           StatusDoing,
			Quest7DaysDay2PassChapter:             StatusDoing,
			Quest7DaysDay3Login:                   StatusDoing,
			Quest7DaysDay3ReachLevel:              StatusDoing,
			Quest7DaysDay3TalentNormalLevel:       StatusDoing,
			Quest7DaysDay3TalentSpecialLevel:      StatusDoing,
			Quest7DaysDay3EquipBetterEquipment:    StatusDoing,
			Quest7DaysDay3EquipGoodEquipment:      StatusDoing,
			Quest7DaysDay3EquipEquipmentLevel8:    StatusDoing,
			Quest7DaysDay3EquipEquipmentLevel5:    StatusDoing,
			Quest7DaysDay3KillEnemies:             StatusDoing,
			Quest7DaysDay3ObtainGold:              StatusDoing,
			Quest7DaysDay3ObtainGem:               StatusDoing,
			Quest7DaysDay3OpenMythChest:           StatusDoing,
			Quest7DaysDay3PassChapter:             StatusDoing,
			Quest7DaysDay4Login:                   StatusDoing,
			Quest7DaysDay4ReachLevel:              StatusDoing,
			Quest7DaysDay4TalentNormalLevel:       StatusDoing,
			Quest7DaysDay4TalentSpecialLevel:      StatusDoing,
			Quest7DaysDay4EquipExcellentEquipment: StatusDoing,
			Quest7DaysDay4EquipBetterEquipment:    StatusDoing,
			Quest7DaysDay4EquipEquipmentLevel10:   StatusDoing,
			Quest7DaysDay4EquipEquipmentLevel8:    StatusDoing,
			Quest7DaysDay4KillEnemies:             StatusDoing,
			Quest7DaysDay4ObtainGold:              StatusDoing,
			Quest7DaysDay4ObtainGem:               StatusDoing,
			Quest7DaysDay4OpenMythChest:           StatusDoing,
			Quest7DaysDay4PassChapter:             StatusDoing,
			Quest7DaysDay5Login:                   StatusDoing,
			Quest7DaysDay5ReachLevel:              StatusDoing,
			Quest7DaysDay5TalentNormalLevel:       StatusDoing,
			Quest7DaysDay5TalentSpecialLevel:      StatusDoing,
			Quest7DaysDay5EquipExcellentEquipment: StatusDoing,
			Quest7DaysDay5EquipEquipmentLevel15:   StatusDoing,
			Quest7DaysDay5EquipEquipmentLevel14:   StatusDoing,
			Quest7DaysDay5EquipEquipmentLevel10:   StatusDoing,
			Quest7DaysDay5KillEnemies:             StatusDoing,
			Quest7DaysDay5ObtainGold:              StatusDoing,
			Quest7DaysDay5ObtainGem:               StatusDoing,
			Quest7DaysDay5OpenMythChest:           StatusDoing,
			Quest7DaysDay5PassChapter:             StatusDoing,
			Quest7DaysDay6Login:                   StatusDoing,
			Quest7DaysDay6ReachLevel:              StatusDoing,
			Quest7DaysDay6TalentNormalLevel:       StatusDoing,
			Quest7DaysDay6TalentSpecialLevel:      StatusDoing,
			Quest7DaysDay6EquipEpicEquipment:      StatusDoing,
			Quest7DaysDay6EquipEquipmentLevel20:   StatusDoing,
			Quest7DaysDay6EquipEquipmentLevel18:   StatusDoing,
			Quest7DaysDay6KillEnemies:             StatusDoing,
			Quest7DaysDay6ObtainGold1:             StatusDoing,
			Quest7DaysDay6ObtainGold2:             StatusDoing,
			Quest7DaysDay6ObtainGem1:              StatusDoing,
			Quest7DaysDay6ObtainGem2:              StatusDoing,
			Quest7DaysDay6OpenMythChest:           StatusDoing,
			Quest7DaysDay6PassChapter:             StatusDoing,
			Quest7DaysDay7Login:                   StatusDoing,
			Quest7DaysDay7ReachLevel:              StatusDoing,
			Quest7DaysDay7TalentNormalLevel:       StatusDoing,
			Quest7DaysDay7TalentSpecialLevel:      StatusDoing,
			Quest7DaysDay7EquipEquipmentLevel25:   StatusDoing,
			Quest7DaysDay7EquipEquipmentLevel20:   StatusDoing,
			Quest7DaysDay7KillEnemies1:            StatusDoing,
			Quest7DaysDay7KillEnemies2:            StatusDoing,
			Quest7DaysDay7ObtainGold1:             StatusDoing,
			Quest7DaysDay7ObtainGold2:             StatusDoing,
			Quest7DaysDay7ObtainGem1:              StatusDoing,
			Quest7DaysDay7ObtainGem2:              StatusDoing,
			Quest7DaysDay7ObtainGem3:              StatusDoing,
			Quest7DaysDay7OpenMythChest1:          StatusDoing,
			Quest7DaysDay7OpenMythChest2:          StatusDoing,
			Quest7DaysDay7OpenMythChest3:          StatusDoing,
			Quest7DaysDay7PassChapter:             StatusDoing},
		Point: 0,
		Bonus: map[int64]int64{
			1: StatusDoing,
			2: StatusDoing,
			3: StatusDoing,
			4: StatusDoing,
			5: StatusDoing,
			6: StatusDoing,
			7: StatusDoing,
			8: StatusDoing}}
	m.Shop = &Shop{
		MonthlyInApp: StatusDoing,
		ChapterPacks: map[int64]bool{},
		DailySales: map[int64]bool{
			1: false, 2: false, 3: false, 4: false, 5: false, 6: false},
		AdsRareChest: 1, KeyRareChest: 0,
		AdsMythChest: 1, KeyMythChest: 0,
		CountMythChest: 0,
		GemPacks: map[int64]bool{
			1: false, 2: false, 3: false},
		GoldPacks: map[int64]bool{
			1: false, 2: false},
		EnergyPacks: map[int64]bool{
			1: false, 2: false, 3: false, 4: false, 5: false, 6: false}}
	m.Discount = &Discount{
		DailyCount:    0,
		DailyReward:   map[int64]bool{1: false, 2: false, 3: false, 4: false, 5: false, 6: false},
		DailyBonus:    map[int64]int64{1: StatusDoing, 2: StatusDoing, 3: StatusDoing},
		WeeklyCount:   0,
		WeeklyReward:  map[int64]bool{1: false, 2: false, 3: false, 4: false, 5: false, 6: false},
		WeeklyBonus:   map[int64]int64{1: StatusDoing, 2: StatusDoing, 3: StatusDoing},
		MonthlyCount:  0,
		MonthlyReward: map[int64]bool{1: false, 2: false, 3: false, 4: false, 5: false, 6: false},
		MonthlyBonus:  map[int64]int64{1: StatusDoing, 2: StatusDoing, 3: StatusDoing},
	}
	m.MonthlyCard = &MonthlyCard{
		TimeNormal: 0, RewardNormal: StatusDoing,
		TimeSuper: 0, RewardSuper: StatusDoing}
	m.BattlePass = &BattlePass{
		Point: 0,
		FreeReward: map[int64]int64{
			1: StatusDoing, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing,
			7: StatusDoing, 8: StatusDoing, 9: StatusDoing, 10: StatusDoing, 11: StatusDoing, 12: StatusDoing,
			13: StatusDoing, 14: StatusDoing, 15: StatusDoing, 16: StatusDoing, 17: StatusDoing, 18: StatusDoing,
			19: StatusDoing, 20: StatusDoing},
		NormalActive: false,
		NormalReward: map[int64]int64{
			1: StatusDoing, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing,
			7: StatusDoing, 8: StatusDoing, 9: StatusDoing, 10: StatusDoing, 11: StatusDoing, 12: StatusDoing,
			13: StatusDoing, 14: StatusDoing, 15: StatusDoing, 16: StatusDoing, 17: StatusDoing, 18: StatusDoing,
			19: StatusDoing, 20: StatusDoing},
		SuperActive: false,
		SuperReward: map[int64]int64{
			1: StatusDoing, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing,
			7: StatusDoing, 8: StatusDoing, 9: StatusDoing, 10: StatusDoing, 11: StatusDoing, 12: StatusDoing,
			13: StatusDoing, 14: StatusDoing, 15: StatusDoing, 16: StatusDoing, 17: StatusDoing, 18: StatusDoing,
			19: StatusDoing, 20: StatusDoing}}
	m.LevelPass1 = &LevelPass{
		FreeReward: map[int64]int64{
			1: StatusDoing, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing,
			7: StatusDoing, 8: StatusDoing, 9: StatusDoing, 10: StatusDoing},
		NormalActive: false,
		NormalReward: map[int64]int64{
			1: StatusDoing, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing,
			7: StatusDoing, 8: StatusDoing, 9: StatusDoing, 10: StatusDoing},
		SuperActive: false,
		SuperReward: map[int64]int64{
			1: StatusDoing, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing,
			7: StatusDoing, 8: StatusDoing, 9: StatusDoing, 10: StatusDoing}}
	m.LevelPass2 = &LevelPass{
		FreeReward: map[int64]int64{
			1: StatusDoing, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing,
			7: StatusDoing, 8: StatusDoing, 9: StatusDoing, 10: StatusDoing},
		NormalActive: false,
		NormalReward: map[int64]int64{
			1: StatusDoing, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing,
			7: StatusDoing, 8: StatusDoing, 9: StatusDoing, 10: StatusDoing},
		SuperActive: false,
		SuperReward: map[int64]int64{
			1: StatusDoing, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing,
			7: StatusDoing, 8: StatusDoing, 9: StatusDoing, 10: StatusDoing}}
	m.CampaignPass = &CampaignPass{
		FreeReward: map[int64]int64{
			1: StatusDoing, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing,
			7: StatusDoing, 8: StatusDoing, 9: StatusDoing, 10: StatusDoing},
		NormalActive: false,
		NormalReward: map[int64]int64{
			1: StatusDoing, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing,
			7: StatusDoing, 8: StatusDoing, 9: StatusDoing, 10: StatusDoing},
		SuperActive: false,
		SuperReward: map[int64]int64{
			1: StatusDoing, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing,
			7: StatusDoing, 8: StatusDoing, 9: StatusDoing, 10: StatusDoing}}
	m.DungeonPass = &DungeonPass{
		NormalReward: map[int64]int64{
			1: StatusDoing, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing,
			7: StatusDoing, 8: StatusDoing, 9: StatusDoing, 10: StatusDoing},
		HardActive: false,
		HardReward: map[int64]int64{
			1: StatusDoing, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing,
			7: StatusDoing, 8: StatusDoing, 9: StatusDoing, 10: StatusDoing},
		CrazyActive: false,
		CrazyReward: map[int64]int64{
			1: StatusDoing, 2: StatusDoing, 3: StatusDoing, 4: StatusDoing, 5: StatusDoing, 6: StatusDoing,
			7: StatusDoing, 8: StatusDoing, 9: StatusDoing, 10: StatusDoing}}
	m.GoldPiggy = &GoldPiggy{
		Counter: 0,
		Size:    GoldPiggySizeS,
	}
	m.LatestSaved = time.Now().UnixMilli()
}
