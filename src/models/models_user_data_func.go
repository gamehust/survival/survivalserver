package models

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"time"
)

func (m *UserData) GenerateToken() error {
	newKey := make([]byte, 16)
	if _, err := rand.Read(newKey); err != nil {
		return err
	}
	m.Info.Token = hex.EncodeToString(newKey)
	m.Info.TokenTime = time.Now().UnixMilli()
	return nil
}

func (m *UserData) ValidateToken(token string) error {
	if m.Info.Token != token || time.Now().UnixMilli()-m.Info.TokenTime > 7*24*3600*1000 {
		return fmt.Errorf("token is invalid")
	}
	return nil
}

func (m *UserData) AddVipPoint(value int64) error {
	m.Profile.VipPoint += value
	if value > 0 {
		if m.Shop.MonthlyInApp == StatusDoing {
			m.Shop.MonthlyInApp = StatusDone
		}
		m.ChangeDailyQuest(DailyQuestIndexMakePurchase, 1)
		m.ChangeAchievement(AchievementIndexMakePurchase, 1)
	}
	return nil
}

func (m *UserData) AddExp(value int64) error {
	m.Profile.Exp += value
	for {
		expNeedPass := USERConfig.CFCurrency.ExpPassLevel[m.Profile.Level]
		if m.Profile.Exp < expNeedPass {
			break
		}
		m.Profile.Exp -= expNeedPass
		m.Profile.Level += 1
		m.SetAchievement(AchievementIndexReachUserLevel, m.Profile.Level)
		m.UpdateEventLevelPass()
	}
	return nil
}

func (m *UserData) AddGem(value int64) error {
	if m.Currency.Gem+value < 0 {
		return fmt.Errorf("gem not enough")
	}
	if value < 0 {
		m.ChangeAchievement(AchievementIndexSpendGem, -value)
	}
	m.Currency.Gem += value
	return nil
}

func (m *UserData) AddGold(value int64) error {
	if m.Currency.Gold+value < 0 {
		return fmt.Errorf("gold not enough")
	}
	if value < 0 {
		m.ChangeAchievement(AchievementIndexSpendGold, -value)
	}
	m.Currency.Gold += value
	return nil
}

func (m *UserData) AddEnergy(timeAdd int64, value int64) error {
	rcv := (timeAdd - m.Currency.EnergyTime) / USERConfig.CFCurrency.EnergyRecover
	if m.Currency.Energy >= m.Currency.MaxEnergy {
		m.Currency.EnergyTime = time.Now().UnixMilli()
	} else if rcv > 0 {
		m.Currency.Energy += rcv
		m.Currency.EnergyTime += rcv * USERConfig.CFCurrency.EnergyRecover
		if m.Currency.Energy >= m.Currency.MaxEnergy {
			m.Currency.Energy = m.Currency.MaxEnergy
			m.Currency.EnergyTime = time.Now().UnixMilli()
		}
	}
	if m.Currency.Energy+value < 0 {
		return fmt.Errorf("energy not enough")
	}
	m.Currency.Energy += value
	return nil
}

func (m *UserData) AddRevivalCoin(value int64) error {
	if m.Currency.RevivalCoin+value < 0 {
		return fmt.Errorf("revival coin not enough")
	}
	m.Currency.RevivalCoin += value
	return nil
}

func (m *UserData) AddStone(index, value int64) error {
	old := m.Stone[index]
	if old+value < 0 {
		return fmt.Errorf("stone %d not enough", index)
	}
	m.Stone[index] = old + value
	return nil
}

func (m *UserData) AddDesign(index, value int64) error {
	old := m.Design[index]
	if old+value < 0 {
		return fmt.Errorf("design %d not enough", index)
	}
	m.Design[index] = old + value
	return nil
}

func (m *UserData) AddTechUsed(index, value int64) error {
	old := m.TechUsed[index]
	if old+value < 0 {
		return fmt.Errorf("tech used %d not enough", index)
	}
	m.TechUsed[index] = old + value
	return nil
}

func (m *UserData) AddKeyRareChest(value int64) error {
	if m.Shop.KeyRareChest+value < 0 {
		return fmt.Errorf("key rare chest not enough")
	}
	m.Shop.KeyRareChest += value
	return nil
}

func (m *UserData) AddKeyMythChest(value int64) error {
	if m.Shop.KeyMythChest+value < 0 {
		return fmt.Errorf("key myth chest not enough")
	}
	m.Shop.KeyMythChest += value
	return nil
}

func (m *UserData) AddEquipment(eqpt *Equipment) error {
	if eqpt == nil || m.Equipment[eqpt.Id] != nil || eqpt.Type < EquipmentTypeWeapon || eqpt.Type > EquipmentTypeBoots ||
		eqpt.Rarity < RarityNormal || eqpt.Rarity > RarityImmortal5 || eqpt.Level < 1 || eqpt.Level > 220 {
		return fmt.Errorf("invalid param")
	}
	m.Equipment[eqpt.Id] = eqpt
	return nil
}

func (m *UserData) RemoveEquipment(id int64) error {
	delete(m.Equipment, id)
	return nil
}

func (m *UserData) AddTechPart(tech *TechPart) error {
	if tech == nil || m.TechPart[tech.Id] != nil || tech.Rarity < RarityNormal || tech.Rarity > RarityImmortal5 {
		return fmt.Errorf("invalid param")
	}
	m.TechPart[tech.Id] = tech
	return nil
}

func (m *UserData) RemoveTechPart(id int64) error {
	delete(m.TechPart, id)
	return nil
}

func (m *UserData) AddPetPiece(index, value int64) error {
	old := m.PetPiece[index]
	if old+value < 0 {
		return fmt.Errorf("pet piece %d not enough", index)
	}
	m.PetPiece[index] = old + value
	return nil
}

func (m *UserData) ChangeDailyQuest(index, value int64) {
	if m.DailyQuest.Reward[index] == StatusDoing {
		m.DailyQuest.Counter[index] += value
		if m.DailyQuest.Counter[index] >= USERConfig.CFDailyQuest.Target[index] {
			m.DailyQuest.Counter[index] = USERConfig.CFDailyQuest.Target[index]
			m.DailyQuest.Reward[index] = StatusDone
		}
	}
}

func (m *UserData) SetDailyQuest(index, value int64) {
	if m.DailyQuest.Reward[index] == StatusDoing && m.DailyQuest.Counter[index] < value {
		m.DailyQuest.Counter[index] = value
		if m.DailyQuest.Counter[index] >= USERConfig.CFDailyQuest.Target[index] {
			m.DailyQuest.Counter[index] = USERConfig.CFDailyQuest.Target[index]
			m.DailyQuest.Reward[index] = StatusDone
		}
	}
}

func (m *UserData) ChangeWeeklyQuest(index, value int64) {
	if m.WeeklyQuest.Reward[index] == StatusDoing {
		m.WeeklyQuest.Counter[index] += value
		if m.WeeklyQuest.Counter[index] >= USERConfig.CFWeeklyQuest.Target[index] {
			m.WeeklyQuest.Counter[index] = USERConfig.CFWeeklyQuest.Target[index]
			m.WeeklyQuest.Reward[index] = StatusDone
		}
	}
}

func (m *UserData) SetWeeklyQuest(index, value int64) {
	if m.WeeklyQuest.Reward[index] == StatusDoing && m.WeeklyQuest.Counter[index] < value {
		m.WeeklyQuest.Counter[index] = value
		if m.WeeklyQuest.Counter[index] >= USERConfig.CFWeeklyQuest.Target[index] {
			m.WeeklyQuest.Counter[index] = USERConfig.CFWeeklyQuest.Target[index]
			m.WeeklyQuest.Reward[index] = StatusDone
		}
	}
}

func (m *UserData) ChangeAchievement(index, value int64) {
	m.Achievement.Counter[index] += value
	if m.Achievement.Reward[index] == StatusDoing {
		limitIndex := m.Achievement.Target[index]
		if m.Achievement.Counter[index] >= USERConfig.CFAchievement.Target[index][limitIndex] {
			m.Achievement.Reward[index] = StatusDone
		}
	}
}

func (m *UserData) SetAchievement(index, value int64) {
	if m.Achievement.Counter[index] < value {
		m.Achievement.Counter[index] = value
		if m.Achievement.Reward[index] == StatusDoing {
			limitIndex := m.Achievement.Target[index]
			if m.Achievement.Counter[index] >= USERConfig.CFAchievement.Target[index][limitIndex] {
				m.Achievement.Reward[index] = StatusDone
			}
		}
	}
}

func (m *UserData) ChangeQuest7Days(index, value int64) {

}

func (m *UserData) SetQuest7Days(index, value int64) {

}

func (m *UserData) UpdateEventLevelPass() {
	for index, level := range USERConfig.CFLevelPass1.TargetLevel {
		if m.Profile.Level < level {
			continue
		}
		if m.LevelPass1.FreeReward[index] == StatusDoing {
			m.LevelPass1.FreeReward[index] = StatusDone
		}
		if m.LevelPass1.NormalReward[index] == StatusDoing {
			m.LevelPass1.NormalReward[index] = StatusDone
		}
		if m.LevelPass1.SuperReward[index] == StatusDoing {
			m.LevelPass1.SuperReward[index] = StatusDone
		}
	}
	for index, level := range USERConfig.CFLevelPass2.TargetLevel {
		if m.Profile.Level < level {
			continue
		}
		if m.LevelPass2.FreeReward[index] == StatusDoing {
			m.LevelPass2.FreeReward[index] = StatusDone
		}
		if m.LevelPass2.NormalReward[index] == StatusDoing {
			m.LevelPass2.NormalReward[index] = StatusDone
		}
		if m.LevelPass2.SuperReward[index] == StatusDoing {
			m.LevelPass2.SuperReward[index] = StatusDone
		}
	}
}

func (m *UserData) UpdateGoldPiggy(value int64) {
	max := USERConfig.CFGoldPiggy.RewardMax[m.GoldPiggy.Size]
	m.GoldPiggy.Counter += value
	if m.GoldPiggy.Counter > max {
		m.GoldPiggy.Counter = max
	}
}
