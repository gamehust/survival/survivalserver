package models

import (
	"encoding/json"

	"go.mongodb.org/mongo-driver/bson"
)

type ServerInfo struct {
	Id             string `json:"_id" bson:"_id"`
	ExternalIPAddr string `json:"ei" bson:"ei"`
	PrivateIPAddr  string `json:"pi" bson:"pi"`
	ExternalPort   int64  `json:"ep" bson:"ep"`
}

const (
	ServerBSONId             = "_id"
	ServerBSONExternalIPAddr = "ei"
	ServerBSONPrivateIPAddr  = "pi"
	ServerBSONExternalPort   = "ep"
)

func (m *ServerInfo) MarshalBSON() ([]byte, error) {
	return bson.Marshal(bson.D{
		bson.E{Key: ServerBSONId, Value: m.Id},
		bson.E{Key: ServerBSONExternalIPAddr, Value: m.ExternalIPAddr},
		bson.E{Key: ServerBSONPrivateIPAddr, Value: m.PrivateIPAddr},
		bson.E{Key: ServerBSONExternalPort, Value: m.ExternalPort}})
}

func (m *ServerInfo) UnmarshalBSON(raw []byte) error {
	elements, err := bson.Raw(raw).Elements()
	if err != nil {
		return err
	}
	for _, element := range elements {
		switch element.Key() {
		case ServerBSONId:
			m.Id = element.Value().StringValue()
		case ServerBSONExternalIPAddr:
			m.ExternalIPAddr = element.Value().StringValue()
		case ServerBSONPrivateIPAddr:
			m.PrivateIPAddr = element.Value().StringValue()
		case ServerBSONExternalPort:
			m.ExternalPort = element.Value().Int64()
		}
	}
	return nil
}

func (m *ServerInfo) MarshalJSON() ([]byte, error) {
	obj := make(map[string]interface{})
	obj[ServerBSONId] = m.Id
	obj[ServerBSONExternalIPAddr] = m.ExternalIPAddr
	obj[ServerBSONPrivateIPAddr] = m.PrivateIPAddr
	obj[ServerBSONExternalPort] = m.ExternalPort
	return json.Marshal(obj)
}

func (m *ServerInfo) UnmarshalJSON(raw []byte) error {
	obj := make(map[string]interface{})
	if err := json.Unmarshal(raw, &obj); err != nil {
		return err
	}
	for key, value := range obj {
		switch key {
		case ServerBSONId:
			m.Id = value.(string)
		case ServerBSONExternalIPAddr:
			m.ExternalIPAddr = value.(string)
		case ServerBSONPrivateIPAddr:
			m.PrivateIPAddr = value.(string)
		case ServerBSONExternalPort:
			m.ExternalPort = int64(value.(float64))
		}
	}
	return nil
}

type ServerMonitor struct {
	Id   string `json:"_id" bson:"_id"`
	Warn int64  `json:"warn" bson:"warn"`
	Err  int64  `json:"err" bson:"err"`
	CPU  int64  `json:"cpu" bson:"cpu"`
	MEM  int64  `json:"mem" bson:"mem"`
	TPS  int64  `json:"tps" bson:"tps"`
	CCR  int64  `json:"ccr" bson:"ccr"`
	Time int64  `json:"time" bson:"time"`
}
