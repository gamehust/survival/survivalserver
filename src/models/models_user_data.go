package models

import (
	"time"
)

const (
	IdRegEx = `^[a-zA-Z0-9]{5,15}$`
)

const (
	GiftNameGem int64 = iota + 1
	GiftNameGold
	GiftNameGoldPatrol
	GiftNameEnergy
	GiftNameRevivalCoin
	GiftNameRareChest
	GiftNameMythChest
	GiftNameStoneRandom
	GiftNamePetPieceRandom
	GiftNameDesignRandom
	GiftNameTechUsedRandom

	GiftNameEventNewYearCoin
	GiftNameEventNewYearTicket
)

const (
	GiftNameEquipmentSExcellentRandom int64 = iota + 100
	GiftNameEquipmentNormalRandom
	GiftNameEquipmentGoodRandom
	GiftNameEquipmentBetterRandom
	GiftNameEquipmentExcellentRandom
	GiftNameEquipmentExcellent1Random
	GiftNameEquipmentEpicRandom
	GiftNameEquipmentEpic1Random
	GiftNameEquipmentEpic2Random
	GiftNameEquipmentLegendaryRandom
	GiftNameEquipmentLegendary1Random
	GiftNameEquipmentLegendary2Random
	GiftNameEquipmentLegendary3Random
	GiftNameEquipmentImmortalRandom
	GiftNameEquipmentImmortal1Random
	GiftNameEquipmentImmortal2Random
	GiftNameEquipmentImmortal3Random
	GiftNameEquipmentImmortal4Random
	GiftNameEquipmentImmortal5Random
)

const (
	GiftNameTechPartSExcellentRandom int64 = iota + 200
	GiftNameTechPartNormalRandom
	GiftNameTechPartGoodRandom
	GiftNameTechPartBetterRandom
	GiftNameTechPartExcellentRandom
	GiftNameTechPartExcellent1Random
	GiftNameTechPartEpicRandom
	GiftNameTechPartEpic1Random
	GiftNameTechPartEpic2Random
	GiftNameTechPartLegendaryRandom
	GiftNameTechPartLegendary1Random
	GiftNameTechPartLegendary2Random
	GiftNameTechPartLegendary3Random
	GiftNameTechPartImmortalRandom
	GiftNameTechPartImmortal1Random
	GiftNameTechPartImmortal2Random
	GiftNameTechPartImmortal3Random
	GiftNameTechPartImmortal4Random
	GiftNameTechPartImmortal5Random
)

type UserData struct {
	Id       string              `json:"_id" bson:"_id"`
	Info     *UserInfo           `json:"-" bson:"if"`
	Profile  *UserProfile        `json:"pf" bson:"pf"`
	MailBox  map[string]*MailBox `json:"mb" bson:"mb"`
	GiftCode map[string]int64    `json:"gc" bson:"gc"`
	// friend
	Friends       map[string]int64 `json:"fs" bson:"fs"`
	FriendInvites map[string]int64 `json:"fi" bson:"fi"`
	FriendRequest map[string]int64 `json:"fr" bson:"fr"`
	// asset
	Asset1    map[string]float64   `json:"a1" bson:"a1"`
	Asset2    map[string]string    `json:"a2" bson:"a2"`
	Currency  *Currency            `json:"cc" bson:"cc"`
	Stone     map[int64]int64      `json:"st" bson:"st"`
	Hero      map[int64]*Hero      `json:"hr" bson:"hr"`
	PetPiece  map[int64]int64      `json:"pp" bson:"pp"`
	Pet       map[int64]*Pet       `json:"pe" bson:"pe"`
	Design    map[int64]int64      `json:"de" bson:"de"`
	Equipment map[int64]*Equipment `json:"eq" bson:"eq"`
	TechUsed  map[int64]int64      `json:"tu" bson:"tu"`
	TechPart  map[int64]*TechPart  `json:"tp" bson:"tp"`
	Talent    *Talent              `json:"tl" bson:"tl"`
	// play
	Campaign    *Campaign    `json:"ca" bson:"ca"`
	BossMode    *BossMode    `json:"bm" bson:"bm"`
	Dungeon     *Dungeon     `json:"dg" bson:"dg"`
	Exploration *Exploration `json:"ex" bson:"ex"`
	// quest
	DailyReward *DailyReward `json:"dr" bson:"dr"`
	DailyQuest  *DailyQuest  `json:"dq" bson:"dq"`
	WeeklyQuest *WeeklyQuest `json:"wq" bson:"wq"`
	Achievement *Achievement `json:"ac" bson:"ac"`
	RookieLogin *RookieLogin `json:"rl" bson:"rl"`
	Quest7Days  *Quest7Days  `json:"q7" bson:"q7"`
	// shop
	Shop     *Shop     `json:"sh" bson:"sh"`
	Discount *Discount `json:"dc" bson:"dc"`
	// event - always on
	MonthlyCard  *MonthlyCard  `json:"mc" bson:"mc"`
	BattlePass   *BattlePass   `json:"bp" bson:"bp"`
	LevelPass1   *LevelPass    `json:"l1" bson:"l1"`
	LevelPass2   *LevelPass    `json:"l2" bson:"l2"`
	CampaignPass *CampaignPass `json:"cp" bson:"cp"`
	DungeonPass  *DungeonPass  `json:"dp" bson:"dp"`
	GoldPiggy    *GoldPiggy    `json:"gp" bson:"gp"`
	// event - on off
	EventNewYear      *EventNewYear      `json:"ny" bson:"ny"`
	EventValentine    *EventValentine    `json:"vl" bson:"vl"`
	EventAprilFool    *EventAprilFool    `json:"af" bson:"af"`
	EventChildrenDay  *EventChildrenDay  `json:"cd" bson:"cd"`
	EventAutumnLeaves *EventAutumnLeaves `json:"al" bson:"al"`
	EventHalloween    *EventHalloween    `json:"hl" bson:"hl"`
	EventChristmas    *EventChristmas    `json:"cr" bson:"cr"`
	EventWorldCup     *EventWorldCup     `json:"wc" bson:"wc"`
	// system
	LatestSaved int64          `json:"ls" bson:"ls"`
	Timezone    string         `json:"-" bson:"tz"`
	Location    *time.Location `json:"-" bson:"-"`
	CreatedTime int64          `json:"-" bson:"ct"`
	UpdatedTime int64          `json:"-" bson:"ut"`
}

const (
	UserDataBSONId                = "_id"
	UserDataBSONUserInfo          = "if"
	UserDataBSONUserProfile       = "pf"
	UserDataBSONMailBox           = "mb"
	UserDataBSONGiftCode          = "gc"
	UserDataBSONAsset1            = "a1"
	UserDataBSONAsset2            = "a2"
	UserDataBSONCurrency          = "cc"
	UserDataBSONStone             = "st"
	UserDataBSONHero              = "hr"
	UserDataBSONDesign            = "de"
	UserDataBSONEquipment         = "eq"
	UserDataBSONTechPart          = "tp"
	UserDataBSONPetPiece          = "pp"
	UserDataBSONPet               = "pe"
	UserDataBSONTalent            = "tl"
	UserDataBSONCampaign          = "ca"
	UserDataBSONBossMode          = "bm"
	UserDataBSONDungeon           = "dg"
	UserDataBSONExploration       = "ex"
	UserDataBSONDailyReward       = "dr"
	UserDataBSONDailyQuest        = "dq"
	UserDataBSONWeeklyQuest       = "wq"
	UserDataBSONAchievement       = "ac"
	UserDataBSONRookieLogin       = "rl"
	UserDataBSONQuest7Days        = "q7"
	UserDataBSONShop              = "sh"
	UserDataBSONDiscount          = "dc"
	UserDataBSONMonthlyCard       = "mc"
	UserDataBSONBattlePass        = "bp"
	UserDataBSONLevelPass1        = "l1"
	UserDataBSONLevelPass2        = "l2"
	UserDataBSONCampaignPass      = "cp"
	UserDataBSONDungeonPass       = "dp"
	UserDataBSONGoldPiggy         = "gp"
	UserDataBSONEventNewYear      = "ny"
	UserDataBSONEventValentine    = "vl"
	UserDataBSONEventAprilFool    = "af"
	UserDataBSONEventChildrenDay  = "cd"
	UserDataBSONEventAutumnLeaves = "al"
	UserDataBSONEventHalloween    = "hl"
	UserDataBSONEventChristmas    = "cr"
	UserDataBSONEventWorldCup     = "wc"
	UserDataBSONTimezone          = "tz"
	UserDataBSONLatestPlay        = "lp"
	UserDataBSONCreatedTime       = "ct"
	UserDataBSONUpdatedTime       = "ut"
)

type UserInfo struct {
	DeviceId     string `json:"di" bson:"di"`
	FacebookId   string `json:"fi" bson:"fi"`
	GoogleId     string `json:"gi" bson:"gi"`
	AppleId      string `json:"ai" bson:"ai"`
	PlayGamesId  string `json:"pi" bson:"pi"`
	GameCenterId string `json:"ci" bson:"ci"`
	Token        string `json:"tk" bson:"tk"`
	TokenTime    int64  `json:"tt" bson:"tt"`
}

const (
	UserInfoBSONDeviceId     = "di"
	UserInfoBSONFacebookId   = "fi"
	UserInfoBSONGoogleId     = "gi"
	UserInfoBSONAppleId      = "ai"
	UserInfoBSONPlayGamesId  = "pi"
	UserInfoBSONGameCenterId = "ci"
	UserInfoBSONToken        = "tk"
	UserInfoBSONTokenTime    = "tt"
)

type UserProfile struct {
	Id       string `json:"i" bson:"i"`
	Block    bool   `json:"b" bson:"b"`
	Name     string `json:"n" bson:"n"`
	Avatar   int64  `json:"a" bson:"a"`
	VipPoint int64  `json:"v" bson:"v"`
	Exp      int64  `json:"e" bson:"e"`
	Level    int64  `json:"l" bson:"l"`
	Country  string `json:"c" bson:"c"`

	Hero      *Hero                `json:"hr" bson:"hr"`
	Pet       map[int64]*Pet       `json:"pe" bson:"pe"`
	Equipment map[int64]*Equipment `json:"eq" bson:"eq"`
	TechPart  map[int64]*TechPart  `json:"tp" bson:"tp"`

	Campaign  int64 `json:"ca" bson:"ca"`
	BossMode  int64 `json:"bm" bson:"bm"`
	Dungeon   int64 `json:"dg" bson:"dg"`
	PvpWin    int64 `json:"pw" bson:"pw"`
	KillEnemy int64 `json:"ke" bson:"ke"`
	KillBoss  int64 `json:"kb" bson:"kb"`
	PlayTime  int64 `json:"pt" bson:"pt"`

	RankGlobal  int64 `json:"rg" bson:"rg"`
	RankCountry int64 `json:"rc" bson:"rc"`
}

type MailBox struct {
	Title   string          `json:"t" bson:"t"`
	Message string          `json:"m" bson:"m"`
	Gift    map[int64]int64 `json:"g" bson:"g"`
	Reward  bool            `json:"r" bson:"r"`
}

type Currency struct {
	Gem         int64 `json:"ge" bson:"ge"`
	Gold        int64 `json:"go" bson:"go"`
	Energy      int64 `json:"ey" bson:"ey"`
	EnergyTime  int64 `json:"et" bson:"et"`
	MaxEnergy   int64 `json:"me" bson:"me"`
	RevivalCoin int64 `json:"rc" bson:"rc"`
}

const (
	RarityNormal int64 = iota + 1
	RarityGood
	RarityBetter
	RarityExcellent
	RarityExcellent1
	RarityEpic
	RarityEpic1
	RarityEpic2
	RarityLegendary
	RarityLegendary1
	RarityLegendary2
	RarityLegendary3
	RarityImmortal
	RarityImmortal1
	RarityImmortal2
	RarityImmortal3
	RarityImmortal4
	RarityImmortal5
)

type Hero struct {
	Index  int64 `json:"x" bson:"x"`
	Rarity int64 `json:"r" bson:"r"`
	Level  int64 `json:"l" bson:"l"`
}

type Pet struct {
	Index  int64 `json:"x" bson:"x"`
	Rarity int64 `json:"r" bson:"r"`
	Level  int64 `json:"l" bson:"l"`
}

const (
	EquipmentTypeWeapon int64 = iota + 1
	EquipmentTypeSuit
	EquipmentTypePendant
	EquipmentTypeBelt
	EquipmentTypeGlove
	EquipmentTypeBoots
)

type Equipment struct {
	Id     int64 `json:"i" bson:"i"`
	Type   int64 `json:"t" bson:"t"`
	Index  int64 `json:"x" bson:"x"`
	Rarity int64 `json:"r" bson:"r"`
	Level  int64 `json:"l" bson:"l"`
}

const (
	TechPartTypeAtk int64 = iota + 1
	TechPartTypeHp
)

type TechPart struct {
	Id     int64 `json:"i" bson:"i"`
	Type   int64 `json:"t" bson:"t"`
	Index  int64 `json:"x" bson:"x"`
	Rarity int64 `json:"r" bson:"r"`
	Level  int64 `json:"l" bson:"l"`
}

type Talent struct {
	Normal  int64 `json:"n" bson:"n"`
	Special int64 `json:"s" bson:"s"`
}

const (
	PlayStep1 int64 = iota + 1
	PlayStep2
	PlayStep3
)

const (
	PlayModeNormal int64 = iota + 1
	PlayModeHard
	PlayModeCrazy
)

type Campaign struct {
	Current     int64           `json:"c" bson:"c"`
	PlayTime    map[int64]int64 `json:"t" bson:"t"`
	RewardStep1 map[int64]int64 `json:"r1" bson:"r1"`
	RewardStep2 map[int64]int64 `json:"r2" bson:"r2"`
	RewardStep3 map[int64]int64 `json:"r3" bson:"r3"`
}

type BossMode struct {
	NormalReward map[int64]int64 `json:"n" bson:"n"`
	HardReward   map[int64]int64 `json:"h" bson:"h"`
	CrazyReward  map[int64]int64 `json:"c" bson:"c"`
}

type Dungeon struct {
	NormalReward map[int64]int64 `json:"n" bson:"n"`
	HardReward   map[int64]int64 `json:"h" bson:"h"`
	CrazyReward  map[int64]int64 `json:"c" bson:"c"`
}

type Exploration struct {
	Time  int64 `json:"t" bson:"t"`
	Quick int64 `json:"q" bson:"q"`
	Ads   int64 `json:"a" bson:"a"`
}

const (
	StatusDoing int64 = iota + 1
	StatusDone
	StatusClaimed
)

type DailyReward struct {
	Day    int64 `json:"d" bson:"d"`
	Reward int64 `json:"r" bson:"r"`
	Bonus  int64 `json:"b" bson:"b"`
}

const (
	DailyQuestIndexPlayCampaign int64 = iota + 1
	DailyQuestIndexUpgradeEquipment
	DailyQuestIndexClaimExploration
	DailyQuestIndexBuyEnergy
	DailyQuestIndexOpenChest
	DailyQuestIndexMakePurchase
)

type DailyQuest struct {
	Counter map[int64]int64 `json:"c" bson:"c"`
	Reward  map[int64]int64 `json:"r" bson:"r"`
	Point   int64           `json:"p" bson:"p"`
	Bonus   map[int64]int64 `json:"b" bson:"b"`
}

const (
	WeeklyQuestIndexClearDungeon int64 = iota + 1
	WeeklyQuestIndexMergeEquipment
	WeeklyQuestIndexMergeTechPart
	WeeklyQuestIndexKillEnemies
	WeeklyQuestIndexBuyDailySale
	WeeklyQuestIndexOpenChest
)

type WeeklyQuest struct {
	Counter map[int64]int64 `json:"c" bson:"c"`
	Reward  map[int64]int64 `json:"r" bson:"r"`
	Point   int64           `json:"p" bson:"p"`
	Bonus   map[int64]int64 `json:"b" bson:"b"`
}

const (
	AchievementIndexLogin int64 = iota + 1
	AchievementIndexReachUserLevel
	AchievementIndexPassCampaign
	AchievementIndexPlayDungeon
	AchievementIndexUnlockTalentNormal
	AchievementIndexUnlockTalentSpecial
	AchievementIndexMakePurchase
	AchievementIndexOpenChest
	AchievementIndexSpendGem
	AchievementIndexSpendGold
	AchievementIndexKillEnemies
	AchievementIndexKillBosses
	AchievementIndexUnlockHero
	AchievementIndexReachHeroRarity
	AchievementIndexReachHeroLevel
	AchievementIndexMergeEquipment
	AchievementIndexReachEquipmentRarity
	AchievementIndexReachEquipmentLevel
	AchievementIndexMergeTechPart
	AchievementIndexReachTechPartRarity
	AchievementIndexReachTechPartLevel
	AchievementIndexUnlockPet
	AchievementIndexReachPetRarity
	AchievementIndexReachPetLevel
)

type Achievement struct {
	Counter map[int64]int64 `json:"c" bson:"c"`
	Target  map[int64]int64 `json:"t" bson:"t"`
	Reward  map[int64]int64 `json:"r" bson:"r"`
}

type RookieLogin struct {
	Day    int64           `json:"d" bson:"d"`
	Reward map[int64]int64 `json:"r" bson:"r"`
}

const (
	Quest7DaysDay1Login int64 = iota + 101
	Quest7DaysDay1ReachLevel
	Quest7DaysDay1TalentNormalLevel
	Quest7DaysDay1TalentSpecialLevel
	Quest7DaysDay1EquipNormalEquipment
	Quest7DaysDay1EquipEquipmentLevel2
	Quest7DaysDay1KillEnemies
	Quest7DaysDay1ObtainGold
	Quest7DaysDay1ObtainGem
	Quest7DaysDay1OpenMythChest
	Quest7DaysDay1PassChapter
)

const (
	Quest7DaysDay2Login int64 = iota + 201
	Quest7DaysDay2ReachLevel
	Quest7DaysDay2TalentNormalLevel
	Quest7DaysDay2TalentSpecialLevel
	Quest7DaysDay2EquipNormalEquipment
	Quest7DaysDay2EquipEquipmentLevel3
	Quest7DaysDay2KillEnemies
	Quest7DaysDay2ObtainGold
	Quest7DaysDay2ObtainGem
	Quest7DaysDay2OpenMythChest
	Quest7DaysDay2PassChapter
)

const (
	Quest7DaysDay3Login int64 = iota + 301
	Quest7DaysDay3ReachLevel
	Quest7DaysDay3TalentNormalLevel
	Quest7DaysDay3TalentSpecialLevel
	Quest7DaysDay3EquipBetterEquipment
	Quest7DaysDay3EquipGoodEquipment
	Quest7DaysDay3EquipEquipmentLevel8
	Quest7DaysDay3EquipEquipmentLevel5
	Quest7DaysDay3KillEnemies
	Quest7DaysDay3ObtainGold
	Quest7DaysDay3ObtainGem
	Quest7DaysDay3OpenMythChest
	Quest7DaysDay3PassChapter
)

const (
	Quest7DaysDay4Login int64 = iota + 401
	Quest7DaysDay4ReachLevel
	Quest7DaysDay4TalentNormalLevel
	Quest7DaysDay4TalentSpecialLevel
	Quest7DaysDay4EquipExcellentEquipment
	Quest7DaysDay4EquipBetterEquipment
	Quest7DaysDay4EquipEquipmentLevel10
	Quest7DaysDay4EquipEquipmentLevel8
	Quest7DaysDay4KillEnemies
	Quest7DaysDay4ObtainGold
	Quest7DaysDay4ObtainGem
	Quest7DaysDay4OpenMythChest
	Quest7DaysDay4PassChapter
)

const (
	Quest7DaysDay5Login int64 = iota + 501
	Quest7DaysDay5ReachLevel
	Quest7DaysDay5TalentNormalLevel
	Quest7DaysDay5TalentSpecialLevel
	Quest7DaysDay5EquipExcellentEquipment
	Quest7DaysDay5EquipEquipmentLevel15
	Quest7DaysDay5EquipEquipmentLevel14
	Quest7DaysDay5EquipEquipmentLevel10
	Quest7DaysDay5KillEnemies
	Quest7DaysDay5ObtainGold
	Quest7DaysDay5ObtainGem
	Quest7DaysDay5OpenMythChest
	Quest7DaysDay5PassChapter
)

const (
	Quest7DaysDay6Login int64 = iota + 601
	Quest7DaysDay6ReachLevel
	Quest7DaysDay6TalentNormalLevel
	Quest7DaysDay6TalentSpecialLevel
	Quest7DaysDay6EquipEpicEquipment
	Quest7DaysDay6EquipEquipmentLevel20
	Quest7DaysDay6EquipEquipmentLevel18
	Quest7DaysDay6KillEnemies
	Quest7DaysDay6ObtainGold1
	Quest7DaysDay6ObtainGold2
	Quest7DaysDay6ObtainGem1
	Quest7DaysDay6ObtainGem2
	Quest7DaysDay6OpenMythChest
	Quest7DaysDay6PassChapter
)

const (
	Quest7DaysDay7Login int64 = iota + 701
	Quest7DaysDay7ReachLevel
	Quest7DaysDay7TalentNormalLevel
	Quest7DaysDay7TalentSpecialLevel
	Quest7DaysDay7EquipEquipmentLevel25
	Quest7DaysDay7EquipEquipmentLevel20
	Quest7DaysDay7KillEnemies1
	Quest7DaysDay7KillEnemies2
	Quest7DaysDay7ObtainGold1
	Quest7DaysDay7ObtainGold2
	Quest7DaysDay7ObtainGem1
	Quest7DaysDay7ObtainGem2
	Quest7DaysDay7ObtainGem3
	Quest7DaysDay7OpenMythChest1
	Quest7DaysDay7OpenMythChest2
	Quest7DaysDay7OpenMythChest3
	Quest7DaysDay7PassChapter
)

type Quest7Days struct {
	Day      int64           `json:"d" bson:"d"`
	Counters map[int64]int64 `json:"c" bson:"c"`
	Rewards  map[int64]int64 `json:"r" bson:"r"`
	Point    int64           `json:"p" bson:"p"`
	Bonus    map[int64]int64 `json:"b" bson:"b"`
}

type Shop struct {
	MonthlyInApp   int64          `json:"mi" bson:"mi"`
	ChapterPacks   map[int64]bool `json:"ct" bson:"ct"`
	DailySales     map[int64]bool `json:"dl" bson:"dl"`
	AdsRareChest   int64          `json:"ar" bson:"ar"`
	KeyRareChest   int64          `json:"kr" bson:"kr"`
	AdsMythChest   int64          `json:"am" bson:"am"`
	KeyMythChest   int64          `json:"km" bson:"km"`
	CountMythChest int64          `json:"cm" bson:"cm"`
	GemPacks       map[int64]bool `json:"ge" bson:"ge"`
	GoldPacks      map[int64]bool `json:"go" bson:"go"`
	EnergyPacks    map[int64]bool `json:"ey" bson:"ey"`
}

type Discount struct {
	DailyCount    int64           `json:"dc" bson:"dc"`
	DailyReward   map[int64]bool  `json:"dr" bson:"dr"`
	DailyBonus    map[int64]int64 `json:"db" bson:"db"`
	WeeklyCount   int64           `json:"wc" bson:"wc"`
	WeeklyReward  map[int64]bool  `json:"wr" bson:"wr"`
	WeeklyBonus   map[int64]int64 `json:"wb" bson:"wb"`
	MonthlyCount  int64           `json:"mc" bson:"mc"`
	MonthlyReward map[int64]bool  `json:"mr" bson:"mr"`
	MonthlyBonus  map[int64]int64 `json:"mb" bson:"mb"`
}

type MonthlyCard struct {
	TimeNormal   int64 `json:"tn" bson:"tn"`
	RewardNormal int64 `json:"rn" bson:"rn"`
	TimeSuper    int64 `json:"ts" bson:"ts"`
	RewardSuper  int64 `json:"rs" bson:"rs"`
}

type BattlePass struct {
	Point        int64           `json:"p" bson:"p"`
	FreeReward   map[int64]int64 `json:"fr" bson:"fr"`
	NormalActive bool            `json:"na" bson:"na"`
	NormalReward map[int64]int64 `json:"nr" bson:"nr"`
	SuperActive  bool            `json:"sa" bson:"sa"`
	SuperReward  map[int64]int64 `json:"sr" bson:"sr"`
}

type LevelPass struct {
	FreeReward   map[int64]int64 `json:"fr" bson:"fr"`
	NormalActive bool            `json:"na" bson:"na"`
	NormalReward map[int64]int64 `json:"nr" bson:"nr"`
	SuperActive  bool            `json:"sa" bson:"sa"`
	SuperReward  map[int64]int64 `json:"sr" bson:"sr"`
}

type CampaignPass struct {
	FreeReward   map[int64]int64 `json:"fr" bson:"fr"`
	NormalActive bool            `json:"na" bson:"na"`
	NormalReward map[int64]int64 `json:"nr" bson:"nr"`
	SuperActive  bool            `json:"sa" bson:"sa"`
	SuperReward  map[int64]int64 `json:"sr" bson:"sr"`
}

type DungeonPass struct {
	NormalReward map[int64]int64 `json:"nr" bson:"nr"`
	HardActive   bool            `json:"ha" bson:"ha"`
	HardReward   map[int64]int64 `json:"hr" bson:"hr"`
	CrazyActive  bool            `json:"ca" bson:"ca"`
	CrazyReward  map[int64]int64 `json:"cr" bson:"cr"`
}

const (
	GoldPiggySizeS int64 = iota + 1
	GoldPiggySizeM
	GoldPiggySizeL
	GoldPiggySizeXL
	GoldPiggySizeXXL
)

type GoldPiggy struct {
	Counter int64 `json:"c" bson:"c"`
	Size    int64 `json:"s" bson:"s"`
}

const (
	EventNewYearQuestLogin int64 = iota + 1
	EventNewYearQuestPlayCampaign
	EventNewYearQuestPlayBossMode
	EventNewYearQuestPlayDungeon
	EventNewYearQuestQuickPatrol
	EventNewYearQuestOpenChest
	EventNewYearQuestKillEnemies
)

type EventNewYear struct {
}

type EventValentine struct {
}

type EventAprilFool struct {
}

type EventChildrenDay struct {
}

type EventAutumnLeaves struct {
}

type EventHalloween struct {
}

type EventChristmas struct {
}

type EventWorldCup struct {
}
