package repository

import (
	"Survival/src/models"
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"

	"Survival/lib/log"
)

var mongoCli *mongo.Client
var database *mongo.Database

func InitMongoRepo(uri string) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if cli, err := mongo.Connect(ctx, options.Client().ApplyURI(uri)); err != nil {
		log.ZapLog.Panic(err)
	} else if err = cli.Ping(ctx, readpref.Primary()); err != nil {
		log.ZapLog.Panic(err)
	} else {
		mongoCli = cli
	}
	log.ZapLog.Infof("Successfully connected and pinged [%s]", uri)
	// database
	database = mongoCli.Database("SurvivalDB")
	serverInfoCol = database.Collection("ServerInfo")
	userConfigCol = database.Collection("UserConfig")
	userDataCol = database.Collection("UserData")
	leaderBoardCol = database.Collection("LeaderBoard")
	// index
	idxOptUnix := &options.IndexOptions{}
	idxOptUnix.SetUnique(true)
	//_, _ = userInfoCol.Indexes().CreateMany(ctx, []mongo.IndexModel{
	//	mongo.IndexModel{Keys: bson.D{{models.UEInfoUsername, 1}}, Options: idxOptUnix}})
	//userInfoCol = mongoCli.Database("UEAsset").Collection("UECurrency")
}

var serverInfoCol *mongo.Collection

func UpsertServerInfo(ctx context.Context, obj *models.ServerInfo) (bool, error) {
	replacement, err := bson.Marshal(obj)
	if err != nil {
		return false, err
	}
	filter := bson.D{{models.ServerBSONId, obj.Id}}
	opts := &options.ReplaceOptions{}
	opts.SetUpsert(true)
	result, err := serverInfoCol.ReplaceOne(ctx, filter, replacement, opts)
	if err != nil {
		return false, err
	}
	return result.UpsertedCount > 0, nil
}

var userConfigCol *mongo.Collection

func UpsertUserConfig(ctx context.Context, obj *models.UserConfig) (bool, error) {
	replacement, err := bson.Marshal(obj)
	if err != nil {
		return false, err
	}
	filter := bson.D{{models.UserConfigBSONId, obj.Id}}
	opts := &options.ReplaceOptions{}
	opts.SetUpsert(true)
	result, err := userConfigCol.ReplaceOne(ctx, filter, replacement, opts)
	if err != nil {
		return false, err
	}
	return result.UpsertedCount > 0, nil
}

var userDataCol *mongo.Collection

func GetUserDataByDeviceId(ctx context.Context, di string) (*models.UserData, error) {
	filter := bson.D{{fmt.Sprintf("%s.%s", models.UserDataBSONUserInfo, models.UserInfoBSONDeviceId), di}}
	raw, err := userDataCol.FindOne(ctx, filter).DecodeBytes()
	if err != nil {
		return nil, err
	}
	var obj models.UserData
	if err = bson.Unmarshal(raw, &obj); err != nil {
		return nil, err
	}
	if obj.ResetDataNewDay() {
		if _, err = UpsertUserData(ctx, &obj); err != nil {
			return nil, err
		}
	}
	return &obj, nil
}

func GetUserDataByFacebookId(ctx context.Context, fi string) (*models.UserData, error) {
	filter := bson.D{{fmt.Sprintf("%s.%s", models.UserDataBSONUserInfo, models.UserInfoBSONFacebookId), fi}}
	raw, err := userDataCol.FindOne(ctx, filter).DecodeBytes()
	if err != nil {
		return nil, err
	}
	var obj models.UserData
	if err = bson.Unmarshal(raw, &obj); err != nil {
		return nil, err
	}
	if obj.ResetDataNewDay() {
		if _, err = UpsertUserData(ctx, &obj); err != nil {
			return nil, err
		}
	}
	return &obj, nil
}

func GetUserDataByGoogleId(ctx context.Context, gi string) (*models.UserData, error) {
	filter := bson.D{{fmt.Sprintf("%s.%s", models.UserDataBSONUserInfo, models.UserInfoBSONGoogleId), gi}}
	raw, err := userDataCol.FindOne(ctx, filter).DecodeBytes()
	if err != nil {
		return nil, err
	}
	var obj models.UserData
	if err = bson.Unmarshal(raw, &obj); err != nil {
		return nil, err
	}
	if obj.ResetDataNewDay() {
		if _, err = UpsertUserData(ctx, &obj); err != nil {
			return nil, err
		}
	}
	return &obj, nil
}

func GetUserDataByAppleId(ctx context.Context, ai string) (*models.UserData, error) {
	filter := bson.D{{fmt.Sprintf("%s.%s", models.UserDataBSONUserInfo, models.UserInfoBSONAppleId), ai}}
	raw, err := userDataCol.FindOne(ctx, filter).DecodeBytes()
	if err != nil {
		return nil, err
	}
	var obj models.UserData
	if err = bson.Unmarshal(raw, &obj); err != nil {
		return nil, err
	}
	if obj.ResetDataNewDay() {
		if _, err = UpsertUserData(ctx, &obj); err != nil {
			return nil, err
		}
	}
	return &obj, nil
}

func GetUserDataByPlayGamesId(ctx context.Context, pgi string) (*models.UserData, error) {
	filter := bson.D{{fmt.Sprintf("%s.%s", models.UserDataBSONUserInfo, models.UserInfoBSONPlayGamesId), pgi}}
	raw, err := userDataCol.FindOne(ctx, filter).DecodeBytes()
	if err != nil {
		return nil, err
	}
	var obj models.UserData
	if err = bson.Unmarshal(raw, &obj); err != nil {
		return nil, err
	}
	if obj.ResetDataNewDay() {
		if _, err = UpsertUserData(ctx, &obj); err != nil {
			return nil, err
		}
	}
	return &obj, nil
}

func GetUserDataByGameCenterId(ctx context.Context, gci string) (*models.UserData, error) {
	filter := bson.D{{fmt.Sprintf("%s.%s", models.UserDataBSONUserInfo, models.UserInfoBSONGameCenterId), gci}}
	raw, err := userDataCol.FindOne(ctx, filter).DecodeBytes()
	if err != nil {
		return nil, err
	}
	var obj models.UserData
	if err = bson.Unmarshal(raw, &obj); err != nil {
		return nil, err
	}
	if obj.ResetDataNewDay() {
		if _, err = UpsertUserData(ctx, &obj); err != nil {
			return nil, err
		}
	}
	return &obj, nil
}

func GetUserData(ctx context.Context, id string) (*models.UserData, error) {
	filter := bson.D{{models.UserDataBSONId, id}}
	raw, err := userDataCol.FindOne(ctx, filter).DecodeBytes()
	if err != nil {
		return nil, err
	}
	var obj models.UserData
	if err = bson.Unmarshal(raw, &obj); err != nil {
		return nil, err
	}
	if obj.ResetDataNewDay() {
		if _, err = UpsertUserData(ctx, &obj); err != nil {
			return nil, err
		}
	}
	return &obj, nil
}

func UpsertUserData(ctx context.Context, obj *models.UserData) (bool, error) {
	obj.UpdatedTime = time.Now().UnixMilli()
	if obj.CreatedTime == 0 {
		obj.CreatedTime = obj.UpdatedTime
	}
	replacement, err := bson.Marshal(obj)
	if err != nil {
		return false, err
	}
	filter := bson.D{{models.UserDataBSONId, obj.Id}}
	opts := &options.ReplaceOptions{}
	opts.SetUpsert(true)
	result, err := userDataCol.ReplaceOne(ctx, filter, replacement, opts)
	if err != nil {
		return false, err
	}
	return result.UpsertedCount > 0, nil
}

func DeleteUserData(ctx context.Context, id string) (bool, error) {
	filter := bson.D{{models.UserDataBSONId, id}}
	result, err := userDataCol.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}
	return result.DeletedCount > 0, nil
}

func CalculateGlobalRanking(ctx context.Context) ([]*models.UserProfile, error) {
	filter := bson.D{}
	opts := &options.FindOptions{}
	opts.SetSort(bson.D{{"pf.ca", -1}, {"pf.ke", -1}, {"pf.pt", -1}})
	opts.SetLimit(100)
	cur, err := userDataCol.Find(ctx, filter, opts)
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)
	var ues []*models.UserProfile
	for cur.Next(ctx) {
		var userData models.UserData
		if err = bson.Unmarshal(cur.Current, &userData); err != nil {
			log.ZapLog.Error(err)
			continue
		}
		ues = append(ues, userData.Profile)
	}
	if _, err = UpsertRanking(ctx, &models.Ranking{
		Id:   "MAIN_RANK_GLOBAL",
		Pfs:  ues,
		Time: time.Now().UnixMilli(),
	}); err != nil {
		log.ZapLog.Error(err)
	}
	return ues, nil
}

func CalculateCountryRanking(ctx context.Context, country string) ([]*models.UserProfile, error) {
	filter := bson.D{{"pf.c", country}}
	opts := &options.FindOptions{}
	opts.SetSort(bson.D{{"pf.ca", -1}, {"pf.ke", -1}, {"pf.pt", -1}})
	opts.SetLimit(100)
	cur, err := userDataCol.Find(ctx, filter, opts)
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)
	var ues []*models.UserProfile
	for cur.Next(ctx) {
		var userData models.UserData
		if err = bson.Unmarshal(cur.Current, &userData); err != nil {
			log.ZapLog.Error(err)
			continue
		}
		ues = append(ues, userData.Profile)
	}
	if _, err = UpsertRanking(ctx, &models.Ranking{
		Id:   "MAIN_RANK_" + country,
		Pfs:  ues,
		Time: time.Now().UnixMilli(),
	}); err != nil {
		log.ZapLog.Error(err)
	}
	return ues, nil
}

func CountUserRankGlobal(ctx context.Context, ca, ke, pt int64) (int64, error) {
	filter := bson.D{{"$or", bson.A{
		bson.D{{"pf.ca", bson.D{{"$gt", ca}}}},
		bson.D{{"$and", bson.A{
			bson.D{{"pf.ca", bson.D{{"$eq", ca}}}},
			bson.D{{"$or", bson.A{
				bson.D{{"pf.ke", bson.D{{"$gt", ke}}}},
				bson.D{{"$and", bson.A{
					bson.D{{"pf.ke", bson.D{{"$eq", ke}}}},
					bson.D{{"pf.pt", bson.D{{"$gt", pt}}}}}}}}}}}}}}}}
	return userDataCol.CountDocuments(ctx, filter)
}

func CountUserRankCountry(ctx context.Context, cc string, ca, ke, pt int64) (int64, error) {
	filter := bson.D{{"$and", bson.A{
		bson.D{{"pt.c", bson.D{{"$eq", cc}}}},
		bson.D{{"$or", bson.A{
			bson.D{{"pf.ca", bson.D{{"$gt", ca}}}},
			bson.D{{"$and", bson.A{
				bson.D{{"pf.ca", bson.D{{"$eq", ca}}}},
				bson.D{{"$or", bson.A{
					bson.D{{"pf.ke", bson.D{{"$gt", ke}}}},
					bson.D{{"$and", bson.A{
						bson.D{{"pf.ke", bson.D{{"$eq", ke}}}},
						bson.D{{"pf.pt", bson.D{{"$gt", pt}}}}}}}}}}}}}}}}}}}
	return userDataCol.CountDocuments(ctx, filter)
}

func UpdateUserRanking(ctx context.Context, obj *models.UserData) (bool, error) {
	filter := bson.D{{models.UserDataBSONId, obj.Id}}
	update := bson.D{{"$set", bson.D{
		{"pf.rg", obj.Profile.RankGlobal},
		{"pf.rc", obj.Profile.RankCountry},
	}}}
	result, err := userDataCol.UpdateOne(ctx, filter, update)
	if err != nil {
		return false, err
	}
	return result.UpsertedCount > 0, nil
}

var leaderBoardCol *mongo.Collection

func GetGlobalRanking(ctx context.Context) (*models.Ranking, error) {
	filter := bson.D{{"_id", "MAIN_RANK_GLOBAL"}}
	raw, err := leaderBoardCol.FindOne(ctx, filter).DecodeBytes()
	if err != nil {
		return nil, err
	}
	var obj models.Ranking
	if err = bson.Unmarshal(raw, &obj); err != nil {
		return nil, err
	}
	return &obj, nil
}

func GetCountryRanking(ctx context.Context, cc string) (*models.Ranking, error) {
	filter := bson.D{{"_id", "MAIN_RANK_" + cc}}
	raw, err := leaderBoardCol.FindOne(ctx, filter).DecodeBytes()
	if err != nil {
		return nil, err
	}
	var obj models.Ranking
	if err = bson.Unmarshal(raw, &obj); err != nil {
		return nil, err
	}
	return &obj, nil
}

func UpsertRanking(ctx context.Context, obj *models.Ranking) (bool, error) {
	filter := bson.D{{"_id", obj.Id}}
	replacement, err := bson.Marshal(obj)
	if err != nil {
		return false, err
	}
	opts := &options.ReplaceOptions{}
	opts.SetUpsert(true)
	result, err := leaderBoardCol.ReplaceOne(ctx, filter, replacement, opts)
	if err != nil {
		return false, err
	}
	return result.UpsertedCount > 0, nil
}
