docker build -f Dockerfile -t survival:v1.0.0 .
docker stop survival
docker rm survival
docker run -it -p 443:443 --name survival --restart always -d \
  --env SERVER_ID=AWS.ServerTest \
  --env VERSION=1.0.0 \
  --env EXTERNAL_IPADDR=18.143.119.34 \
  --env PRIVATE_IPADDR=172.26.2.221 \
  --env EXTERNAL_PORT=443 \
  --env MONGODB_URI=mongodb://172.26.2.221:30017/ \
  --env ELASTIC_URI=http://172.26.2.221:9200/ \
  --env LOG_LEVEL=debug \
  survival:v1.0.0