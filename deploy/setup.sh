# install dependency
yum update -y
yum install -y yum-utils xorg-x11-xauth git
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
systemctl start docker
systemctl enable docker
systemctl status docker
# run mongo single
docker run -it -d -p 27017:27017 -v /home/mongo/data:/data/db --name mongo --restart always mongo
# run mongo replica set
docker run -it -d -p 27017:27017 -v /home/mongo/data:/data/db --name mongo --restart always mongo --replSet survival
docker exec -it mongo mongosh --eval "rs.initiate({
 _id: \"survival\",
 members: [
  {_id: 0, host: \"ip-vm1:27017\"},
  {_id: 1, host: \"ip-vm2:27017\"},
  {_id: 2, host: \"ip-vm3:27017\"}
 ]
})"
docker exec -it mongo mongosh --eval "rs.status()"