# 1 node

docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.17.8

# 3 node



docker run -p 9201:9200 \
 --name "es01" \
 -e "node.name=es01" \
 -e "cluster.name=esSurvival" \
 -e "discovery.seed_hosts=192.168.146.137:9202,192.168.146.137:9203" \
 -e "cluster.initial_master_nodes=es01,es02,es03" \
 -e "bootstrap.memory_lock=true" \
 -v /home/es01/data:/usr/share/elasticsearch/data \
 docker.elastic.co/elasticsearch/elasticsearch:7.17.8

docker run -p 9202:9200 \
 --name "es02" \
 -e "node.name=es02" \
 -e "cluster.name=esSurvival" \
 -e "discovery.seed_hosts=192.168.146.137:9201,192.168.146.137:9203" \
 -e "cluster.initial_master_nodes=es01,es02,es03" \
 -e "bootstrap.memory_lock=true" \
 -v /home/es02/data:/usr/share/elasticsearch/data \
 docker.elastic.co/elasticsearch/elasticsearch:7.17.8

docker run -p 9203:9200 \
 --name "es03" \
 -e "node.name=es03" \
 -e "cluster.name=esSurvival" \
 -e "discovery.seed_hosts=192.168.146.137:9201,192.168.146.137:9203" \
 -e "cluster.initial_master_nodes=es01,es02,es03" \
 -e "bootstrap.memory_lock=true" \
 -v /home/es03/data:/usr/share/elasticsearch/data \
 docker.elastic.co/elasticsearch/elasticsearch:7.17.8

