mkdir /home/mongo01/
mkdir /home/mongo02/
mkdir /home/mongo03/
mkdir /home/mongo01/data
mkdir /home/mongo02/data
mkdir /home/mongo03/data

docker stop mongo01 mongo02 mongo03
docker rm mongo01 mongo02 mongo03

docker run -it -d -p 27011:27017 -v /home/mongo01/data:/data/db --name mongo01 --restart always mongo --replSet survival
docker run -it -d -p 27012:27017 -v /home/mongo02/data:/data/db --name mongo02 --restart always mongo --replSet survival
docker run -it -d -p 27013:27017 -v /home/mongo03/data:/data/db --name mongo03 --restart always mongo --replSet survival

docker exec -it mongo01 mongosh --eval "rs.initiate({
 _id: \"survival\",
 members: [
  {_id: 0, host: \"172.26.0.91:27011\"},
  {_id: 1, host: \"172.26.0.91:27013\"},
  {_id: 2, host: \"172.26.0.91:27012\"},
  {_id: 3, host: \"172.26.7.106:27011\"},
  {_id: 4, host: \"172.26.7.106:27012\"},
  {_id: 5, host: \"172.26.7.106:27013\"}
 ]
})"

docker exec -it mongo01 mongosh --eval "rs.status()"

docker exec -it mongo mongosh
