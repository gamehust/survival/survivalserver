FROM golang:1.19.3
RUN mkdir /survival
COPY . /survival
WORKDIR /survival
RUN go build --mod=vendor -o /survival/bin ./src/main.go
ENV TLS_CERT_PATH=/survival/lib/tls/cert.pem
ENV TLS_KEY_PATH=/survival/lib/tls/key.pem
CMD ["/survival/bin"]