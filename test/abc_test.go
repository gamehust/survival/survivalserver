package test

import (
	"Survival/lib/log"
	"Survival/src/models"
	"Survival/src/repository"
	"context"
	"encoding/json"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"math/rand"
	"strconv"
	"strings"
	"testing"
	"time"
)

type AAA struct {
	Array     []int64
	MapString map[string]string
	MapInt64  map[int64]int64
}

func TestJSON(t *testing.T) {
	t.Logf("%d", time.Now().UnixMilli())
	t.Logf("%d", time.Now().UnixMicro())
	t.Logf("%d", time.Now().UnixNano())
	for i := 0; i < 10; i++ {
		t.Logf("%d", time.Now().UnixNano())
	}

	aaa := AAA{
		Array:     []int64{1, 2, 3},
		MapString: map[string]string{"1": "1", "2": "2", "3": "3"},
		MapInt64:  map[int64]int64{1: 1, 2: 2, 3: 3},
	}
	abin, _ := json.Marshal(&aaa)
	t.Logf("%s", abin)

	var bbb interface{}
	_ = json.Unmarshal(abin, &bbb)
	t.Logf("%#v", (bbb.(map[string]interface{})["Array"]).([]interface{})[0])
}

func TestABCDDD(t *testing.T) {
	ss := map[int]string{
		1: "\"1, 1, 2, 2, 2, 3, 3, 3, 4, 4, \n4, 5, 5, 5, 6, 6, 6, 6, 7, 7, \n7, 7, 8, 8, 8, 8, 9, 9, 9, 9, \n10, 10, 10, 12, 12, 12, 14, 14, 14, 16, \n16, 16, 18, 18, 18, 20, 20, 20, 22, 22\"\t",
		2: "\"1, 1, 2, 2, 3, 3, 4, 4, 5, 5, \n6, 6, 7, 7, 8, 8, 9, 9, 10, 10, \n11, 11, 12, 12, 13, 13, 14, 14, 15, 15, \n17, 17, 19, 19, 21, 21, 23, 23, 25, 25, \n27, 27, 29, 29, 31, 31, 33, 33, 35, 35\"\t",
		3: "\"2, 2, 3, 3, 4, 4, 5, 5, 6, 6,\n8, 8, 10, 10, 12, 12, 14, 14, 16, 16,\n18, 18, 20, 20, 22, 22, 24, 24, 26, 26,\n28, 28, 30, 30, 32, 32, 34, 34, 36, 36,\n38, 38, 40, 40, 42, 42, 44, 44, 46, 46\"\t",
		4: "\"3, 3, 4, 4, 5, 5, 6, 6, 7, 7,\n8, 8, 10, 10, 12, 12, 14, 14, 16, 16,\n18, 18, 20, 20, 22, 22, 24, 24, 26, 26,\n29, 29, 31, 31, 34, 34, 37, 37, 40, 40,\n43, 43, 46, 46, 49, 49, 52, 52, 55, 55\"\t",
		5: "\"4, 4, 5, 5, 6, 6, 7, 7, 8, 8,\n9, 9, 11, 11, 13, 13, 15, 15, 17, 17,\n20, 20, 23, 23, 26, 26, 29, 29, 32, 32,\n35, 35, 38, 38, 41, 41, 44, 44, 47, 47,\n50, 50, 53, 53, 56, 56, 59, 59, 62, 62\"\t",
		6: "\"5, 5, 7, 7, 9, 9, 11, 11, 13, 13,\n15, 15, 17, 17, 19, 19, 21, 21, 23, 23,\n26, 26, 29, 29, 32, 32, 35, 35, 38, 38,\n41, 41, 45, 45, 49, 49, 53, 53, 57, 57,\n61, 61, 65, 65, 69, 69, 73, 73, 77, 77\"\t",
		7: "\"6, 6, 8, 8, 10, 10, 12, 12, 14, 14,\n17, 17, 20, 20, 23, 23, 26, 26, 29, 29,\n32, 32, 36, 36, 40, 40, 44, 44, 48, 48,\n52, 52, 56, 56, 60, 60, 64, 64, 68, 68,\n72, 72, 76, 76, 80, 80, 84, 84, 88, 88\"\t",
		8: "\"7, 7, 9, 9, 11, 11, 13, 13, 15, 15,\n19, 19, 23, 23, 27, 27, 31, 31, 35, 35,\n39, 39, 43, 43, 47, 47, 51, 51, 55, 55,\n59, 59, 63, 63, 67, 67, 71, 71, 75, 75,\n79, 79, 83, 83, 87, 87, 91, 91, 95, 95\"\t"}
	for i, s := range ss {
		fmt.Printf("SHIP %d:", i)
		sum := 0
		for _, g := range strings.Fields(s) {
			gi, _ := strconv.Atoi(strings.Split(g, ",")[0])
			sum += gi
		}
		fmt.Printf("%d\n", sum)
	}
}

func TestSmoothIncreaseNumber(t *testing.T) {
	a := 100000
	b := a - 100000
	for i := 0; i < 100; i++ {
		a = SmoothIncreaseNumber(a, b)
		t.Logf("%d - %d", a, b)
	}
}

func SmoothIncreaseNumber(from, to int) int {
	if from-to > 1000000 {
		return from - 1000000
	}
	if from-to > 100000 {
		return from - 100000
	}
	if from-to > 10000 {
		return from - 10000
	}
	if from-to > 1000 {
		return from - 1000
	}
	if from-to > 100 {
		return from - 100
	}
	if from-to > 10 {
		return from - 10
	}
	if from-to > 0 {
		return from - 1
	}
	if from-to < -1000000 {
		return from + 1000000
	}
	if from-to < -100000 {
		return from + 100000
	}
	if from-to < -10000 {
		return from + 10000
	}
	if from-to < -1000 {
		return from + 1000
	}
	if from-to < -100 {
		return from + 100
	}
	if from-to < -10 {
		return from + 10
	}
	if from-to < 0 {
		return from + 1
	}
	return to
}

func TestCreateRankingData(t *testing.T) {
	models.InitENVConfig()
	models.InitUserConfig()
	log.InitZapLog(models.ENVConfig.LogLevel)
	repository.InitMongoRepo("mongodb://192.168.146.131:27017/")
	for i := 0; i < 10000; i++ {
		var ue models.UserData
		ue.GetDefault()
		ue.Profile.Name = ue.Id
		ue.Profile.Campaign = rand.Int63n(100)
		ue.Profile.KillEnemy = rand.Int63n(100000)
		ue.Profile.PlayTime = rand.Int63n(100000000) + 1670313720000
		if _, err := repository.UpsertUserData(context.TODO(), &ue); err != nil {
			t.Error(err)
		}
	}
}

func TestCalculateRanking(t *testing.T) {
	cli, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://192.168.146.131:27017/"))
	if err != nil {
		log.ZapLog.Panic(err)
	} else if err = cli.Ping(context.TODO(), readpref.Primary()); err != nil {
		log.ZapLog.Panic(err)
	}
	opts := &options.FindOptions{}
	opts.SetSort(bson.D{{"pf.ca", -1}, {"pf.ke", -1}})
	cursor, err := cli.Database("SurvivalDB").Collection("UserData").Find(context.TODO(), bson.D{}, opts)
	if err != nil {
		panic(err)
	}
	var results []bson.D
	if err = cursor.All(context.TODO(), &results); err != nil {
		panic(err)
	}
	for i, result := range results {
		fmt.Println(i, result.Map()["pf"])
	}
}

func TestABCBB(t *testing.T) {
	str := "public CFCurrency cc;\n        public CFHero hr;\n        public CFPet pe;\n        public CFEquipment eq;\n        public CFTechPart tp;\n        public CFTalent tl;\n\n        // play\n        public CFCampaign ca;\n        public CFBossMode bm;\n        public CFDungeon dg;\n        public CFExploration ex;\n\n        // quest\n        public CFDailyReward dr;\n        public CFDailyQuest dq;\n        public CFWeeklyQuest wq;\n        public CFAchievement ac;\n        public CFRookieLogin rl;\n        public CFQuest7Days q7;\n\n        // shop\n        public CFShop sh;\n        public CFDiscount dc;\n\n        // event - always on\n        public CFMonthlyCard mc;\n        public CFBattlePass bp;\n        public CFLevelPass1 l1;\n        public CFLevelPass2 l2;\n        public CFCampaignPass cp;\n        public CFDungeonPass dp;\n        public CFGoldPiggy gp;\n\n        // event - on off\n        public CFEventNewYear ny;\n        public CFEventValentine vl;\n        public CFEventAprilFool af;\n        public CFEventChildrenDay cd;\n        public CFEventAutumnLeaves al;\n        public CFEventHalloween hl;\n        public CFEventChristmas cr;\n        public CFEventWorldCup wc;"

	oldCf, oldCfc := "", ""
	for _, s := range strings.Split(str, "\n") {
		if strings.Contains(s, "public") {
			if oldCf == "" {
				oldCf = strings.Fields(s)[1]
				oldCfc = strings.Split(strings.Fields(s)[2], ";")[0]
				continue
			}
			sss := strings.Fields(s)[1]
			ss2 := strings.Split(strings.Fields(s)[2], ";")[0]

			fmt.Printf("\n            case LoadConfigStepSendGet%s:\n                if (DataManager.UserConfig.vs[UserConfig.%sVS] != _configVersion[UserConfig.%sVS])\n                {\n                    StartCoroutine(ConsumerConfig.SendGet%s());\n                    _loadConfigStep = LoadConfigStepWaitGet%s;\n                }\n                else\n                    _loadConfigStep = LoadConfigStepSendGet%s;\n                break;\n            case LoadConfigStepWaitGet%s:\n                if (Consumer.GetResponseStatusCode() == Consumer.ResponseStatusOk)\n                {\n                    DataManager.UserConfig.vs[UserConfig.%sVS] = _configVersion[UserConfig.%sVS];\n                    DataManager.UserConfig.%s = ConsumerConfig.GetUserConfig().%s;\n                    _loadConfigStep = LoadConfigStepSendGet%s;\n                }\n                else if (Consumer.GetResponseStatusCode() != Consumer.ResponseStatusSending)\n                {\n                    Debug.Log(\"ReloadConfig: ERROR \" + Consumer.GetResponseStatusCode());\n                    popupServerError.SetActive(true);\n                    _loginStep = LoginStepError;\n                }\n                break;", oldCf, oldCf, oldCf, oldCf, oldCf, sss, oldCf, oldCf, oldCf, oldCfc, oldCfc, sss)

			oldCf = sss
			oldCfc = ss2
		}
	}
}
