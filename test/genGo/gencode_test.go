package genGo

import (
	"fmt"
	"strings"
	"testing"
)

func TestGenConfigCurrency(t *testing.T) {
	epl := "500\n\n\n1500\n\n\n2500\n\n\n3750\n\n\n5000\n\n\n7500\n\n\n10000\n\n\n12500\n\n\n15000\n\n\n20000\n\n\n50000\n\n\n55000\n\n\n60000\n\n\n80000\n\n\n80000\n\n\n100000\n\n\n100000\n\n\n100000\n\n\n100000\n\n\n100000\n\n\n150000\n\n\n150000\n\n\n150000\n\n\n150000\n\n\n150000\n\n\n150000\n\n\n175000\n\n\n200000\n\n\n200000\n\n\n200000\n\n\n200000\n\n\n200000\n\n\n200000\n\n\n300000\n\n\n300000\n\n\n300000\n\n\n300000\n\n\n300000\n\n\n300000\n\n\n300000\n\n\n300000\n\n\n300000\n\n\n300000\n\n\n400000\n\n\n400000\n\n\n400000\n\n\n400000\n\n\n400000\n\n\n400000\n\n\n600000\n\n\n600000\n\n\n600000\n\n\n600000\n\n\n750000\n\n\n750000\n\n\n750000\n\n\n750000\n\n\n750000\n\n\n750000\n\n\n750000\n\n\n750000\n\n\n750000\n\n\n750000\n\n\n750000\n\n\n750000\n\n\n750000\n\n\n850000\n\n\n900000\n\n\n900000\n\n\n900000\n\n\n900000\n\n\n900000\n\n\n900000\n\n\n900000\n\n\n900000\n\n\n900000\n\n\n900000\n\n\n900000\n\n\n900000\n\n\n1000000\n\n\n1200000\n\n\n1200000\n\n\n1200000\n\n\n1200000\n\n\n1200000\n\n\n1200000\n\n\n1200000\n\n\n1200000\n\n\n1200000\n\n\n1200000\n\n\n1200000\n\n\n1200000\n\n\n1200000\n\n\n1200000\n\n\n1200000\n\n\n1200000\n\n\n1200000\n\n\n1200000\n\n\n1200000\n\n\n1300000\n\n"
	for i, s := range strings.Fields(epl) {
		fmt.Printf("%d:%s,", i+1, s)
	}
}

func TestGenConfigHero(t *testing.T) {
	fmt.Printf("GemUnlock:\n")
	GemUnlock := "0\n0\n0\n5000\n5000\n5000\n7000\n7000\n7000\n10000\n10000\n10000\n20000\n20000\n20000\n30000\n30000\n30000\n50000\n50000\n50000\n100000"
	for i, s := range strings.Fields(GemUnlock) {
		fmt.Printf("%d:%s,", i+1, s)
	}
	fmt.Printf("\nTierUnlockLevel\n")
	TierUnlockLevel := "1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n2\n2\n2\n2\n2\n2\n2\n2\n2\n2\n3\n3\n3\n3\n3\n3\n3\n3\n3\n3\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n5\n5\n5\n5\n5\n5\n5\n5\n5\n5\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n8\n8\n8\n8\n8\n8\n8\n8\n8\n8\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n11\n11\n11\n11\n11\n11\n11\n11\n11\n11\n12\n12\n12\n12\n12\n12\n12\n12\n12\n12\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n14\n14\n14\n14\n14\n14\n14\n14\n14\n14\n15\n15\n15\n15\n15\n15\n15\n15\n15\n15\n16\n16\n16\n16\n16\n16\n16\n16\n16\n16\n17\n17\n17\n17\n17\n17\n17\n17\n17\n17\n18\n18\n18\n18\n18\n18\n18\n18\n18\n18"
	for i, s := range strings.Fields(TierUnlockLevel) {
		fmt.Printf("%d:%s,", i+1, s)
	}
	fmt.Printf("\nGoldUpgradeLevel\n")
	GoldUpgradeLevel := "0\n1000\n2000\n3000\n4000\n5000\n6000\n7000\n8000\n9000\n10000\n11000\n12000\n13000\n14000\n15000\n16000\n17000\n18000\n19000\n20000\n21000\n22000\n23000\n24000\n25000\n26000\n27000\n28000\n29000\n30000\n31000\n32000\n33000\n34000\n35000\n36000\n37000\n38000\n39000\n40000\n41000\n42000\n43000\n44000\n45000\n46000\n47000\n48000\n49000\n50000\n51000\n52000\n53000\n54000\n55000\n56000\n57000\n58000\n59000\n60000\n61000\n62000\n63000\n64000\n65000\n66000\n67000\n68000\n69000\n70000\n71000\n72000\n73000\n74000\n75000\n76000\n77000\n78000\n79000\n80000\n81000\n82000\n83000\n84000\n85000\n86000\n87000\n88000\n89000\n90000\n91000\n92000\n93000\n94000\n95000\n96000\n97000\n98000\n99000\n100000\n101000\n102000\n103000\n104000\n105000\n106000\n107000\n108000\n109000\n110000\n111000\n112000\n113000\n114000\n115000\n116000\n117000\n118000\n119000\n120000\n121000\n122000\n123000\n124000\n125000\n126000\n127000\n128000\n129000\n130000\n131000\n132000\n133000\n134000\n135000\n136000\n137000\n138000\n139000\n140000\n141000\n142000\n143000\n144000\n145000\n146000\n147000\n148000\n149000\n150000\n151000\n152000\n153000\n154000\n155000\n156000\n157000\n158000\n159000\n160000\n161000\n162000\n163000\n164000\n165000\n166000\n167000\n168000\n169000\n170000\n171000\n172000\n173000\n174000\n175000\n176000\n177000\n178000\n179000\n180000\n181000\n182000\n183000\n184000\n185000\n186000\n187000\n188000\n189000\n190000\n191000\n192000\n193000\n194000\n195000\n196000\n197000\n198000\n199000\n200000\n201000\n202000\n203000\n204000\n205000\n206000\n207000\n208000\n209000\n210000\n211000\n212000\n213000\n214000\n215000\n216000\n217000\n218000\n219000"
	for i, s := range strings.Fields(GoldUpgradeLevel) {
		fmt.Printf("%d:%s,", i+1, s)
	}
	fmt.Printf("\nStoneUpgradeTier\n")
	StoneUpgradeTier := "10\n\n\n\n\n\n\n\n\n\n10\n\n\n\n\n\n\n\n\n\n20\n\n\n\n\n\n\n\n\n\n30\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n30\n\n\n\n\n\n\n\n\n\n50\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n50\n\n\n\n\n\n\n\n\n\n50\n\n\n\n\n\n\n\n\n\n70\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n70\n\n\n\n\n\n\n\n\n\n70\n\n\n\n\n\n\n\n\n\n70\n\n\n\n\n\n\n\n\n\n100\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n100\n\n\n\n\n\n\n\n\n\n100\n\n\n\n\n\n\n\n\n\n100\n\n\n\n\n\n\n\n\n\n100\n\n\n\n\n\n\n\n\n\n100\n\n\n\n\n\n\n\n\n"
	for i, s := range strings.Fields(StoneUpgradeTier) {
		fmt.Printf("%d:%s,", i+1, s)
	}
	fmt.Printf("\nGemUpgradeTier\n")
	GemUpgradeTier := "100\n\n\n\n\n\n\n\n\n\n100\n\n\n\n\n\n\n\n\n\n200\n\n\n\n\n\n\n\n\n\n300\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n300\n\n\n\n\n\n\n\n\n\n500\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n500\n\n\n\n\n\n\n\n\n\n500\n\n\n\n\n\n\n\n\n\n700\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n700\n\n\n\n\n\n\n\n\n\n700\n\n\n\n\n\n\n\n\n\n700\n\n\n\n\n\n\n\n\n\n1000\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n1000\n\n\n\n\n\n\n\n\n\n1000\n\n\n\n\n\n\n\n\n\n1000\n\n\n\n\n\n\n\n\n\n1000\n\n\n\n\n\n\n\n\n\n1000\n\n\n\n\n\n\n\n\n"
	for i, s := range strings.Fields(GemUpgradeTier) {
		fmt.Printf("%d:%s,", i+1, s)
	}
}

func TestGenConfigEquipment(t *testing.T) {
	fmt.Printf("RarityUnlockLevel\n")
	RarityUnlockLevel := "1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n2\n2\n2\n2\n2\n2\n2\n2\n2\n2\n3\n3\n3\n3\n3\n3\n3\n3\n3\n3\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n5\n5\n5\n5\n5\n5\n5\n5\n5\n5\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n8\n8\n8\n8\n8\n8\n8\n8\n8\n8\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n11\n11\n11\n11\n11\n11\n11\n11\n11\n11\n12\n12\n12\n12\n12\n12\n12\n12\n12\n12\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n13\n14\n14\n14\n14\n14\n14\n14\n14\n14\n14\n15\n15\n15\n15\n15\n15\n15\n15\n15\n15\n16\n16\n16\n16\n16\n16\n16\n16\n16\n16\n17\n17\n17\n17\n17\n17\n17\n17\n17\n17\n18\n18\n18\n18\n18\n18\n18\n18\n18\n18"
	for i, s := range strings.Fields(RarityUnlockLevel) {
		fmt.Printf("%d:%s,", i+1, s)
	}
	fmt.Printf("\nGoldUpgradeLevel\n")
	GoldUpgradeLevel := "0\n1000\n2000\n3000\n4000\n5000\n6000\n7000\n8000\n9000\n10000\n11000\n12000\n13000\n14000\n15000\n16000\n17000\n18000\n19000\n20000\n21000\n22000\n23000\n24000\n25000\n26000\n27000\n28000\n29000\n30000\n31000\n32000\n33000\n34000\n35000\n36000\n37000\n38000\n39000\n40000\n41000\n42000\n43000\n44000\n45000\n46000\n47000\n48000\n49000\n50000\n51000\n52000\n53000\n54000\n55000\n56000\n57000\n58000\n59000\n60000\n61000\n62000\n63000\n64000\n65000\n66000\n67000\n68000\n69000\n70000\n71000\n72000\n73000\n74000\n75000\n76000\n77000\n78000\n79000\n80000\n81000\n82000\n83000\n84000\n85000\n86000\n87000\n88000\n89000\n90000\n91000\n92000\n93000\n94000\n95000\n96000\n97000\n98000\n99000\n100000\n101000\n102000\n103000\n104000\n105000\n106000\n107000\n108000\n109000\n110000\n111000\n112000\n113000\n114000\n115000\n116000\n117000\n118000\n119000\n120000\n121000\n122000\n123000\n124000\n125000\n126000\n127000\n128000\n129000\n130000\n131000\n132000\n133000\n134000\n135000\n136000\n137000\n138000\n139000\n140000\n141000\n142000\n143000\n144000\n145000\n146000\n147000\n148000\n149000\n150000\n151000\n152000\n153000\n154000\n155000\n156000\n157000\n158000\n159000\n160000\n161000\n162000\n163000\n164000\n165000\n166000\n167000\n168000\n169000\n170000\n171000\n172000\n173000\n174000\n175000\n176000\n177000\n178000\n179000\n180000\n181000\n182000\n183000\n184000\n185000\n186000\n187000\n188000\n189000\n190000\n191000\n192000\n193000\n194000\n195000\n196000\n197000\n198000\n199000\n200000\n201000\n202000\n203000\n204000\n205000\n206000\n207000\n208000\n209000\n210000\n211000\n212000\n213000\n214000\n215000\n216000\n217000\n218000\n219000"
	for i, s := range strings.Fields(GoldUpgradeLevel) {
		fmt.Printf("%d:%s,", i+1, s)
	}
	fmt.Printf("\nTotalGoldUpgradeLevel\n")
	TotalGoldUpgradeLevel := "0\n1000\n3000\n6000\n10000\n15000\n21000\n28000\n36000\n45000\n55000\n66000\n78000\n91000\n105000\n120000\n136000\n153000\n171000\n190000\n210000\n231000\n253000\n276000\n300000\n325000\n351000\n378000\n406000\n435000\n465000\n496000\n528000\n561000\n595000\n630000\n666000\n703000\n741000\n780000\n820000\n861000\n903000\n946000\n990000\n1035000\n1081000\n1128000\n1176000\n1225000\n1275000\n1326000\n1378000\n1431000\n1485000\n1540000\n1596000\n1653000\n1711000\n1770000\n1830000\n1891000\n1953000\n2016000\n2080000\n2145000\n2211000\n2278000\n2346000\n2415000\n2485000\n2556000\n2628000\n2701000\n2775000\n2850000\n2926000\n3003000\n3081000\n3160000\n3240000\n3321000\n3403000\n3486000\n3570000\n3655000\n3741000\n3828000\n3916000\n4005000\n4095000\n4186000\n4278000\n4371000\n4465000\n4560000\n4656000\n4753000\n4851000\n4950000\n5050000\n5151000\n5253000\n5356000\n5460000\n5565000\n5671000\n5778000\n5886000\n5995000\n6105000\n6216000\n6328000\n6441000\n6555000\n6670000\n6786000\n6903000\n7021000\n7140000\n7260000\n7381000\n7503000\n7626000\n7750000\n7875000\n8001000\n8128000\n8256000\n8385000\n8515000\n8646000\n8778000\n8911000\n9045000\n9180000\n9316000\n9453000\n9591000\n9730000\n9870000\n10011000\n10153000\n10296000\n10440000\n10585000\n10731000\n10878000\n11026000\n11175000\n11325000\n11476000\n11628000\n11781000\n11935000\n12090000\n12246000\n12403000\n12561000\n12720000\n12880000\n13041000\n13203000\n13366000\n13530000\n13695000\n13861000\n14028000\n14196000\n14365000\n14535000\n14706000\n14878000\n15051000\n15225000\n15400000\n15576000\n15753000\n15931000\n16110000\n16290000\n16471000\n16653000\n16836000\n17020000\n17205000\n17391000\n17578000\n17766000\n17955000\n18145000\n18336000\n18528000\n18721000\n18915000\n19110000\n19306000\n19503000\n19701000\n19900000\n20100000\n20301000\n20503000\n20706000\n20910000\n21115000\n21321000\n21528000\n21736000\n21945000\n22155000\n22366000\n22578000\n22791000\n23005000\n23220000\n23436000\n23653000\n23871000\n24090000"
	for i, s := range strings.Fields(TotalGoldUpgradeLevel) {
		fmt.Printf("%d:%s,", i+1, s)
	}
	fmt.Printf("\nDesignUpgradeLevel\n")
	DesignUpgradeLevel := "0\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n20\n21\n22\n23\n24\n25\n26\n27\n28\n29\n30\n31\n32\n33\n34\n35\n36\n37\n38\n39\n40\n41\n42\n43\n44\n45\n46\n47\n48\n49\n50\n51\n52\n53\n54\n55\n56\n57\n58\n59\n60\n61\n62\n63\n64\n65\n66\n67\n68\n69\n70\n71\n72\n73\n74\n75\n76\n77\n78\n79\n80\n81\n82\n83\n84\n85\n86\n87\n88\n89\n90\n91\n92\n93\n94\n95\n96\n97\n98\n99\n100\n101\n102\n103\n104\n105\n106\n107\n108\n109\n110\n111\n112\n113\n114\n115\n116\n117\n118\n119\n120\n121\n122\n123\n124\n125\n126\n127\n128\n129\n130\n131\n132\n133\n134\n135\n136\n137\n138\n139\n140\n141\n142\n143\n144\n145\n146\n147\n148\n149\n150\n151\n152\n153\n154\n155\n156\n157\n158\n159\n160\n161\n162\n163\n164\n165\n166\n167\n168\n169\n170\n171\n172\n173\n174\n175\n176\n177\n178\n179\n180\n181\n182\n183\n184\n185\n186\n187\n188\n189\n190\n191\n192\n193\n194\n195\n196\n197\n198\n199\n200\n201\n202\n203\n204\n205\n206\n207\n208\n209\n210\n211\n212\n213\n214\n215\n216\n217\n218\n219"
	for i, s := range strings.Fields(DesignUpgradeLevel) {
		fmt.Printf("%d:%s,", i+1, s)
	}
	fmt.Printf("\nTotalDesignUpgradeLevel\n")
	TotalDesignUpgradeLevel := "0\n1\n3\n6\n10\n15\n21\n28\n36\n45\n55\n66\n78\n91\n105\n120\n136\n153\n171\n190\n210\n231\n253\n276\n300\n325\n351\n378\n406\n435\n465\n496\n528\n561\n595\n630\n666\n703\n741\n780\n820\n861\n903\n946\n990\n1035\n1081\n1128\n1176\n1225\n1275\n1326\n1378\n1431\n1485\n1540\n1596\n1653\n1711\n1770\n1830\n1891\n1953\n2016\n2080\n2145\n2211\n2278\n2346\n2415\n2485\n2556\n2628\n2701\n2775\n2850\n2926\n3003\n3081\n3160\n3240\n3321\n3403\n3486\n3570\n3655\n3741\n3828\n3916\n4005\n4095\n4186\n4278\n4371\n4465\n4560\n4656\n4753\n4851\n4950\n5050\n5151\n5253\n5356\n5460\n5565\n5671\n5778\n5886\n5995\n6105\n6216\n6328\n6441\n6555\n6670\n6786\n6903\n7021\n7140\n7260\n7381\n7503\n7626\n7750\n7875\n8001\n8128\n8256\n8385\n8515\n8646\n8778\n8911\n9045\n9180\n9316\n9453\n9591\n9730\n9870\n10011\n10153\n10296\n10440\n10585\n10731\n10878\n11026\n11175\n11325\n11476\n11628\n11781\n11935\n12090\n12246\n12403\n12561\n12720\n12880\n13041\n13203\n13366\n13530\n13695\n13861\n14028\n14196\n14365\n14535\n14706\n14878\n15051\n15225\n15400\n15576\n15753\n15931\n16110\n16290\n16471\n16653\n16836\n17020\n17205\n17391\n17578\n17766\n17955\n18145\n18336\n18528\n18721\n18915\n19110\n19306\n19503\n19701\n19900\n20100\n20301\n20503\n20706\n20910\n21115\n21321\n21528\n21736\n21945\n22155\n22366\n22578\n22791\n23005\n23220\n23436\n23653\n23871\n24090"
	for i, s := range strings.Fields(TotalDesignUpgradeLevel) {
		fmt.Printf("%d:%s,", i+1, s)
	}
}

func TestGenConfigTechPart(t *testing.T) {

}

func TestGenConfigPet(t *testing.T) {
	fmt.Printf("GemUnlock\n")
	GemUnlock := "500\n500\n500\n700\n700\n700\n1000\n1000\n1000\n1500\n1500\n1500\n2000\n2000\n2000"
	for i, s := range strings.Fields(GemUnlock) {
		fmt.Printf("%d:%s,", i+1, s)
	}
	fmt.Printf("\nTierUnlockLevel\n")
	TierUnlockLevel := "1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n2\n2\n2\n2\n2\n2\n2\n2\n2\n2\n3\n3\n3\n3\n3\n3\n3\n3\n3\n3\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n5\n5\n5\n5\n5\n5\n5\n5\n5\n5\n5\n5\n5\n5\n5\n5\n5\n5\n5\n5\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7"
	for i, s := range strings.Fields(TierUnlockLevel) {
		fmt.Printf("%d:%s,", i+1, s)
	}
	fmt.Printf("\nGoldUpgradeLevel\n")
	GoldUpgradeLevel := "0\n1000\n2000\n3000\n4000\n5000\n6000\n7000\n8000\n9000\n10000\n11000\n12000\n13000\n14000\n15000\n16000\n17000\n18000\n19000\n20000\n21000\n22000\n23000\n24000\n25000\n26000\n27000\n28000\n29000\n30000\n31000\n32000\n33000\n34000\n35000\n36000\n37000\n38000\n39000\n40000\n41000\n42000\n43000\n44000\n45000\n46000\n47000\n48000\n49000\n50000\n51000\n52000\n53000\n54000\n55000\n56000\n57000\n58000\n59000\n60000\n61000\n62000\n63000\n64000\n65000\n66000\n67000\n68000\n69000\n70000\n71000\n72000\n73000\n74000\n75000\n76000\n77000\n78000\n79000\n80000\n81000\n82000\n83000\n84000\n85000\n86000\n87000\n88000\n89000\n90000\n91000\n92000\n93000\n94000\n95000\n96000\n97000\n98000\n99000\n100000\n101000\n102000\n103000\n104000\n105000\n106000\n107000\n108000\n109000\n110000\n111000\n112000\n113000\n114000\n115000\n116000\n117000\n118000\n119000"
	for i, s := range strings.Fields(GoldUpgradeLevel) {
		fmt.Printf("%d:%s,", i+1, s)
	}
	fmt.Printf("\nPieceUpgradeTier\n")
	PieceUpgradeTier := "10\n\n\n\n\n\n\n\n\n\n10\n\n\n\n\n\n\n\n\n\n20\n\n\n\n\n\n\n\n\n\n30\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n50\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n70\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n100\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
	for i, s := range strings.Fields(PieceUpgradeTier) {
		fmt.Printf("%d:%s,", i+1, s)
	}
}

func TestGenConfigTalent(t *testing.T) {
	fmt.Printf("LevelUnlockNormal\n")
	for i := 1; i <= 300; i++ {
		fmt.Printf("%d:%d,", i, (i-1)/3+1)
	}
	fmt.Printf("\nLevelUnlockSpecial\n")
	LevelUnlockSpecial := "3\n\n\n\n\n\n\n\n\n5\n\n\n\n\n\n7\n\n\n\n\n\n9\n\n\n\n\n\n11\n\n\n\n\n\n13\n\n\n\n\n\n15\n\n\n\n\n\n20\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n25\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n30\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n35\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n40\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n50\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n60\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n70\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n80\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n90\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n100\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
	for i, s := range strings.Fields(LevelUnlockSpecial) {
		fmt.Printf("%d:%s,", i+1, s)
	}
	fmt.Printf("\nGoldUpgradeNormal\n")
	GoldUpgradeNormal := "1000\n2000\n3000\n4000\n5000\n6000\n7000\n8000\n9000\n10000\n11000\n12000\n13000\n14000\n15000\n16000\n17000\n18000\n19000\n20000\n21000\n22000\n23000\n24000\n25000\n26000\n27000\n28000\n29000\n30000\n31000\n32000\n33000\n34000\n35000\n36000\n37000\n38000\n39000\n40000\n41000\n42000\n43000\n44000\n45000\n46000\n47000\n48000\n49000\n50000\n51000\n52000\n53000\n54000\n55000\n56000\n57000\n58000\n59000\n60000\n61000\n62000\n63000\n64000\n65000\n66000\n67000\n68000\n69000\n70000\n71000\n72000\n73000\n74000\n75000\n76000\n77000\n78000\n79000\n80000\n81000\n82000\n83000\n84000\n85000\n86000\n87000\n88000\n89000\n90000\n91000\n92000\n93000\n94000\n95000\n96000\n97000\n98000\n99000\n100000\n101000\n102000\n103000\n104000\n105000\n106000\n107000\n108000\n109000\n110000\n111000\n112000\n113000\n114000\n115000\n116000\n117000\n118000\n119000\n120000\n121000\n122000\n123000\n124000\n125000\n126000\n127000\n128000\n129000\n130000\n131000\n132000\n133000\n134000\n135000\n136000\n137000\n138000\n139000\n140000\n141000\n142000\n143000\n144000\n145000\n146000\n147000\n148000\n149000\n150000\n151000\n152000\n153000\n154000\n155000\n156000\n157000\n158000\n159000\n160000\n161000\n162000\n163000\n164000\n165000\n166000\n167000\n168000\n169000\n170000\n171000\n172000\n173000\n174000\n175000\n176000\n177000\n178000\n179000\n180000\n181000\n182000\n183000\n184000\n185000\n186000\n187000\n188000\n189000\n190000\n191000\n192000\n193000\n194000\n195000\n196000\n197000\n198000\n199000\n200000\n201000\n202000\n203000\n204000\n205000\n206000\n207000\n208000\n209000\n210000\n211000\n212000\n213000\n214000\n215000\n216000\n217000\n218000\n219000\n220000\n221000\n222000\n223000\n224000\n225000\n226000\n227000\n228000\n229000\n230000\n231000\n232000\n233000\n234000\n235000\n236000\n237000\n238000\n239000\n240000\n241000\n242000\n243000\n244000\n245000\n246000\n247000\n248000\n249000\n250000\n251000\n252000\n253000\n254000\n255000\n256000\n257000\n258000\n259000\n260000\n261000\n262000\n263000\n264000\n265000\n266000\n267000\n268000\n269000\n270000\n271000\n272000\n273000\n274000\n275000\n276000\n277000\n278000\n279000\n280000\n281000\n282000\n283000\n284000\n285000\n286000\n287000\n288000\n289000\n290000\n291000\n292000\n293000\n294000\n295000\n296000\n297000\n298000\n299000\n300000"
	for i, s := range strings.Fields(GoldUpgradeNormal) {
		fmt.Printf("%d:%s,", i+1, s)
	}
	fmt.Printf("\nGemUpgradeSpecial\n")
	GemUpgradeSpecial := "100\n\n\n\n\n\n\n\n\n200\n\n\n\n\n\n300\n\n\n\n\n\n500\n\n\n\n\n\n500\n\n\n\n\n\n700\n\n\n\n\n\n700\n\n\n\n\n\n1000\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n1000\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n1000\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n1000\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n1000\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n2000\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n2000\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n2000\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n2000\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n2000\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n2000\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
	for i, s := range strings.Fields(GemUpgradeSpecial) {
		fmt.Printf("%d:%s,", i+1, s)
	}
}

func TestGenConfigCampaign(t *testing.T) {
	fmt.Printf("\nGoldReward\n")
	GoldReward := "4000\n4400\n4800\n5200\n5600\n6000\n6400\n6800\n7200\n7600\n8000\n8400\n8800\n9200\n9600\n10000\n10400\n10800\n11200\n11600\n12000\n12400\n12800\n13200\n13600\n14000\n14400\n14800\n15200\n15600\n16000\n16400\n16800\n17200\n17600\n18000\n18400\n18800\n19200\n19600\n20000\n20400\n20800\n21200\n21600\n22000\n22400\n22800\n23200\n23600\n24000\n24400\n24800\n25200\n25600\n26000\n26400\n26800\n27200\n27600\n28000\n28400\n28800\n29200\n29600\n30000\n30400\n30800\n31200\n31600\n32000\n32400\n32800\n33200\n33600\n34000\n34400\n34800\n35200\n35600\n36000\n36400\n36800\n37200\n37600\n38000\n38400\n38800\n39200\n39600\n40000\n40400\n40800\n41200\n41600\n42000\n42400\n42800\n43200\n43600"
	for i, s := range strings.Fields(GoldReward) {
		fmt.Printf("%d:%s,", i+1, s)
	}
	fmt.Printf("\nGoldSpin\n")
	GoldSpin := "8000\n8800\n9600\n10400\n11200\n12000\n12800\n13600\n14400\n15200\n16000\n16800\n17600\n18400\n19200\n20000\n20800\n21600\n22400\n23200\n24000\n24800\n25600\n26400\n27200\n28000\n28800\n29600\n30400\n31200\n32000\n32800\n33600\n34400\n35200\n36000\n36800\n37600\n38400\n39200\n40000\n40800\n41600\n42400\n43200\n44000\n44800\n45600\n46400\n47200\n48000\n48800\n49600\n50400\n51200\n52000\n52800\n53600\n54400\n55200\n56000\n56800\n57600\n58400\n59200\n60000\n60800\n61600\n62400\n63200\n64000\n64800\n65600\n66400\n67200\n68000\n68800\n69600\n70400\n71200\n72000\n72800\n73600\n74400\n75200\n76000\n76800\n77600\n78400\n79200\n80000\n80800\n81600\n82400\n83200\n84000\n84800\n85600\n86400\n87200"
	for i, s := range strings.Fields(GoldSpin) {
		fmt.Printf("%d:%s,", i+1, s)
	}
	fmt.Printf("\nexr\n")
	exr := "1000\n1100\n1200\n1300\n1400\n1500\n1600\n1700\n1800\n1900\n2000\n2100\n2200\n2300\n2400\n2500\n2600\n2700\n2800\n2900\n3000\n3100\n3200\n3300\n3400\n3500\n3600\n3700\n3800\n3900\n4000\n4100\n4200\n4300\n4400\n4500\n4600\n4700\n4800\n4900\n5000\n5100\n5200\n5300\n5400\n5500\n5600\n5700\n5800\n5900\n6000\n6100\n6200\n6300\n6400\n6500\n6600\n6700\n6800\n6900\n7000\n7100\n7200\n7300\n7400\n7500\n7600\n7700\n7800\n7900\n8000\n8100\n8200\n8300\n8400\n8500\n8600\n8700\n8800\n8900\n9000\n9100\n9200\n9300\n9400\n9500\n9600\n9700\n9800\n9900\n10000\n10100\n10200\n10300\n10400\n10500\n10600\n10700\n10800\n10900"
	for i, s := range strings.Fields(exr) {
		fmt.Printf("%d:%s,", i+1, s)
	}
}

func TestGenConfigChallenge(t *testing.T) {
	fmt.Printf("gor\n")
	gor := "15000\n33000\n38500\n44000\n49500\n55000\n60500\n66000\n71500\n77000\n82500\n88000\n93500\n99000\n104500\n110000\n115500\n121000\n126500\n132000\n137500\n143000\n148500\n154000\n159500\n165000\n170500\n176000\n181500\n187000\n192500\n198000\n203500\n209000\n214500\n220000\n225500\n231000\n236500\n242000\n247500\n253000\n258500\n264000\n269500\n275000\n280500\n286000\n291500\n297000\n302500\n308000\n313500\n319000\n324500\n330000\n335500\n341000\n346500\n352000\n357500\n363000\n368500\n374000\n379500\n385000\n390500\n396000\n401500\n407000\n412500\n418000\n423500\n429000\n434500\n440000\n445500\n451000\n456500\n462000\n467500\n473000\n478500\n484000\n489500\n495000\n500500\n506000\n511500\n517000\n522500\n528000\n533500\n539000\n544500\n550000\n555500\n561000\n566500\n572000"
	for i, s := range strings.Fields(gor) {
		fmt.Printf("%d: %s,", i+1, s)
	}
	fmt.Printf("\nexr\n")
	exr := "1000\n1100\n1200\n1300\n1400\n1500\n1600\n1700\n1800\n1900\n2000\n2100\n2200\n2300\n2400\n2500\n2600\n2700\n2800\n2900\n3000\n3100\n3200\n3300\n3400\n3500\n3600\n3700\n3800\n3900\n4000\n4100\n4200\n4300\n4400\n4500\n4600\n4700\n4800\n4900\n5000\n5100\n5200\n5300\n5400\n5500\n5600\n5700\n5800\n5900\n6000\n6100\n6200\n6300\n6400\n6500\n6600\n6700\n6800\n6900\n7000\n7100\n7200\n7300\n7400\n7500\n7600\n7700\n7800\n7900\n8000\n8100\n8200\n8300\n8400\n8500\n8600\n8700\n8800\n8900\n9000\n9100\n9200\n9300\n9400\n9500\n9600\n9700\n9800\n9900\n10000\n10100\n10200\n10300\n10400\n10500\n10600\n10700\n10800\n10900"
	for i, s := range strings.Fields(exr) {
		fmt.Printf("%d: %s,", i+1, s)
	}
	fmt.Printf("\nstr\n")
	str := "3\n3\n3\n3\n3\n3\n3\n3\n3\n3\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n5\n5\n5\n5\n5\n5\n5\n5\n5\n5\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n8\n8\n8\n8\n8\n8\n8\n8\n8\n8\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n11\n11\n11\n11\n11\n11\n11\n11\n11\n11\n12\n12\n12\n12\n12\n12\n12\n12\n12\n12"
	for i, s := range strings.Fields(str) {
		fmt.Printf("%d: %s,", i+1, s)
	}
	fmt.Printf("\ntpr\n")
	tpr := "3\n3\n3\n3\n3\n3\n3\n3\n3\n3\n4\n4\n4\n4\n4\n4\n4\n4\n4\n4\n5\n5\n5\n5\n5\n5\n5\n5\n5\n5\n6\n6\n6\n6\n6\n6\n6\n6\n6\n6\n7\n7\n7\n7\n7\n7\n7\n7\n7\n7\n8\n8\n8\n8\n8\n8\n8\n8\n8\n8\n9\n9\n9\n9\n9\n9\n9\n9\n9\n9\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n11\n11\n11\n11\n11\n11\n11\n11\n11\n11\n12\n12\n12\n12\n12\n12\n12\n12\n12\n12"
	for i, s := range strings.Fields(tpr) {
		fmt.Printf("%d: %s,", i+1, s)
	}
}

func TestGenConfigPatrol(t *testing.T) {
	fmt.Printf("gor\n")
	gor := "2000\n2200\n2400\n2600\n2800\n3000\n3200\n3400\n3600\n3800\n4000\n4200\n4400\n4600\n4800\n5000\n5200\n5400\n5600\n5800\n6000\n6200\n6400\n6600\n6800\n7000\n7200\n7400\n7600\n7800\n8000\n8200\n8400\n8600\n8800\n9000\n9200\n9400\n9600\n9800\n10000\n10200\n10400\n10600\n10800\n11000\n11200\n11400\n11600\n11800\n12000\n12200\n12400\n12600\n12800\n13000\n13200\n13400\n13600\n13800\n14000\n14200\n14400\n14600\n14800\n15000\n15200\n15400\n15600\n15800\n16000\n16200\n16400\n16600\n16800\n17000\n17200\n17400\n17600\n17800\n18000\n18200\n18400\n18600\n18800\n19000\n19200\n19400\n19600\n19800\n20000\n20200\n20400\n20600\n20800\n21000\n21200\n21400\n21600\n21800"
	for i, s := range strings.Fields(gor) {
		fmt.Printf("%d: %s,", i+1, s)
	}
	fmt.Printf("\nexr\n")
	exr := "500\n550\n600\n650\n700\n750\n800\n850\n900\n950\n1000\n1050\n1100\n1150\n1200\n1250\n1300\n1350\n1400\n1450\n1500\n1550\n1600\n1650\n1700\n1750\n1800\n1850\n1900\n1950\n2000\n2050\n2100\n2150\n2200\n2250\n2300\n2350\n2400\n2450\n2500\n2550\n2600\n2650\n2700\n2750\n2800\n2850\n2900\n2950\n3000\n3050\n3100\n3150\n3200\n3250\n3300\n3350\n3400\n3450\n3500\n3550\n3600\n3650\n3700\n3750\n3800\n3850\n3900\n3950\n4000\n4050\n4100\n4150\n4200\n4250\n4300\n4350\n4400\n4450\n4500\n4550\n4600\n4650\n4700\n4750\n4800\n4850\n4900\n4950\n5000\n5050\n5100\n5150\n5200\n5250\n5300\n5350\n5400\n5450"
	for i, s := range strings.Fields(exr) {
		fmt.Printf("%d: %s,", i+1, s)
	}
	fmt.Printf("\nder\n")
	der := "0,50\n0,63\n0,75\n0,88\n1,00\n1,13\n1,25\n1,38\n1,50\n1,63\n1,75\n1,88\n2,00\n2,13\n2,25\n2,38\n2,50\n2,63\n2,75\n2,88\n3,00\n3,13\n3,25\n3,38\n3,50\n3,63\n3,75\n3,88\n4,00\n4,13\n4,25\n4,38\n4,50\n4,63\n4,75\n4,88\n5,00\n5,13\n5,25\n5,38\n5,50\n5,63\n5,75\n5,88\n6,00\n6,13\n6,25\n6,38\n6,50\n6,63\n6,75\n6,88\n7,00\n7,13\n7,25\n7,38\n7,50\n7,63\n7,75\n7,88\n8,00\n8,13\n8,25\n8,38\n8,50\n8,63\n8,75\n8,88\n9,00\n9,13\n9,25\n9,38\n9,50\n9,63\n9,75\n9,88\n10,00\n10,13\n10,25\n10,38\n10,50\n10,63\n10,75\n10,88\n11,00\n11,13\n11,25\n11,38\n11,50\n11,63\n11,75\n11,88\n12,00\n12,13\n12,25\n12,38\n12,50\n12,63\n12,75\n12,88"
	for i, s := range strings.Fields(der) {
		fmt.Printf("%d: %s,", i+1, strings.ReplaceAll(s, ",", "."))
	}
	fmt.Printf("\neqr\n")
	eqr := "0,05\n0,06\n0,08\n0,09\n0,10\n0,11\n0,13\n0,14\n0,15\n0,16\n0,18\n0,19\n0,20\n0,21\n0,23\n0,24\n0,25\n0,26\n0,28\n0,29\n0,30\n0,31\n0,33\n0,34\n0,35\n0,36\n0,38\n0,39\n0,40\n0,41\n0,43\n0,44\n0,45\n0,46\n0,48\n0,49\n0,50\n0,51\n0,53\n0,54\n0,55\n0,56\n0,58\n0,59\n0,60\n0,61\n0,63\n0,64\n0,65\n0,66\n0,68\n0,69\n0,70\n0,71\n0,73\n0,74\n0,75\n0,76\n0,78\n0,79\n0,80\n0,81\n0,83\n0,84\n0,85\n0,86\n0,88\n0,89\n0,90\n0,91\n0,93\n0,94\n0,95\n0,96\n0,98\n0,99\n1,00\n1,01\n1,03\n1,04\n1,05\n1,06\n1,08\n1,09\n1,10\n1,11\n1,13\n1,14\n1,15\n1,16\n1,18\n1,19\n1,20\n1,21\n1,23\n1,24\n1,25\n1,26\n1,28\n1,29"
	for i, s := range strings.Fields(eqr) {
		fmt.Printf("%d: %s,", i+1, strings.ReplaceAll(s, ",", "."))
	}
}

func TestGenConfigAchievements(t *testing.T) {
	fmt.Printf("target\n")
	target := "1\t2\t3\t5\t7\t10\t20\t30\t50\t100\n10\t20\t30\t40\t50\t60\t70\t80\t90\t100\n10\t20\t30\t40\t50\t60\t70\t80\t90\t100\n10\t20\t50\t100\t200\t500\t1000\t10000\t100000\t1000000\n10\t20\t30\t50\t70\t100\t150\t200\t250\t300\n1\t2\t3\t4\t5\t6\t7\t10\t14\t18\n1\t2\t3\t5\t7\t10\t20\t30\t50\t100\n10\t20\t50\t100\t200\t500\t1000\t10000\t100000\t1000000\n100\t200\t500\t1000\t2000\t5000\t10000\t100000\t1000000\t10000000\n10000\t20000\t50000\t100000\t200000\t500000\t1000000\t10000000\t100000000\t1000000000\n100000\t200000\t500000\t1000000\t2000000\t5000000\t10000000\t100000000\t1000000000\t10000000000\n10\t20\t50\t100\t200\t500\t1000\t10000\t100000\t1000000\n1\t2\t3\t4\t5\t6\t7\t8\t9\t10\n10\t20\t30\t40\t50\t60\t70\t80\t100\t120\n10\t20\t50\t100\t200\t500\t1000\t10000\t100000\t1000000\n10\t20\t30\t40\t50\t60\t70\t80\t100\t120\n10\t20\t50\t100\t200\t500\t1000\t10000\t100000\t1000000\n1\t2\t3\t4\t5\t6\t7\t8\t9\t10\n10\t20\t30\t40\t50\t60\t70\t80\t100\t120"
	ss := make(map[int64]map[int64]string)
	var numQuest int64 = 0
	var numLimit int64 = 0
	for i, si := range strings.Split(target, "\n") {
		ss[int64(i+1)] = make(map[int64]string)
		for j, sj := range strings.Split(si, "\t") {
			ss[int64(i+1)][int64(j+1)] = strings.Fields(sj)[0]
			numLimit = int64(j + 1)
		}
		numQuest = int64(i + 1)
	}
	for i := int64(1); i <= numQuest; i++ {
		fmt.Printf("\n%d:{", i)
		for j := int64(1); j <= numLimit; j++ {
			fmt.Printf("%d:%s,", j, ss[i][j])
		}
		fmt.Printf("},")
	}
	fmt.Printf("\nreward\n")
	str := "10\t10\t20\t20\t30\t30\t40\t40\t50\t50"
	for i, s := range strings.Fields(str) {
		fmt.Printf("%d: %s,", i+1, s)
	}
}

func TestGenConfigShop(t *testing.T) {
	fmt.Printf("cpp\n")
	cpp := "1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n20\n21\n22\n23\n24\n25\n26\n27\n28\n29\n30\n31\n32\n33\n34\n35\n36\n37\n38\n39\n40\n41\n42\n43\n44\n45\n46\n47\n48\n49\n50\n51\n52\n53\n54\n55\n56\n57\n58\n59\n60\n61\n62\n63\n64\n65\n66\n67\n68\n69\n70\n71\n72\n73\n74\n75\n76\n77\n78\n79\n80\n81\n82\n83\n84\n85\n86\n87\n88\n89\n90\n91\n92\n93\n94\n95\n96\n97\n98\n99\n100"
	for i, s := range strings.Fields(cpp) {
		fmt.Printf("%d: %s, ", i+1, s)
	}
	for i, _ := range strings.Fields(cpp) {
		fmt.Printf("\n%d: {models.GiftNameGem:%d00, models.GiftNameGold: %d0000, models.GiftNameStoneRandom:%d, models.GiftNameMythChest: %d}, ", i+1, i+1, i+1, i+1, i+1)
	}
}

func TestGenConfigBattlePass(t *testing.T) {
	fmt.Printf("TargetPoint\n")
	TargetPoint := "100\n200\n300\n400\n500\n600\n700\n800\n900\n1000\n1100\n1200\n1300\n1400\n1500\n1600\n1700\n1800\n1900\n2000"
	for i, s := range strings.Fields(TargetPoint) {
		fmt.Printf("%d: %s, ", i+1, s)
	}
	fmt.Printf("\nFreeReward\n")
	FreeReward := "30\n30\n30\n30\n50\n30\n30\n30\n30\n50\n30\n30\n30\n30\n50\n30\n30\n30\n30\n100"
	for i, s := range strings.Fields(FreeReward) {
		fmt.Printf("%d: {GiftNameGem:%s}, ", i+1, s)
	}
	fmt.Printf("\nNormalReward\n")
	NormalReward := "150\n150\n150\n150\n250\n150\n150\n150\n150\n250\n150\n150\n150\n150\n250\n150\n150\n150\n150\n500"
	for i, s := range strings.Fields(NormalReward) {
		fmt.Printf("%d: {GiftNameGem:%s}, ", i+1, s)
	}
	fmt.Printf("\nSuperReward\n")
	SuperReward := "300\n300\n300\n300\n500\n300\n300\n300\n300\n500\n300\n300\n300\n300\n500\n300\n300\n300\n300\n1000"
	for i, s := range strings.Fields(SuperReward) {
		fmt.Printf("%d: {GiftNameGem:%s}, ", i+1, s)
	}
}

func TestGenConfigLevelPass1(t *testing.T) {
	fmt.Printf("TargetLevel\n")
	TargetLevel := "5\n10\n15\n20\n25\n30\n35\n40\n45\n50"
	for i, s := range strings.Fields(TargetLevel) {
		fmt.Printf("%d: %s, ", i+1, s)
	}
	fmt.Printf("\nFreeReward\n")
	FreeReward := "50\n50\n50\n50\n50\n100\n100\n100\n100\n100"
	for i, s := range strings.Fields(FreeReward) {
		fmt.Printf("%d: {GiftNameGem:%s}, ", i+1, s)
	}
	fmt.Printf("\nNormalReward\n")
	NormalReward := "500\n500\n500\n500\n500\n1000\n1000\n1000\n1000\n1000"
	for i, s := range strings.Fields(NormalReward) {
		fmt.Printf("%d: {GiftNameGem:%s}, ", i+1, s)
	}
	fmt.Printf("\nSuperReward\n")
	SuperReward := "1000\n1000\n1000\n1000\n1000\n2000\n2000\n2000\n2000\n2000"
	for i, s := range strings.Fields(SuperReward) {
		fmt.Printf("%d: {GiftNameGem:%s}, ", i+1, s)
	}
}

func TestGenConfigLevelPass2(t *testing.T) {
	fmt.Printf("TargetLevel\n")
	TargetLevel := "55\n60\n65\n70\n75\n80\n85\n90\n95\n100"
	for i, s := range strings.Fields(TargetLevel) {
		fmt.Printf("%d: %s, ", i+1, s)
	}
	fmt.Printf("\nFreeReward\n")
	FreeReward := "150\n150\n150\n150\n150\n200\n200\n200\n200\n200"
	for i, s := range strings.Fields(FreeReward) {
		fmt.Printf("%d: {GiftNameGem:%s}, ", i+1, s)
	}
	fmt.Printf("\nNormalReward\n")
	NormalReward := "1500\n1500\n1500\n1500\n1500\n2000\n2000\n2000\n2000\n2000"
	for i, s := range strings.Fields(NormalReward) {
		fmt.Printf("%d: {GiftNameGem:%s}, ", i+1, s)
	}
	fmt.Printf("\nSuperReward\n")
	SuperReward := "3000\n3000\n3000\n3000\n3000\n4000\n4000\n4000\n4000\n4000"
	for i, s := range strings.Fields(SuperReward) {
		fmt.Printf("%d: {GiftNameGem:%s}, ", i+1, s)
	}
}

func TestGenConfigCampaignPass(t *testing.T) {
	fmt.Printf("TargetLevel\n")
	TargetLevel := "10\n20\n30\n40\n50\n60\n70\n80\n90\n100"
	for i, s := range strings.Fields(TargetLevel) {
		fmt.Printf("%d: %s, ", i+1, s)
	}
	fmt.Printf("\nFreeReward\n")
	FreeReward := "150\n150\n150\n150\n150\n200\n200\n200\n200\n200"
	for i, s := range strings.Fields(FreeReward) {
		fmt.Printf("%d: {GiftNameGem:%s}, ", i+1, s)
	}
	fmt.Printf("\nNormalReward\n")
	NormalReward := "1500\n1500\n1500\n1500\n1500\n2000\n2000\n2000\n2000\n2000"
	for i, s := range strings.Fields(NormalReward) {
		fmt.Printf("%d: {GiftNameGem:%s}, ", i+1, s)
	}
	fmt.Printf("\nSuperReward\n")
	SuperReward := "3000\n3000\n3000\n3000\n3000\n4000\n4000\n4000\n4000\n4000"
	for i, s := range strings.Fields(SuperReward) {
		fmt.Printf("%d: {GiftNameGem:%s}, ", i+1, s)
	}
}
