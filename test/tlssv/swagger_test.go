package main

import (
	"fmt"
	"strings"
	"testing"
)

func TestGenSwagger(t *testing.T) {
	str := "\ntype UnlockHeroRequest struct {\n\tIndex int64 `json:\"x,omitempty\"`\n}\n\ntype UnlockHeroResponse struct {\n\tIndex int64 `json:\"x,omitempty\"`\n\tLevel int64 `json:\"l,omitempty\"`\n\tGem   int64 `json:\"g,omitempty\"`\n}\n\ntype UpgradeHeroRequest struct {\n\tIndex int64 `json:\"x,omitempty\"`\n}\n\ntype UpgradeHeroResponse struct {\n\tIndex int64 `json:\"x,omitempty\"`\n\tLevel int64 `json:\"l,omitempty\"`\n\tGold  int64 `json:\"g,omitempty\"`\n\tStone int64 `json:\"s,omitempty\"`\n}\n\ntype DowngradeHeroRequest struct {\n\tIndex int64 `json:\"x,omitempty\"`\n}\n\ntype DowngradeHeroResponse struct {\n\tIndex int64 `json:\"x,omitempty\"`\n\tLevel int64 `json:\"l,omitempty\"`\n\tGold  int64 `json:\"g,omitempty\"`\n\tStone int64 `json:\"s,omitempty\"`\n}\n\ntype UpgradeEquipmentRequest struct {\n\tId int64 `json:\"i,omitempty\"`\n}\n\ntype UpgradeEquipmentResponse struct {\n\tId     int64 `json:\"i,omitempty\"`\n\tLevel  int64 `json:\"l,omitempty\"`\n\tGold   int64 `json:\"g,omitempty\"`\n\tDesign int64 `json:\"d,omitempty\"`\n}\n\ntype DowngradeEquipmentRequest struct {\n\tId int64 `json:\"i,omitempty\"`\n}\n\ntype DowngradeEquipmentResponse struct {\n\tId     int64 `json:\"i,omitempty\"`\n\tLevel  int64 `json:\"l,omitempty\"`\n\tGold   int64 `json:\"g,omitempty\"`\n\tDesign int64 `json:\"d,omitempty\"`\n}\n\ntype MergeEquipmentRequest struct {\n\tId  int64   `json:\"i,omitempty\"`\n\tIds []int64 `json:\"is,omitempty\"`\n}\n\ntype SplitEquipmentRequest struct {\n\tId int64 `json:\"i,omitempty\"`\n}\n\ntype SplitEquipmentResponse struct {\n\tEquipment map[int64]*UEEquipment `json:\"e,omitempty\"`\n\tDesign    int64                  `json:\"d,omitempty\"`\n\tGold      int64                  `json:\"g,omitempty\"`\n}\n\ntype MergeTechPartRequest struct {\n\tId  int64   `json:\"i,omitempty\"`\n\tIds []int64 `json:\"is,omitempty\"`\n}\n\nconst (\n\tTalentNormal int64 = iota\n\tTalentSpecial\n)\n\ntype UpgradeTalentRequest struct {\n\tIndex int64 `json:\"x,omitempty\"`\n}\n\ntype UpgradeTalentResponse struct {\n\tNormal  int64 `json:\"n,omitempty\"`\n\tGold    int64 `json:\"g,omitempty\"`\n\tSpecial int64 `json:\"s,omitempty\"`\n\tDna     int64 `json:\"d,omitempty\"`\n}\n\ntype SplitTechPartRequest struct {\n\tId int64 `json:\"i,omitempty\"`\n}\n\ntype SplitTechPartResponse struct {\n\tTechPart map[int64]*UETechPart `json:\"t,omitempty\"`\n}\n"

	strs := strings.Split(str, "\n")
	for i, stri := range strs {
		if strings.Contains(stri, "struct") {
			fmt.Printf("\n\t\t%s:\n\t\t\ttype: object\n\t\t\tproperties:", strings.Fields(stri)[1])
			for j := i + 1; j < len(strs); j++ {
				if strings.Contains(strs[j], "}") {
					break
				}
				strjs := strings.Fields(strs[j])
				if len(strjs) < 1 || strjs[0] == "//" {
					break
				}
				fmt.Printf("\n\t\t\t\t%s:", strings.Split(strings.Split(strjs[2], "\"")[1], ",")[0])
				if strings.Contains(strjs[1], "string") {
					fmt.Printf("\n\t\t\t\t\ttype: string")
				} else if strings.Contains(strjs[1], "map[int64]map[int64]int64") {
					fmt.Printf("\n\t\t\t\t\ttype: object\n\t\t\t\t\tadditionalProperties:\n\t\t\t\t\t\ttype: int64")
				} else if strings.Contains(strjs[1], "map[int64]map[int64]bool") {
					fmt.Printf("\n\t\t\t\t\ttype: object\n\t\t\t\t\tadditionalProperties:\n\t\t\t\t\t\ttype: bool")
				} else if strings.Contains(strjs[1], "map[int64]int64") {
					fmt.Printf("\n\t\t\t\t\ttype: object\n\t\t\t\t\tadditionalProperties:\n\t\t\t\t\t\ttype: integer")
				} else if strings.Contains(strjs[1], "map[int64]bool") {
					fmt.Printf("\n\t\t\t\t\ttype: object\n\t\t\t\t\tadditionalProperties:\n\t\t\t\t\t\ttype: boolean")
				} else if strings.Contains(strjs[1], "int") {
					fmt.Printf("\n\t\t\t\t\ttype: integer")
				}
				fmt.Printf("\n\t\t\t\t\tdescription: %s", strjs[0])
			}
		}
	}
}
