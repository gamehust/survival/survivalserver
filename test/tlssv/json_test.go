package main_test

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strings"
	"testing"
)

type ABC = struct {
	A uint
	B int
	C string
	D float64
	E []byte
	F map[int64]int64
}

func TestJSON(t *testing.T) {
	aaa := ABC{
		A: 123,
		B: -123,
		C: "123",
		D: 123.123,
		E: []byte("111"),
		F: make(map[int64]int64),
	}
	aaa.F[1] = 1
	aaa.F[2] = 2
	aaa.F[3] = 3
	b, err := json.Marshal(&aaa)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("%s", b)
	bbb := make(map[string]interface{})
	if err := json.Unmarshal(b, &bbb); err != nil {
		t.Fatal(err)
	}
	t.Logf("%#v", bbb)

	for k, v := range bbb {
		t.Logf("%#v - %#v - %s - %s", k, v, reflect.TypeOf(k), reflect.TypeOf(v))
	}
}

func TestGenText(t *testing.T) {
	strs := []string{"Currency", "Hero", "Equipment", "Talent", "Stage", "Quest", "Shop", "MonthlyCard", "Event"}
	for _, s := range strs {
		fmt.Printf("\nfunc GetCF%s(ctx context.Context) (*models.CF%s, error) {\n\tfilter := bson.D{{ConfigBSONId, CF%sBSONId}}\n\traw, err := configCol.FindOne(ctx, filter).DecodeBytes()\n\tif err != nil {\n\t\treturn nil, err\n\t}\n\tvar cf%s models.CF%s\n\tif err = bson.Unmarshal(raw, &cf%s); err != nil {\n\t\treturn nil, err\n\t}\n\treturn &cf%s, nil\n}\n\nfunc UpsertCF%s(ctx context.Context, cf%s *models.CF%s) (bool, error) {\n\treplacement, err := bson.Marshal(cf%s)\n\tif err != nil {\n\t\treturn false, err\n\t}\n\tfilter := bson.D{{ConfigBSONId, CF%sBSONId}}\n\topts := &options.ReplaceOptions{}\n\topts.SetUpsert(true)\n\tresult, err := configCol.ReplaceOne(ctx, filter, replacement, opts)\n\tif err != nil {\n\t\treturn false, err\n\t}\n\treturn result.UpsertedCount > 0, nil\n}\n", s, s, s, s, s, s, s, s, s, s, s, s)
	}
}

func TestGenText2(t *testing.T) {
	strs := []string{"Currency", "Hero", "Equipment", "Talent", "Stage", "Quest", "Shop", "MonthlyCard", "Event"}
	for _, s := range strs {
		fmt.Printf("\n\tcf%s, err := repository.GetCF%s(ctx)\n\tdefer cancel()\n\tif err != nil && err == mongo.ErrNoDocuments {\n\t\tcf%s = &models.CF%s{}\n\t\tif _, err = repository.UpsertCF%s(ctx, cf%s); err != nil {\n\t\t\tpanic(err)\n\t\t}\n\t} else if err != nil {\n\t\tpanic(err)\n\t}", s, s, s, s, s, s)
	}
}

func TestGenText3(t *testing.T) {
	strs := []string{"Info", "Profile", "Currency", "Hero", "Equipment", "TechPart", "Talent", "Stage", "Quest", "Shop", "MonthlyCard", "GrowthFundLevel", "PiggyBank", "NormalDailyReward", "RookieLogin", "Event7Days"}
	for _, s := range strs {
		fmt.Printf("\nvar ue%sCol *mongo.Collection\n\nfunc GetUE%s(ctx context.Context, ui string) (*models.UE%s, error) {\n\tfilter := bson.D{{models.UE%sBSONUId, ui}}\n\traw, err := ue%sCol.FindOne(ctx, filter).DecodeBytes()\n\tif err != nil {\n\t\treturn nil, err\n\t}\n\tvar ue%s models.UE%s\n\tif err = bson.Unmarshal(raw, &ue%s); err != nil {\n\t\treturn nil, err\n\t}\n\treturn &ue%s, nil\n}\n\nfunc UpsertUE%s(ctx context.Context, ue%s *models.UE%s) (bool, error) {\n\treplacement, err := bson.Marshal(ue%s)\n\tif err != nil {\n\t\treturn false, err\n\t}\n\tfilter := bson.D{{models.UE%sBSONUId, ue%s.UId}}\n\topts := &options.ReplaceOptions{}\n\topts.SetUpsert(true)\n\tresult, err := ue%sCol.ReplaceOne(ctx, filter, replacement, opts)\n\tif err != nil {\n\t\treturn false, err\n\t}\n\treturn result.UpsertedCount > 0, nil\n}\n\nfunc DeleteUE%s(ctx context.Context, ui string) (bool, error) {\n\tfilter := bson.D{{models.UE%sBSONUId, ui}}\n\tresult, err := ue%sCol.DeleteOne(ctx, filter)\n\tif err != nil {\n\t\treturn false, err\n\t}\n\treturn result.DeletedCount > 0, nil\n}\n", s, s, s, s, s, s, s, s, s, s, s, s, s, s, s, s, s, s, s)
	}

}

func TestGenText4(t *testing.T) {
	strs := strings.Fields("500   2000   5000   11000   17000   23000   29000   30000   40000   40000   50000   55000   60000   80000   80000   100000   100000   100000   100000   100000   150000   150000   150000   150000   150000   150000   175000   200000   200000   200000   200000   200000   200000   300000   300000   300000   300000   300000   300000   300000   300000   300000   300000   400000   400000   400000   400000   400000   400000   400000   400000   400000   400000   500000   500000   500000   500000   500000   500000   500000   500000   500000   500000   500000   500000   500000   550000   600000   600000   600000   600000   600000   600000   600000   600000   600000   600000   600000   600000   600000   800000   800000   800000   800000   800000   800000   800000   800000   800000   800000   800000   800000   800000   800000   800000   800000   800000   800000   800000   800000  ")
	for _, s := range strs {
		fmt.Printf("%s,", s)
	}
}

func TestGenText5(t *testing.T) {
	var str [30][]string
	str[0] = strings.Fields("1\n3\n5\n7\n14\n21\n28\n35\n42\n49\n56\n63\n70\n77\n84\n91\n98\n105\n112\n119\n126\n133\n140\n147\n154\n161\n168\n175\n182\n189\n196\n203")
	str[1] = strings.Fields("5\n10\n15\n20\n25\n30\n35\n40\n45\n50\n55\n60\n65\n70\n75\n80\n85\n90\n95\n100\n105\n110\n115\n120\n125\n130\n135\n140\n145\n150\n155\n160")
	str[2] = strings.Fields("5\n10\n15\n20\n25\n30\n35\n40\n45\n50\n55\n60\n65\n70\n75\n80\n85\n90\n95\n100\n105\n110\n115\n120\n125\n130\n135\n140\n145\n150\n155\n160")
	str[3] = strings.Fields("5\n10\n20\n40\n80\n160\n320\n640\n1280\n2560\n5120\n10240\n20480\n40960\n81920\n163840\n327680\n655360\n1310720\n2621440\n5242880\n10485760\n20971520\n41943040\n83886080\n167772160\n335544320\n671088640\n1342177280\n2684354560\n5368709120\n10737418240")
	str[4] = strings.Fields("5\n10\n15\n20\n25\n30\n35\n40\n45\n50\n55\n60\n65\n70\n75\n80\n85\n90\n95\n100\n105\n110\n115\n120\n125\n130\n135\n140\n145\n150\n155\n160")
	str[5] = strings.Fields("5\n10\n20\n40\n80\n160\n320\n640\n1280\n2560\n5120\n10240\n20480\n40960\n81920\n163840\n327680\n655360\n1310720\n2621440\n5242880\n10485760\n20971520\n41943040\n83886080\n167772160\n335544320\n671088640\n1342177280\n2684354560\n5368709120\n10737418240")
	str[6] = strings.Fields("1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n20\n21\n22\n23\n24\n25\n26\n27\n28\n29\n30\n31\n32")
	str[7] = strings.Fields("5\n10\n20\n40\n80\n160\n320\n640\n1280\n2560\n5120\n10240\n20480\n40960\n81920\n163840\n327680\n655360\n1310720\n2621440\n5242880\n10485760\n20971520\n41943040\n83886080\n167772160\n335544320\n671088640\n1342177280\n2684354560\n5368709120\n10737418240")
	str[8] = strings.Fields("500\n1000\n2000\n4000\n8000\n16000\n32000\n64000\n128000\n256000\n512000\n1024000\n2048000\n4096000\n8192000\n16384000\n32768000\n65536000\n131072000\n262144000\n524288000\n1048576000\n2097152000\n4194304000\n8388608000\n16777216000\n33554432000\n67108864000\n134217728000\n268435456000\n536870912000\n1073741824000")
	str[9] = strings.Fields("10000\n20000\n40000\n80000\n160000\n320000\n640000\n1280000\n2560000\n5120000\n10240000\n20480000\n40960000\n81920000\n163840000\n327680000\n655360000\n1310720000\n2621440000\n5242880000\n10485760000\n20971520000\n41943040000\n83886080000\n167772160000\n335544320000\n671088640000\n1342177280000\n2684354560000\n5368709120000\n10737418240000\n21474836480000")
	str[10] = strings.Fields("5\n10\n15\n20\n25\n30\n35\n40\n45\n50\n55\n60\n65\n70\n75\n80\n85\n90\n95\n100\n105\n110\n115\n120\n125\n130\n135\n140\n145\n150\n155\n160")
	str[11] = strings.Fields("10\n20\n30\n40\n50\n60\n70\n80\n90\n100\n110\n120\n130\n140\n150\n160\n170\n180\n190\n200\n210\n220\n230\n240\n250\n260\n270\n280\n290\n300\n310\n320")
	str[12] = strings.Fields("10\n20\n30\n40\n50\n60\n70\n80\n90\n100\n110\n120\n130\n140\n150\n160\n170\n180\n190\n200\n210\n220\n230\n240\n250\n260\n270\n280\n290\n300\n310\n320")
	str[13] = strings.Fields("10\n20\n30\n40\n50\n60\n70\n80\n90\n100\n110\n120\n130\n140\n150\n160\n170\n180\n190\n200\n210\n220\n230\n240\n250\n260\n270\n280\n290\n300\n310\n320")
	str[14] = strings.Fields("\n\n10\n20\n30\n40\n50\n60\n70\n80\n90\n100\n110\n120\n130\n140\n150\n160\n170\n180\n190\n200\n210\n220\n230\n240\n250\n260\n270\n280\n290\n300")
	str[15] = strings.Fields("\n\n\n\n5\n10\n15\n20\n25\n30\n35\n40\n45\n50\n55\n60\n65\n70\n75\n80\n85\n90\n95\n100\n105\n110\n115\n120\n125\n130\n135\n140")
	str[16] = strings.Fields("\n\n\n\n\n\n\n\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n20\n21\n22\n23\n24")
	str[17] = strings.Fields("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15\n16")
	str[18] = strings.Fields("1\n2\n4\n8\n16\n32\n64\n128\n256\n512\n1024\n2048\n4096\n8192\n16384\n32768\n65536\n131072\n262144\n524288\n1048576\n2097152\n4194304\n8388608\n16777216\n33554432\n67108864\n134217728\n268435456\n536870912\n1073741824\n2147483648")
	str[19] = strings.Fields("100000\n200000\n400000\n800000\n1600000\n3200000\n6400000\n12800000\n25600000\n51200000\n102400000\n204800000\n409600000\n819200000\n1638400000\n3276800000\n6553600000\n13107200000\n26214400000\n52428800000\n104857600000\n209715200000\n419430400000\n838860800000\n1677721600000\n3355443200000\n6710886400000\n13421772800000\n26843545600000\n53687091200000\n107374182400000\n214748364800000")
	str[20] = strings.Fields("10\n20\n40\n80\n160\n320\n640\n1280\n2560\n5120\n10240\n20480\n40960\n81920\n163840\n327680\n655360\n1310720\n2621440\n5242880\n10485760\n20971520\n41943040\n83886080\n167772160\n335544320\n671088640\n1342177280\n2684354560\n5368709120\n10737418240\n21474836480")
	str[21] = strings.Fields("1\n2\n4\n8\n16\n32\n64\n128\n256\n512\n1024\n2048\n4096\n8192\n16384\n32768\n65536\n131072\n262144\n524288\n1048576\n2097152\n4194304\n8388608\n16777216\n33554432\n67108864\n134217728\n268435456\n536870912\n1073741824\n2147483648")
	str[22] = strings.Fields("10\n20\n30\n40\n50\n60\n70\n80\n90\n100\n110\n120\n130\n140\n150\n160\n170\n180\n190\n200\n210\n220\n230\n240\n250\n260\n270\n280\n290\n300\n310\n320")
	str[23] = strings.Fields("10\n20\n30\n40\n50\n60\n70\n80\n90\n100\n110\n120\n130\n140\n150\n160\n170\n180\n190\n200\n210\n220\n230\n240\n250\n260\n270\n280\n290\n300\n310\n320")
	str[24] = strings.Fields("\n\n10\n20\n30\n40\n50\n60\n70\n80\n90\n100\n110\n120\n130\n140\n150\n160\n170\n180\n190\n200\n210\n220\n230\n240\n250\n260\n270\n280\n290\n300")
	str[25] = strings.Fields("\n\n\n\n5\n10\n15\n20\n25\n30\n35\n40\n45\n50\n55\n60\n65\n70\n75\n80\n85\n90\n95\n100\n105\n110\n115\n120\n125\n130\n135\n140")
	str[26] = strings.Fields("\n\n\n\n\n\n\n\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n20\n21\n22\n23\n24")
	str[27] = strings.Fields("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15\n16")

	sgem := strings.Fields("10\n10\n20\n20\n30\n30\n30\n30\n40\n40\n40\n40\n40\n40\n40\n40\n50\n50\n50\n50\n50\n50\n50\n50\n50\n50\n50\n50\n50\n50\n50\n50")
	for i, ss := range str {
		fmt.Printf("\n%d:{", i+1)
		for j, s := range ss {
			fmt.Printf("%d: %s,", j+1, s)
			fmt.Printf("%d: %s,", j+1, sgem[len(sgem)-len(ss)+j])
		}
		fmt.Printf("},")
	}
}

func TestGenText6(t *testing.T) {
	//strTarget := strings.Fields("1\n3\n5\n1\n1\n1\n1000\n50000\n200\n1\n2\n2\n8\n10\n2\n1\n3\n10000\n100000\n500\n3\n4\n3\n11\n20\n3\n1\n2\n8\n5\n30000\n200000\n1000\n5\n5\n4\n14\n30\n4\n1\n3\n10\n8\n50000\n300000\n2000\n10\n6\n5\n16\n40\n6\n15\n4\n10\n14\n100000\n400000\n3000\n20\n7\n6\n18\n50\n7\n1\n20\n18\n200000\n500000\n800000\n5000\n7000\n30\n8\n7\n20\n60\n8\n25\n20\n300000\n500000\n1200000\n1500000\n10000\n15000\n20000\n40\n50\n100\n10")
	strPoint := strings.Fields("10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n10\n20\n10\n10\n10\n20\n20\n10\n20\n10\n20\n10\n10\n20\n20\n10\n10\n10\n20\n20\n20\n20\n20\n10\n10\n20\n20\n20\n20\n20\n10\n10\n20\n20\n50\n50\n20\n20\n50\n50\n50\n50\n100\n50\n50\n100\n50")
	i, j := 0, 1
	//for _, s := range strTarget {
	//	i++
	//	switch j {
	//	case 1:
	//		if i > 11 {
	//			i, j = 1, 2
	//			fmt.Printf("},\n")
	//			fmt.Printf("%d: {", j)
	//			fmt.Printf("%d:%s,", i, s)
	//		} else {
	//			if i == 1 {
	//				fmt.Printf("%d: {", j)
	//			}
	//			fmt.Printf("%d:%s,", i, s)
	//		}
	//	case 2:
	//		if i > 11 {
	//			i, j = 1, 3
	//			fmt.Printf("},\n")
	//			fmt.Printf("%d: {", j)
	//			fmt.Printf("%d:%s,", i, s)
	//		} else {
	//			fmt.Printf("%d:%s,", i, s)
	//		}
	//	case 3:
	//		if i > 13 {
	//			i, j = 1, 4
	//			fmt.Printf("},\n")
	//			fmt.Printf("%d: {", j)
	//			fmt.Printf("%d:%s,", i, s)
	//		} else {
	//			fmt.Printf("%d:%s,", i, s)
	//		}
	//	case 4:
	//		if i > 13 {
	//			i, j = 1, 5
	//			fmt.Printf("},\n")
	//			fmt.Printf("%d: {", j)
	//			fmt.Printf("%d:%s,", i, s)
	//		} else {
	//			fmt.Printf("%d:%s,", i, s)
	//		}
	//	case 5:
	//		if i > 13 {
	//			i, j = 1, 6
	//			fmt.Printf("},\n")
	//			fmt.Printf("%d: {", j)
	//			fmt.Printf("%d:%s,", i, s)
	//		} else {
	//			fmt.Printf("%d:%s,", i, s)
	//		}
	//	case 6:
	//		if i > 14 {
	//			i, j = 1, 7
	//			fmt.Printf("},\n")
	//			fmt.Printf("%d: {", j)
	//			fmt.Printf("%d:%s,", i, s)
	//		} else {
	//			fmt.Printf("%d:%s,", i, s)
	//		}
	//	case 7:
	//		if i > 17 {
	//			i, j = 1, 8
	//			fmt.Printf("},\n")
	//			fmt.Printf("%d: {", j)
	//			fmt.Printf("%d:%s,", i, s)
	//		} else {
	//			fmt.Printf("%d:%s,", i, s)
	//		}
	//	}
	//}

	for _, s := range strPoint {
		i++
		switch j {
		case 1:
			if i > 11 {
				i, j = 1, 2
				fmt.Printf("},\n")
				fmt.Printf("%d: {", j)
				fmt.Printf("%d:%s,", i, s)
			} else {
				if i == 1 {
					fmt.Printf("%d: {", j)
				}
				fmt.Printf("%d:%s,", i, s)
			}
		case 2:
			if i > 11 {
				i, j = 1, 3
				fmt.Printf("},\n")
				fmt.Printf("%d: {", j)
				fmt.Printf("%d:%s,", i, s)
			} else {
				fmt.Printf("%d:%s,", i, s)
			}
		case 3:
			if i > 13 {
				i, j = 1, 4
				fmt.Printf("},\n")
				fmt.Printf("%d: {", j)
				fmt.Printf("%d:%s,", i, s)
			} else {
				fmt.Printf("%d:%s,", i, s)
			}
		case 4:
			if i > 13 {
				i, j = 1, 5
				fmt.Printf("},\n")
				fmt.Printf("%d: {", j)
				fmt.Printf("%d:%s,", i, s)
			} else {
				fmt.Printf("%d:%s,", i, s)
			}
		case 5:
			if i > 13 {
				i, j = 1, 6
				fmt.Printf("},\n")
				fmt.Printf("%d: {", j)
				fmt.Printf("%d:%s,", i, s)
			} else {
				fmt.Printf("%d:%s,", i, s)
			}
		case 6:
			if i > 14 {
				i, j = 1, 7
				fmt.Printf("},\n")
				fmt.Printf("%d: {", j)
				fmt.Printf("%d:%s,", i, s)
			} else {
				fmt.Printf("%d:%s,", i, s)
			}
		case 7:
			if i > 17 {
				i, j = 1, 8
				fmt.Printf("},\n")
				fmt.Printf("%d: {", j)
				fmt.Printf("%d:%s,", i, s)
			} else {
				fmt.Printf("%d:%s,", i, s)
			}
		}
	}
}
