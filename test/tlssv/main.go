package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(rsp http.ResponseWriter, req *http.Request) {
		_, _ = fmt.Fprint(rsp, "HelloWorld")
	})
	//log.Fatal(http.ListenAndServe(":9000", nil))
	log.Fatal(http.ListenAndServeTLS(":9000", "E:\\GolandProject\\CartoonSurvival\\test\\tlssv\\tls.crt", "E:\\GolandProject\\CartoonSurvival\\test\\tlssv\\tls.key", nil))
}
