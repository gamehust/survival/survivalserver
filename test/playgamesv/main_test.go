package main_test

import (
	"context"
	"encoding/json"
	"golang.org/x/oauth2"
	"google.golang.org/api/games/v1"
	"google.golang.org/api/option"
	"testing"
	"time"
)

func TestPlayGameService(t *testing.T) {
	config := &oauth2.Config{
		ClientID:     "981967211575-jsnv05kfehjb6t8b1808p8nuooc41fig.apps.googleusercontent.com",
		ClientSecret: "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",
		Endpoint: oauth2.Endpoint{
			AuthURL:   "https://accounts.google.com/o/oauth2/auth",
			TokenURL:  "https://www.googleapis.com/oauth2/v4/token",
			AuthStyle: 0,
		},
		RedirectURL: "https://legend-of-survivor.firebaseapp.com/__/auth/handler",
		Scopes:      []string{games.GamesScope},
	}
	oauthCode := "4/0ARtbsJq9MYL9xHXX5t125Ht4q1AQE4APLgUVmFnJBEeLllc0rgssBc6G07nhMQDWjtG51w"

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	token, err := config.Exchange(ctx, oauthCode)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("%#v", token)
	gamesService, err := games.NewService(ctx, option.WithTokenSource(config.TokenSource(ctx, token)))
	if err != nil {
		t.Fatal(err)
	}

	str, _ := json.MarshalIndent(gamesService, "", "\t")
	t.Logf("%s", str)

	player, err := gamesService.Players.Get("g13000010023353771320").Do()
	if err != nil {
		t.Fatal(err)
	}
	str, _ = json.MarshalIndent(player, "", "\t")
	t.Logf("%s", str)
}
