package main

import (
	"bufio"
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"golang.org/x/net/http2"

	"Survival/lib/log"
	"Survival/lib/util"
	"Survival/src/models"
)

var ueId string
var ueToken string
var ueData *models.UserData

func main() {
	log.InitZapLog("debug")
	Login()
	GetAllData()
	for {
		fmt.Println("=======================================")
		fmt.Println("0: Exit")
		fmt.Println("1: Data")
		fmt.Println("2: Play")
		fmt.Println("3: Shop")
		fmt.Println("4: Event")
		switch ReadCMDInput() {
		case "0":
			fmt.Println("See you again!")
			return
		case "1":
			fmt.Println(util.PrettyJSON(ueData))
		case "2":
			fmt.Println(util.PrettyJSON(ueData.Currency))

		}
	}
}

var httpCli = &http.Client{
	Transport: &http2.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	},
}

var reader = bufio.NewReader(os.Stdin)

func ReadCMDInput() string {
	input, err := reader.ReadString('\n')
	if err != nil {
		panic(err)
	}
	if inputs := strings.Fields(input); len(inputs) > 0 {
		input = inputs[0]
		for i := 1; i < len(inputs); i++ {
			input += " " + inputs[i]
		}
		return input
	}
	return ""
}

func Login() {
	loginReq := models.LoginRequest{
		Present:     models.LoginPresentDeviceId,
		DeviceId:    "ClientFake001",
		CountryCode: "VN",
		Timezone:    "Asia/Bangkok",
	}
	reqData, _ := json.Marshal(&loginReq)
	log.ZapLog.Debugf("send: LoginRequest: %s", reqData)
	req, _ := http.NewRequest(http.MethodPost, "https://192.168.0.85:443/login", bytes.NewReader(reqData))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("AccessToken", "1qazXSW@3edcVFR$")
	rsp, err := httpCli.Do(req)
	if err != nil || rsp.StatusCode != http.StatusOK {
		log.ZapLog.Fatal(err)
	}
	rspData, _ := ioutil.ReadAll(rsp.Body)
	defer rsp.Body.Close()
	log.ZapLog.Debugf("recv: %s", rspData)
	var loginRsp models.LoginResponse
	_ = json.Unmarshal(rspData, &loginRsp)

	ueId = loginRsp.Id
	ueToken = loginRsp.Token
}

func GetAllData() {
	req, _ := http.NewRequest(http.MethodGet, "https://192.168.0.85/data/"+ueId, nil)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("AccessToken", "1qazXSW@3edcVFR$")
	req.Header.Set("UEToken", ueToken)
	log.ZapLog.Debug("send: GetAllData")
	rsp, err := httpCli.Do(req)
	if err != nil || rsp.StatusCode != http.StatusOK {
		log.ZapLog.Fatal(err)
	}
	rspData, _ := ioutil.ReadAll(rsp.Body)
	defer rsp.Body.Close()
	log.ZapLog.Debugf("recv: %s", rspData)
	if err = json.Unmarshal(rspData, &ueData); err != nil {
		log.ZapLog.Fatal(err)
	}
}
