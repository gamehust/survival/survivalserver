function SearchUserLogs() {
    let id = document.getElementById("idSearchUserLogs").value;
    let time = document.getElementById("timeSearchUserLogs").value;
    $.ajax({
        type: "GET",
        url: "/N23q8CymDu/user-logs/" + id + "/" + time,
        datatype: "json",
        error: function (request, status, error) {
            alert("API_ERROR: " + request.responseText);
        },
        success: function (data) {
            $('#resultSearchUserLogs').dataTable().fnClearTable();
            $('#resultSearchUserLogs').dataTable().fnAddData(data);
        },
    });
}

$(document).ready(function () {
    $('#resultSearchUserLogs').DataTable({
        "columns": [
            { "width": "20%" },
            { "width": "20%" },
            { "width": "60%" },
        ],
        "order": [[0, 'esc']],
    });
});